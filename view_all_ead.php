<h2 style="margin-top:0">Encoded Archival Description</h2>
<table class="tbl_view_ead_content" width="100%">
	<caption style="font-size:20px">EAD Entry View</caption>
	<tr>
		<th>Title</th>
		<th>Date</th>
		<th>Author</th>
		<th>Options</th>
	</tr>
	<?php 
		foreach($arr_ead_result as $row): ?>
		<tr>
			<td width="50%"><?php echo $row['TITLE']; ?></td>
			<td width="10%"><?php echo $row['DATE']; ?></td>
			<td width="30%"><?php echo $row['AUTHOR']; ?></td>
			<td width="10%" class="spacinga">
				<form name="frm_view_ead" id="frm_view_ead" method="get" action="view.php">
					<a class="view_all_ead_link" href="view.php?ead=<?php echo $row['ID'];?>">Details</a>  <br />
					<a class="view_all_ead_link" href="view.php?xml=<?php echo $row['ID'];?>" target="_blank">Xml</a> <br />
					<a class="view_all_ead_link" href="view.php?isEad=<?php echo $row['ID'];?>" target="_blank">Ead</a>
				</form>
				<?php if ($is_user_logged_in && ($role == "1" || $role == "2")) { ?>
				<form name="frm_update_ead" id="frm_update_ead" method="post" action="update_ead.php">
					<input type="hidden" value="<?php echo $row['ID'];?>" id="ead_id" name="ead_id">
					<input type="submit" class="btn btn-success" name="submit" id="submit" value="Update" />
				</form>
				<?php }?>
				<?php if ($is_user_logged_in && ($role == "1")) { ?>
				<form name="frm_delete_ead" id="frm_delete_ead" method="post" action="delete_ead_by_id.php">
					<input type="hidden" value="<?php echo $row['ID'];?>" id="ead_id" name="ead_id">
					<input type="submit" class="btn btn-danger" name="delete_ead" id="delete_ead" value="Delete" />
				</form>
				<?php }?>
			</td>
		</tr>
		<tr class="empty_row"><td colspan="4">&nbsp;</td></tr>
	<?php endforeach; ?>
</table>