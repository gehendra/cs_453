<div class="content">
<form name="user_sign_in" id="user_sign_in" method="post" action="login.php">
	<div class="form-group" id="user_email">
		<div class="ead_left">
			<label for="Title">Email:*</label>
		</div>
		<div class="ead_right">
		  <input class="form-control" type="text" id="txt_user_email" name="txt_user_email" required="required" placeholder="Enter the email"/>
		</div>
	</div>
	<div class="clearfix"></div>
	<div class="form-group" id="user_pass">
		<div class="ead_left">
			<label for="Title">Password:*</label>
		</div>
		<div class="ead_right">
		  <input class="form-control" type="password" id="txt_user_pass" name="txt_user_pass" required="required" placeholder="Enter the password"/>
		</div>
	</div>
	<div class="clearfix"></div>
	<div class="form-group" id="user_submit">
		<div class="ead_left">
			&nbsp;
		</div>
		<div class="ead_right">
		  <input class="form-control btn btn-success" type="submit" id="txt_user_submit" name="txt_user_submit" value="Submit" />
		</div>
	</div>
	<div class="clearfix"></div>
	<div class="form-group" id="ead_title">
		<div class="ead_left">
			&nbsp;
		</div>
		<div class="ead_right">
			<a class="forgot_password" href="forgot_password.php">forgot password?</a>
			<a class="new_user" href="add_new_user.php">New User</a>
		</div>
	</div>
</form>
</div>