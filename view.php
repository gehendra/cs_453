<?php include "header.php";?>
	<?php
		$view_specific_ead = "";
		$is_xml = "";
		$display_ead = "";

		if (isset($_GET['ead'])) {
			$view_specific_ead = $_GET['ead'];
		}
		
		if (isset($_GET['xml'])) {
			$is_xml = $_GET['xml'];
		}

		if (isset($_GET['isEad'])) {
			$display_ead = $_GET['isEad'];
		}
	?>
	<div class="content">
		<div class="form-content">
			<div class="view_ead_content">
				<?php
					if($view_specific_ead != "" && $is_xml == "") {
						$arr_specific_ead_result = view_ead($view_specific_ead);
						include "view_specific_ead.php";
					}
					else if ($is_xml != "" &&  $display_ead == "" && $view_specific_ead == "") {
						$arr_specific_ead_result = view_ead($is_xml);
						include "view_xml.php";
					}
					else if ($display_ead != "" && $is_xml == "" && $view_specific_ead == "") {
						$arr_specific_ead_result = view_ead($display_ead);
						include "view_xml_ead_test.php";
					}
					else {
						$arr_ead_result = array();
						$arr_ead_result = view_all_ead();
						include "view_all_ead.php";
					}
				?>
			</div>
			<!--EAD Information Ends Here-->
		</div>
	</div>
<?php include "footer.php"; ?>


<?php 
	function view_all_ead() {
		include "connection.php";
		try {
 			$con = new PDO("mysql:host=$db_host;dbname=$mysql_name", $db_user, $db_pass);
			$sql = "CALL get_all_ead()";
			$stmt = $con->prepare($sql);
			$stmt->execute();
 		}
 		catch (PDOException $e)
	  {
	    $error = 'Error searching by email.' ;
	    exit();
	  }
	  $arr_result = $stmt->fetchAll();
	  return $arr_result;
	}

	function view_ead($view_specific_ead) {
		include "connection.php";
		try {
			// Pull from ead table
 			$con = new PDO("mysql:host=$db_host;dbname=$mysql_name", $db_user, $db_pass);
			$sql = "CALL get_specific_ead(:id)";
			$stmt = $con->prepare($sql);
			$stmt->bindParam(':id', intval($view_specific_ead), PDO::PARAM_INT);
			$result = $stmt->execute();
			$arr_ead_result = $stmt->fetchAll();
			$arr_ead_result = $arr_ead_result[0];

			// pull from publication
			$con = new PDO("mysql:host=$db_host;dbname=$mysql_name", $db_user, $db_pass);
			$sql = "CALL get_publisherd_by_eid(:ead_id)";
			$stmt = $con->prepare($sql);
			$stmt->bindParam(':ead_id', intval($view_specific_ead), PDO::PARAM_INT);
			$stmt->execute();
			$arr_publisher = $stmt->fetchAll();
			$arr_ead_result['PUBLISHER'] = $arr_publisher[0];

			// pull from collection
			$con = new PDO("mysql:host=$db_host;dbname=$mysql_name", $db_user, $db_pass);
			$sql = "CALL get_collection_by_ead_id(:ead_id)";
			$stmt = $con->prepare($sql);
			$stmt->bindParam(':ead_id', intval($view_specific_ead), PDO::PARAM_INT);
			$stmt->execute();
			$arr_collection = $stmt->fetchAll();
			$arr_ead_result['COLLECTION'] = $arr_collection[0];

			// pull from ead_admin
			$con = new PDO("mysql:host=$db_host;dbname=$mysql_name", $db_user, $db_pass);
			$sql = "CALL get_ead_admin_by_ead_id(:ead_id)";
			$stmt = $con->prepare($sql);
			$stmt->bindParam(':ead_id', intval($view_specific_ead), PDO::PARAM_INT);
			$stmt->execute();
			$arr_ead_admin = $stmt->fetchAll();
			$arr_ead_result['EAD_ADMIN'] = $arr_ead_admin[0];

			// pull from access_control
			$con = new PDO("mysql:host=$db_host;dbname=$mysql_name", $db_user, $db_pass);
			$sql = "CALL get_access_control_by_ead_id(:ead_id)";
			$stmt = $con->prepare($sql);
			$stmt->bindParam(':ead_id', intval($view_specific_ead), PDO::PARAM_INT);
			$stmt->execute();
			$arr_acess_control = $stmt->fetchAll();
			$arr_ead_result['ACESS_CONTROL'] = $arr_acess_control[0];

			//pull from container
			$con = new PDO("mysql:host=$db_host;dbname=$mysql_name", $db_user, $db_pass);
			$sql = "CALL get_ead_container(:container_id)";
			$stmt = $con->prepare($sql);
			$stmt->bindParam(':container_id', intval($view_specific_ead), PDO::PARAM_INT);
			$stmt->execute();
			$arr_ead_result_container = $stmt->fetchAll();
			$arr_ead_result['CONTAINER'] = $arr_ead_result_container;
			$arr_ead_result['CONTAINER_COUNT'] = count($arr_ead_result_container);
 		}
 		catch (PDOException $e)
	  {
	    $error = 'Database connection issue.' ;
	    exit();
	  }
		return $arr_ead_result;
	}
?>