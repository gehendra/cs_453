<?php include "header.php";?>
	<div class="content">
		<div class="form-content">
			<div class="view_ead_content">
				<?php
				if (isset($_POST['submit'])) {
					$title = $_POST['txt_ead_title'];
					$sub_title = $_POST['txt_ead_subtitle'];
					$author = $_POST['txt_ead_author'];
					$arr_ead_result = search_ead($title, $sub_title, $author);
					if ($arr_ead_result) {
						include "view_all_ead.php";	
						exit(0);
					}
					else {
						echo "No result found. Please try again";
					}
				}
				?>
				<?php include "search_form.php";?>
				</div>
			</div>
	</div>
<?php include "footer.php"; ?>


<?php 
function search_ead($title="", $sub_title="", $author="") {
	include "connection.php";
	try {
		$con = new PDO("mysql:host=$db_host;dbname=$mysql_name", $db_user, $db_pass);
		$sql = "SELECT TITLE,DATE,AUTHOR from EAD where ";
		if ($title && $sub_title && $author) {
			$sql = $sql . "TITLE like " . "\"%" . $title . "%\"" . " AND SUBTITLE like " . "\"%" .  $sub_title . "%\"" . " AND AUTHOR like " . "\"%" . $author . "%\"";
		}
		else if ($title && $sub_title) {
			$sql = $sql . "TITLE like " . "\"%" . $title . "%\"" . " AND SUBTITLE like " . "\"%" .  $sub_title . "%\"";
		}
		else if ($sub_title && $author) {
			$sql = $sql . "SUBTITLE like " . "\"%" .  $sub_title . "%\"" . " AND AUTHOR like " . "\"%" . $author . "%\"";
		}
		else if ($title && $author) {
			$sql = $sql . "TITLE like " . "\"%" . $title . "%\"" . " AND AUTHOR like " . "\"%" . $author . "%\"";
		}
		else if ($title) {
			$sql = $sql . "TITLE like " . "\"%" . $title . "%\"";
		}
		else if ($sub_title) {
			$sql = $sql . "SUBTITLE like " . "\"%" .  $sub_title . "%\"" ;
		}
		else if ($author) {
			$sql = $sql . "AUTHOR like " . "\"%" . $author . "%\"";
		}
		else if ($title == "" && $sub_title == "" && $date == "" && $author == "") {
			$sql = $sql . "1" ;
		}
		$stmt = $con->prepare($sql);
		$stmt->execute();
		
		}
		catch (PDOException $e)
  {
    $error = 'Database connection issue.' ;
    exit();
  }
  $result = $stmt->fetchAll();
  return $result;
}
?>