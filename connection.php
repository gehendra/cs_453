<?php
$db_host = "localhost";  // Change as required
$db_user = "archive";  // Change as required
$db_pass = "";  // Change as required
$mysql_name = "archive";        // Change as required

try
{
  $pdo = new PDO("mysql:host=$db_host;dbname=$mysql_name", $db_user, $db_pass);
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $pdo->exec('SET NAMES "utf8"');
}
catch (PDOException $e)
{
  $error = 'Unable to connect to the database server.';
  include 'error.html.php';
  exit();
}
?>