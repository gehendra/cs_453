<?php include "header.php";?>
	<div class="content">
		<div class="form-content">
			<div class="view_ead_content">
				<?php
				if (isset($_POST['submit'])) {
					$CCESID = $_POST['txt_ead_cces_id_no'];
					$diocese = $_POST['txt_ead_diocese'];
					$description = $_POST['txt_ead_description_of_image'];
					$arr_cces_result = search_cces($CCESID, $diocese, $description);
					if ($arr_cces_result) {
						include "view_all_cces.php";	
						exit(0);
					}
					else {
						echo "No result found. Please try again";
					}
				}
				?>
					<?php include "cces_search_form.php";?>
				</div>
			</div>
	</div>
<?php include "footer.php"; ?>


<?php 
function search_cces($ccesid="", $diocese="", $description="") {
	include "connection.php";
	try {
		$con = new PDO("mysql:host=$db_host;dbname=$mysql_name", $db_user, $db_pass);
		$sql = "SELECT * from cces_photos where ";
		if ($ccesid && $diocese && $description) {
			$sql = $sql . "CCESID=" . intval($ccesid) . " AND DIOCESE like " . "\"%" .  $diocese . "%\"" . " AND DESCRIPTION like " . "\"%" . $description . "%\"";
		}
		else if ($ccesid && $diocese) {
			$sql = $sql . "CCESID=" . intval($ccesid) . " AND DIOCESE like " . "\"%" .  $diocese . "%\"";
		}
		else if ($ccesid && $description) {
			$sql = $sql . "CCESID=" . intval($ccesid) . " AND DESCRIPTION like " . "\"%" . $description . "%\"";
		}
		else if ($ccesid) {
			$sql = $sql . "CCESID=" . intval($ccesid);
		}
		else if ($diocese && $description) {
			$sql = $sql . "DIOCESE like " . "\"%" .  $diocese . "%\"" . " AND DESCRIPTION like " . "\"%" . $description . "%\"";
		}
		else if ($diocese) {
			$sql = $sql . "DIOCESE like " . "\"%" .  $diocese . "%\"";
		}
		else if ($description) {
			$sql = $sql . "DESCRIPTION like " . "\"%" . $description . "%\"";
		}
		else if ($ccesid == "" && $diocese == "" && $date == "" && $description == "") {
			$sql = $sql . "1" ;
		}
		$stmt = $con->prepare($sql);
		$stmt->execute();
		
		}
		catch (PDOException $e)
  {
    $error = 'Database connection issue.' ;
    exit();
  }
  $result = $stmt->fetchAll();
  return $result;
}
?>