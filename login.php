<?php
	if (isset($_POST["txt_user_submit"]) && isset($_POST['txt_user_email']) && isset($_POST['txt_user_pass']))
	{
		include "global_functions.php";
		require "password.php";
		$user_access = login($_POST);
		$email = $_POST['txt_user_email'];
		$options = array('cost' => 11);
		$password_test = password_hash($_POST['txt_user_pass'], PASSWORD_BCRYPT, $options);
	 	$valid_password = password_verify($_POST['txt_user_pass'], $password_test);
	 	if ($valid_password)
		{
			track_login_user($email);
			session_start();
			$_SESSION['archive']['loggedIn'] = TRUE;
      $_SESSION['archive']['email'] = $user_access['EMAIL'];
      $_SESSION['archive']['fname'] = $user_access['FIRST_NAME'];
      $_SESSION['archive']['lname'] = $user_access['LAST_NAME'];
      $_SESSION['archive']['role'] = $user_access['ROLE'];
      header('Location:  index.php');
		}
		else {
			session_start();
			unset($_SESSION['archive']['loggedIn']);
      unset($_SESSION['archive']['email']);
      unset($_SESSION['archive']['fname']);
      unset($_SESSION['archive']['lname']);
      unset($_SESSION['archive']['role']);
      header("Location: /archive/login.php");
		}
	}

	include $_SERVER['DOCUMENT_ROOT'] . "/archive/header.php";
	include $_SERVER['DOCUMENT_ROOT'] . "/archive/login_form.php";
	include $_SERVER['DOCUMENT_ROOT'] . "/archive/footer.php";

	function track_login_user($email) {
		include $_SERVER['DOCUMENT_ROOT']. "/archive/connection.php";
		try {
	 		$con = new PDO("mysql:host=$db_host;dbname=$mysql_name", $db_user, $db_pass);
	 		$date = date("Y-m-d h:i:s");
			$sql = "INSERT INTO archive_log (DESCRIPTION, LOGGED_TIME) values (\"User loggedin\",\"" .  $date . "\")";
			$stmt = $con->prepare($sql);
			$result = $stmt->execute();
		} catch (PDOException $e) {
			die("Error occurred:" . $e->getMessage());
		}
	}
?>