<h2 style="margin-top:0">Catholic Church Extension Society</h2>
<legend style="margin-bottom:10">Search Form</legend>
<form role="form" name="frmSearchCCES" method="post" enctype="multipart/form-data" onsubmit="return isValidForm();">
	<div class="form-group" id="ead_subtitle">
		<label for="CCESID">CCES ID</label>
		<div class="ead_right">
		  <input class="form-control" type="text" id="txt_ead_cces_id_no" name="txt_ead_cces_id_no" placeholder="Enter a CCES ID"/>
		</div>
	</div>

	<div class="form-group" id="ead_subtitle">
		<label for="Diocese">Diocese</label>
		<div class="ead_right">
			<input class="form-control" type="text" id="txt_ead_diocese" name="txt_ead_diocese" placeholder="Enter a Diocese" />
		</div>
	</div>

	<div class="form-group" id="ead_subtitle">
		<label for="description">Description of Image</label>
		<div class="ead_right">
			<input class="form-control" type="text" id="txt_ead_description_of_image" name="txt_ead_description_of_image" placeholder="Enter keywords to search the description" />
		</div>
	</div>
	<input type="submit" name="submit" id="submit" value="Search" class="btn btn-success"/>
</form>