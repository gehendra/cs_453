<div class="form-group">	
	<fieldset id="ead_scope_and_content">
		<legend>Scope and Content</legend>
    <div class="form-group" id="lbl_ead_scope_content_note">
			<label for="Note">Note :</label>
			<div class="ead_right">
        <textarea class="form-control" name="txt_ead_scope_content_note" id="txt_ead_scope_content_note" cols="45" rows="5"></textarea>
      </div> 
		</div>   
  	<div class="form-group" id="lbl_ead_scope_content_arrangement_note">
			<label for="Arrangement Note">Arrangement Note :</label>
			<div class="ead_right">
      	<textarea class="form-control" name="txt_ead_scope_content_note" id="txt_ead_scope_content_note" cols="45" rows="5"></textarea>
    	</div> 
		</div>
		<fieldset>
			<legend>Controlled Access</legend>
			<?php include "access_control.php";?>
      <input type="button" class="add_access_control btn btn-primary" value="Add Another Access Control" />
		</fieldset>
	</fieldset>
</div>