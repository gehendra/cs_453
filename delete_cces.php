<?php
	session_start();
	if (!isset($_SESSION['archive']['email']) && $_SESSION['archive']['role'] != "1") {
		header("Location: /archive/index.php");
	}
	if (isset($_POST['delete_cces']) && isset($_POST['cces_image_no']) && $_POST['cces_image_no'] != "") {
		$image_no = intval($_POST['cces_image_no']);
		delete_ccess_image_no($image_no);
		delete_subject_by_image_no($image_no);
		header("Location: " . "/archive/view_cces.php");
	}

	function delete_ccess_image_no($image_no) {
		include $_SERVER['DOCUMENT_ROOT']. "/archive/connection.php";
		try {
			if ($image_no) {
				$con = new PDO("mysql:host=$db_host;dbname=$mysql_name", $db_user, $db_pass);
				$sql = "CALL delete_cces_by_image_no(:image_no)";
				$stmt = $con->prepare($sql);
				$stmt->bindParam(':image_no', intval($image_no), PDO::PARAM_INT);
				$stmt->execute();
			}
		}
		catch (PDOException $e)
	  {
	    $error = 'Dattabase connection error.' ;
	    exit();
	  }
	}

	function delete_subject_by_image_no($image_no) {
		include $_SERVER['DOCUMENT_ROOT']. "/archive/connection.php";
		try {
			if ($image_no) {
				$con = new PDO("mysql:host=$db_host;dbname=$mysql_name", $db_user, $db_pass);
				$sql = "CALL delete_subject_by_image_no(:image_no)";
				$stmt = $con->prepare($sql);
				$stmt->bindParam(':image_no', intval($image_no), PDO::PARAM_INT);
				$stmt->execute();
			}

		}
		catch (PDOException $e)
	  {
	    $error = 'Dattabase connection error.' ;
	    exit();
	  }
	}

?>