<?php
	session_start();
	if (!isset($_SESSION['archive']['email']) && $_SESSION['archive']['role'] != "1") {
		header("Location: /archive/index.php");
	}
	include $_SERVER['DOCUMENT_ROOT']. "/archive/header.php";
?>
	<div class="content">
	<?php 
	$user_id = intval($current_user['ID']);
	$user_info = get_user_info($user_id);
	?>
	<?php include $_SERVER['DOCUMENT_ROOT']. "/archive/admin/see_profile.php"; ?>
		
	</div>
	<?php include $_SERVER['DOCUMENT_ROOT']. "/archive/footer.php"; 
	
	function get_user_info($user_id) {
	include $_SERVER['DOCUMENT_ROOT']. "/archive/connection.php";
 	$user = "" ;
	try {
		$con = new PDO("mysql:host=$db_host;dbname=$mysql_name", $db_user, $db_pass);
		$sql = "CALL get_user_info_by_id(:id)";
		$stmt = $con->prepare($sql);
		$stmt->bindParam(':id', intval($user_id), PDO::PARAM_INT);
		$stmt->execute();
	}
	catch (PDOException $e)
  {
    $error = 'Database connection issue.' ;
    exit();
  }
  $user = $stmt->fetchAll();

 	return $user[0];
	}
?>
