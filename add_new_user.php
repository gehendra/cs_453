<?php
session_start();
if(isset($_SESSION['archive']['email']))
{
	header("Location: index.php");
}
	include "connection.php";
  
	//Insert user starts here
	if(isset($_POST["btn_submit"]) && $_POST["txt_first_name"] != "" && $_POST["txt_last_name"] != "" && $_POST["txt_email"] != ""  && $_POST["txt_password"] != "") {
	 	try {
	 		$con = new PDO("mysql:host=$db_host;dbname=$mysql_name", $db_user, $db_pass);
			$sql = "CALL add_user(:fname,:lname,:email,:pass)";
			$stmt = $con->prepare($sql);
			$stmt->bindParam(':fname', $_POST["txt_first_name"], PDO::PARAM_STR);
			$stmt->bindParam(':lname', $_POST["txt_last_name"], PDO::PARAM_STR);
			$stmt->bindParam(':email', $_POST["txt_email"], PDO::PARAM_STR);
			$options = array('cost' => 11);
			require "password.php";
		 	$password = password_hash($_POST["txt_password"], PASSWORD_DEFAULT, $options);
			$stmt->bindParam(':pass', $password, PDO::PARAM_STR);
			$result = $stmt->execute();
			if ($result) {
				$new_user_info = get_user_details($_POST["txt_email"]);
				session_start();
				$_SESSION['archive']['user_id'] = $new_user_info['ID'];
				$_SESSION['archive']['email'] = $_POST["txt_email"];
				$_SESSION['archive']['fname'] = $new_user_info['FIRST_NAME'];
				$_SESSION['archive']['lname'] = $new_user_info['LAST_NAME'];
				$_SESSION['archive']['role'] = $new_user_info['ROLE'];
			}
			header("Location: /archive/index.php");

		} catch (PDOException $e) {
			die("Error occurred:" . $e->getMessage());
		}
	}
?>
<?php include "header.php"; ?>
<div class="content">
	<?php include "add_new_user_form.php"; ?>
</div>
<?php include "footer.php"; ?>

<?php 
function get_user_details($email){
	include "connection.php";
	$con = new PDO("mysql:host=$db_host;dbname=$mysql_name", $db_user, $db_pass);
	$sql = "CALL get_user_info(:email)";
	$stmt = $con->prepare($sql);
	$stmt->bindParam(':email',$email, PDO::PARAM_STR);
	$result = $stmt->execute();
	if ($result) {
		return $stmt->fetch();
	}
	else {
		return false;
	}
}
?>

