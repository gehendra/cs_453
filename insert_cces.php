<?php 
include_once "mysql_info.php";
include_once "MySQL.php";
$table_name = "cces_photos";
$ead = new MySQL($mysql_name,$db_user,$db_pass,$db_host);
// Insert to CCES Begins
 
if($_POST["submit"] && isset($_POST['txt_ead_cces_id_no']) && isset($_POST['txt_ead_state']) && isset($_POST['txt_ead_description_of_image']) && isset($_POST['txt_ead_summary_image_size_l'])) { 
	$cces_array = array (
		'CCESID'=> $_POST['txt_ead_cces_id_no'], 
		'STATE'=> $_POST['txt_ead_state'], 
		'DESCRIPTION'=> $_POST['txt_ead_description_of_image'], 
		'IMAGE_SIZE_L'=> $_POST['txt_ead_summary_image_size_l'], 
		'IMAGE_SIZE_W'=> $_POST['txt_ead_summary_image_size_w'], 
		'OTHERID' => $_POST['txt_ead_other_id_no'],
		'TOWN_OR_CITY' => $_POST['txt_ead_town_or_city'],
		'DIOCESE' =>  $_POST['txt_ead_diocese'],
		'CHURCH' =>  $_POST['txt_ead_church'],
		'DATE' =>  $_POST['txt_ead_date'],
		'TRANSCRIPTION' =>  $_POST['txt_ead_transcription'],
		'TYPE' =>  $_POST['txt_ead_summary_image_type'],
		'FILENAME' => $_FILES["fileToUpload"]["name"]
	);
	$ead->insert($cces_array,$table_name);
	$image_no = $ead->LastInsertID();

	$table_name = "cces_photos";
	$ead = new MySQL($mysql_name,$db_user,$db_pass,$db_host);
	if ($image_no) {
		$filename = upload_image($image_no,$_FILES);
		$update_filename = array ('FILENAME' => $filename);
		$condition = array ('IMAGE_NO' => intval($image_no));
		$ead->update($table_name, $update_filename, $condition);

	}
	
	// Insert to CCES Ends 
	// Insert to Subject Begins
	$IMAGE_NO = $image_no;
	$count_subject = 0;
	if($_POST['txt_ead_subject_name']) {
	  $count_subject = count($_POST['txt_ead_subject_name']);
	}
	$table_name = "SUBJECTS";
	$ead = new MySQL($mysql_name,$db_user,$db_pass,$db_host);
	for ($i=0;$i<$count_subject;$i++) {
		$subject_array = array (
			'NAME' => $_POST['txt_ead_subject_name'][$i],
			'IMAGE_NO' => intval($IMAGE_NO)
		);
		$ead->insert($subject_array,$table_name);
	}
	header("Location: /archive/view_cces.php");
}
?>
<?php
	//function upload_image($image_no,$_FILES) {
	function upload_image($image_no,$files) {
		$target_dir = $_SERVER['DOCUMENT_ROOT']. "/archive/uploads/";
		$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
		$break_filename = explode(".",$_FILES["fileToUpload"]["name"]);
		$filename = $image_no . "." . $break_filename[1];
		$target_file =  $target_dir . $filename;
		$uploadOk = 1;
		$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
		// Check if image file is a actual image or fake image
		if($image_no) {
		  $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
	    if($check !== false) {
	      $uploadOk = 1;
	    } else {
	      $uploadOk = 0;
	    }
		}
		// Check if file already exists
		if (file_exists($target_file)) {
		    echo "Sorry, file already exists.";
		    $uploadOk = 0;
		}
		// Check file size
		/*if ($_FILES["fileToUpload"]["size"] > 500000) {
		    echo "Sorry, your file is too large.";
		    $uploadOk = 0;
		}
		// Allow certain file formats
		if($imageFileType != "jpg" && $imageFileType != "JPG" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) {
		    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
		    $uploadOk = 0;
		}*/
		// Check if $uploadOk is set to 0 by an error
		if ($uploadOk == 0) {
		    echo "Sorry, your file was not uploaded.";
		// if everything is ok, try to upload file
		} else {
		  if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        return $filename;
	    } else {
        echo "Sorry, there was an error uploading your file.";
	    }
		}
	}
?>