<?php
// "Create" the document.
	$xml = new DOMDocument("1.0", "ISO-8859-15");
	$ead = $xml->createElement( "cces" );
	$ead->setAttribute("image_no", $arr_specific_cces_result['IMAGE_NO']);

	$cces_id = $xml->createElement("cces_id", $arr_specific_cces_result['CCESID']);
	$ead->appendChild($cces_id);
	
	if($arr_specific_cces_result['OTHERID'] != "") {
		$other_id = $xml->createElement("other_id", $arr_specific_cces_result['OTHERID']);
		$ead->appendChild($other_id);
	}
	
	$state = $xml->createElement("state", $arr_specific_cces_result['STATE']);
	$ead->appendChild($state);
	
	if($arr_specific_cces_result['TOWN_OR_CITY'] != "") {
		$town_or_city = $xml->createElement("town_or_city", $arr_specific_cces_result['TOWN_OR_CITY']);
		$ead->appendChild($town_or_city);
	}
	
	if($arr_specific_cces_result['DIOCESE'] != "") {
		$diocese = $xml->createElement("diocese", $arr_specific_cces_result['DIOCESE']);
		$ead->appendChild($diocese);
	}
		
	if($arr_specific_cces_result['CHURCH'] != "") {
		$church = $xml->createElement("church", $arr_specific_cces_result['CHURCH']);
		$ead->appendChild($church);
	}

	if($arr_specific_cces_result['DATE'] != "") {
		$date = $xml->createElement("date", $arr_specific_cces_result['DATE']);
		$ead->appendChild($date);	
	}
	
	$description = $xml->createElement("description", $arr_specific_cces_result['DESCRIPTION']);
	$ead->appendChild($description);
	
	if($arr_specific_cces_result['TRANSCRIPTION'] != "") {
		$transcription = $xml->createElement("transcription", $arr_specific_cces_result['TRANSCRIPTION']);
		$ead->appendChild($transcription);
	}
	
	$image_size_l = $xml->createElement("image_size_l", $arr_specific_cces_result['IMAGE_SIZE_L']);
	$ead->appendChild($image_size_l);

	$image_size_w = $xml->createElement("image_size_w", $arr_specific_cces_result['IMAGE_SIZE_W']);
	$ead->appendChild($image_size_w);

	if($arr_specific_cces_result['TYPE'] != "") {
		$type = $xml->createElement("type", $arr_specific_cces_result['TYPE']);
		$ead->appendChild($type);
	}
	
	$file_name = $xml->createElement("file_name", $arr_specific_cces_result['FILENAME']);
	$ead->appendChild($file_name);
	

	/* Subject section begins */
	$container_type = 0;
	$arr_container_content = $arr_specific_cces_result['SUBJECTS'];

	$subject = $xml->createElement("subject");
	$ead->setAttribute("image_no", $arr_specific_cces_result['IMAGE_NO']);
	if(is_array($arr_container_content)) {
		foreach ($arr_container_content as $subjects_row) {
			$name = $xml->createElement("name", $subjects_row['NAME']);
			$subject->appendChild($name);
		}	
	}
	$ead->appendChild($subject);
	/* Subject section ends   */

	$xml->appendChild($ead);
	$xml->FormatOutput = true;
	$string_value = $xml->saveXML();
	$xml->save("ead_cces.xml");



	/* XML format generator ends   */
?>

<script type="text/javascript">
	$(document).ready(function(){
		setTimeout(function(){
			window.location.href = "http://localhost:8888/archive/ead_cces.xml"}, 1000);
	});

</script>