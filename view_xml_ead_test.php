<?php
	$access_control_title = unserialize($arr_specific_ead_result['ACCESS_CONTROL']['ACCESS_CONTROL_TITLE']);
	$count_access_control_title = count($access_control_title);
	$access_control_name = unserialize($arr_specific_ead_result['ACCESS_CONTROL']['ACCESS_CONTROL_NAME']);
	$count_access_control_name = count($access_control_name);
	$access_control_corp_name = unserialize($arr_specific_ead_result['ACCESS_CONTROL']['ACCESS_CONTROL_CORP_NAME']);
	$count_access_control_corp_name = count($access_control_corp_name);

	$max_count_access_control_array = max ($count_access_control_title,$count_access_control_name,$count_access_control_corp_name);

// "Create" the document.
	$imp = new DOMImplementation;

	$dtd = $imp->createDocumentType('ead', '', '');
	//$dtd = $imp->createDocumentType('ead', '', 'C:\xampp\htdocs\xampp\cs_336\ead.dtd');
	// dtd hidden so will show on browser
	$xml = $imp->createDocument("", "", $dtd);
	$xml->encoding = "UTF-8";
	$xml->version = "1.0";
//	$xslt = $xml->createProcessingInstruction('xml-stylesheet', 'type="text/xsl" href="HowdeshellEADstylesheet.css"');
//	$xml->appendChild($xslt);
//  to do: add in doctype header <!DOCTYPE ead SYSTEM "C:\xampp\htdocs\xampp\cs_336\ead.dtd">

	

	
	$ead = $xml->createElement( "ead" );
	$ead->setAttribute("xsi:schemaLocation", "urn:isbn:1-931666-22-9 http://www.loc.gov/ead/ead.xsd");
	$ead->setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
	$ead->setAttribute("xmlns:xlink", "http://www.w3.org/1999/xlink");
	$ead->setAttribute("xmlns", "urn:isbn:1-931666-22-9");

	
// beginning of ead header

	
	$eadheader = $xml->createElement("eadheader");
	$eadheader->setAttribute("countryencoding", "iso3166-1");
	$eadheader->setAttribute("scriptencoding", "iso15924");
	$eadheader->setAttribute("langencoding", "iso639-2b");
	$eadheader->setAttribute("audience", "external");
	$eadheader->setAttribute("relatedencoding", "dublincore");
	
	
	$ead->appendChild($eadheader);

	
	
	
	
	
	$eadid = $xml->createElement("eadid");
	$eadid->setAttribute("identifier", $arr_specific_ead_result['ID']);
	$eadid->setAttribute("mainagencycode", "ICL-Ar");
	$eadid->setAttribute("countrycode", "US");
	
	
	$eadheader->appendChild($eadid);
	
	
// beginning of file description section
	
	$filedesc = $xml->createElement("filedesc");
	$eadheader->appendChild($filedesc);
	
	$titlestmt = $xml->createElement("titlestmt");
	$filedesc->appendChild($titlestmt);
	
	$titleproper = $xml->createElement("titleproper", $arr_specific_ead_result['TITLE']);
	$titleproper->setAttribute("encodinganalog", "title");
	

	$title_date = $xml->createElement("date", $arr_specific_ead_result['DATE']);
	$title_date->setAttribute("normal", "1999/2000");
	$titleproper->appendChild($title_date);

	$titlestmt->appendChild($titleproper);

	$subtitle = $xml->createElement("subtitle", $arr_specific_ead_result['SUBTITLE']);
	$titlestmt->appendChild($subtitle);

	
	
	
	
	

	$prepared_by_author = $xml->createElement("author", $arr_specific_ead_result['AUTHOR']);
	$prepared_by_author->setAttribute("encodinganalog", "creator");
	$titlestmt->appendChild($prepared_by_author);

	
	
	
	$publicationstmt = $xml->createElement("publicationstmt");
	$filedesc->appendChild($publicationstmt);
	
	$publication_publisher = $xml->createElement("publisher", $arr_specific_ead_result['PUBLISHER']['PUBLISHER']);
	$publication_publisher->setAttribute("encodinganalog", "publisher");
	$publicationstmt->appendChild($publication_publisher);
	
	$publication_date = $xml->createElement("date", $arr_specific_ead_result['PUBLISHER']['PUBLISHER_DATE']);
	$publication_date->setAttribute("encodinganalog", "date");
	$publication_date->setAttribute("normal", "2013");
	$publication_date->setAttribute("type", "publication");
	$publicationstmt->appendChild($publication_date);
	
// end of file description section
	
	
	
	
// beginning of profile description section
	
	
		
	$profiledesc = $xml->createElement("profiledesc");
	$eadheader->appendChild($profiledesc);
	
	$creation = $xml->createElement("creation", $arr_specific_ead_result['PUBLISHER']['PUBLISHER_DESC']);
	$profiledesc->appendChild($creation);
	$creation_date = $xml->createElement("date", $arr_specific_ead_result['PUBLISHER']['PUBLISHER_DATE']);
	$creation_date->setAttribute("normal", "19990101");
	$creation->appendChild($creation_date);
// description of how the finding aid was created
	
	
	$publication_language = $xml->createElement("langusage", "Description is in");
	$profiledesc->appendChild($publication_language);
	$publication_lang = $xml->createElement("language", $arr_specific_ead_result['PUBLISHER']['PUBLISHER_LANG']);
	$publication_lang->setAttribute("encodinganalog", "language");
	$publication_lang->setAttribute("langcode", "eng");
	$publication_language->appendChild($publication_lang);
	
	
// beginning of profile description section
	
	
// beginning of revision description section
	
	
	$revisiondesc = $xml->createElement("revisiondesc");
	$eadheader->appendChild($revisiondesc);
	
	$change = $xml->createElement("change");
	$change->setAttribute("audience", "internal");
	$revisiondesc->appendChild($change);
	
	$publication_revision_date = $xml->createElement("date", $arr_specific_ead_result['REVISION_DATE']);
	$publication_revision_date->setAttribute("normal", "19990101");
	$change->appendChild($publication_revision_date);
	
	$revision_item = $xml->createElement("item", $arr_specific_ead_result['REVISION_DESC']);
	$change->appendChild($revision_item);
	
	
// end of revision description section
	
	
// end of ead header
	
	
// beginning of archive description
	
	
// beginning of summary information

	$archdesc = $xml->createElement("archdesc");
	$archdesc->setAttribute("relatedencoding", "marc21");
	$archdesc->setAttribute("type", "inventory");
	$archdesc->setAttribute("audience", "external");
	$archdesc->setAttribute("level", "collection");
	$ead->appendChild($archdesc);
	
	$did = $xml->createElement("did");
	$archdesc->appendChild($did);
	
	$head = $xml->createElement("head", "Summary Information");
	$did->appendChild($head);
	
	$repository = $xml->createElement("repository");
	$repository->setAttribute("id", "archives");
	$repository->setAttribute("label", "Repository");
	$repository->setAttribute("encodinganalog", "850");
	$did->appendChild($repository);
	
	
	$summary_info_corp_name = $xml->createElement("corpname",$arr_specific_ead_result['SUMMARY_INFO_CORP_NAME']);
	$summary_info_corp_name->setAttribute("normal", "Loyola University, Chicago. University Archives and Special Collections");
	$summary_info_corp_name->setAttribute("source", "lcnaf");
	$repository->appendChild($summary_info_corp_name);
	
	$address = $xml->createElement("address");
	$repository->appendChild($address);
	
	
	$summary_info_addr = $xml->createElement("addressline",$arr_specific_ead_result['SUMMARY_INFO_ADDR']);
	$address->appendChild($summary_info_addr);
	
//  end of summary information
	
	
	
	
	$collection_title = $xml->createElement("unittitle",$arr_specific_ead_result['COLLECTION']['COLLECTION_TITLE']);
	$collection_title->setAttribute("label", "Title");
	$collection_title->setAttribute("encodinganalog", "245$ a");
	$did->appendChild($collection_title);
	
	
	
	if ($arr_specific_ead_result['COLLECTION']['COLLECTION_INCL'] != "") {
		$collection_incl = $xml->createElement("unitdate",$arr_specific_ead_result['COLLECTION']['COLLECTION_INCL']);
		$collection_incl->setAttribute("encodinganalog", "245$ f");
		$$collection_incl->setAttribute("type", "inclusive");
		$collection_incl->setAttribute("normal", "1999/2000");
		$did->appendChild($collection_incl);
		}
		
	if ($arr_specific_ead_result['COLLECTION']['COLLECTION_BULK'] != "") {
		$collection_inclb = $xml->createElement("unitdate",$arr_specific_ead_result['COLLECTION']['COLLECTION_INCL']);
		$collection_inclb->setAttribute("encodinganalog", "245$ g");
		$$collection_inclb->setAttribute("type", "bulk");
		$collection_inclb->setAttribute("normal", "1999/2000");
		$did->appendChild($collection_inclb);
	}

	
	$origination = $xml->createElement("origination");
	$origination->setAttribute("label", "Creator");
	$did->appendChild($origination);
	
	$collection_creator = $xml->createElement("corpname",$arr_specific_ead_result['COLLECTION']['COLLECTION_CREATOR']);
	$collection_creator->setAttribute("encodinganalog", "110");
	$collection_creator->setAttribute("source", "lcnaf");
	$origination->appendChild($collection_creator);
	
	$unitid = $xml->createElement("unitid",$arr_specific_ead_result['ID']);
	$unitid->setAttribute("label", "Accession No.");
	$unitid->setAttribute("encodinganalog", "099");
	$did->appendChild($unitid);
	
	$physical_description = $xml->createElement("physdesc");
	$did->appendChild($physical_description);
	
	$collection_linear_feet = $xml->createElement("extent",$arr_specific_ead_result['COLLECTION']['COLLECTION_LINEAR_FEET']);
	$physical_description->appendChild($collection_linear_feet);
	
	$collection_item_num = $xml->createElement("extent",$arr_specific_ead_result['COLLECTION']['COLLECTION_ITEM_NUM']);
	$physical_description->appendChild($collection_item_num);
	
	
	$collection_language = $xml->createElement("langmaterial", "Materials are in ");	
	$collection_language->setAttribute("label", "Language");
	$collection_language->setAttribute("encodinganalog", "546");
	$did->appendChild($collection_language);
	
	$collection_lang = $xml->createElement("language",$arr_specific_ead_result['COLLECTION']['COLLECTION_LANG']);	
	$collection_lang->setAttribute("langcode", "eng");
	$collection_language->appendChild($collection_lang);
	
	
	
// beginning of separated materials
	

		$seperated_material = $xml->createElement("separatedmaterial");
		$seperated_material->setAttribute("encodinganalog", "544");
		$seperated_material_title = $xml->createElement("head",$arr_specific_ead_result['SEPERATED_MATERIAL_TITLE']);
		$seperated_material_body = $xml->createElement("p",$arr_specific_ead_result['SEPERATED_MATERIAL_BODY']);

			$seperated_material->appendChild($seperated_material_title);

			$seperated_material->appendChild($seperated_material_body);

		$archdesc->appendChild($seperated_material);

// end of separated materials
	
	
	
// beginning of related materials	
	
	
	
	
		$related_material = $xml->createElement("relatedmaterial");
		$related_material_title = $xml->createElement("head",$arr_specific_ead_result['RELATED_MATERIAL_TITLE']);
		$related_material_body = $xml->createElement("p",$arr_specific_ead_result['RELATED_MATERIAL_BODY']);

		$related_material->appendChild($related_material_title);
		$related_material->appendChild($related_material_body);
		$archdesc->appendChild($related_material);
	
	
// end of related materials	
	
	
// beginning of administrative information	
	
	
	
		$descgrp = $xml->createElement("descgrp");
		$archdesc->appendChild($descgrp);
		
		$admin_info_head = $xml->createElement("head");
		$descgrp->appendChild($admin_info_head);
		
		
		
		$accurals = $xml->createElement("accruals");
		$accurals->setAttribute("encodinganalog", "584");
		$descgrp->appendChild($accurals);
				
		$accruals_head = $xml->createElement("head");
		$accurals->appendChild($accruals_head);
		
		$admin_info_accural = $xml->createElement("p",$arr_specific_ead_result['EAD_ADMIN']['ADMIN_INFO_ACCURAL']);
		
			$accurals->appendChild($admin_info_accural);
		
		$custodhist = $xml->createElement("custodhist");
		$custodhist->setAttribute("encodinganalog", "561");
		$descgrp->appendChild($custodhist);
				
		$prov_head = $xml->createElement("head");
		$custodhist->appendChild($prov_head);
		
		$admin_info_provenance = $xml->createElement("p",$arr_specific_ead_result['EAD_ADMIN']['ADMIN_INFO_PROVENANCE']);
		
			$custodhist->appendChild($admin_info_provenance);
		
		$acqinfo = $xml->createElement("acqinfo");
		$acqinfo->setAttribute("encodinganalog", "541");
		$descgrp->appendChild($acqinfo);
				
		$acq_head = $xml->createElement("head");
		$acqinfo->appendChild($acq_head);
		
		$admin_info_acquisition = $xml->createElement("p",$arr_specific_ead_result['EAD_ADMIN']['ADMIN_INFO_ACQUISITION']);
		
			$acqinfo->appendChild($admin_info_acquisition);
		
		$userestrict = $xml->createElement("userestrict");
		$userestrict->setAttribute("encodinganalog", "540");
		$descgrp->appendChild($userestrict);
				
		$userestrict_head = $xml->createElement("head");
		$userestrict->appendChild($userestrict_head);
		
		$admin_info_usage = $xml->createElement("p",$arr_specific_ead_result['EAD_ADMIN']['ADMIN_INFO_USAGE']);
		
			$userestrict->appendChild($admin_info_usage);
		
		
		$prefercite = $xml->createElement("prefercite");
		$prefercite->setAttribute("encodinganalog", "524");
		$descgrp->appendChild($prefercite);
				
		$prefercite_head = $xml->createElement("head");
		$prefercite->appendChild($prefercite_head);
		
		$admin_info_preferred_citation = $xml->createElement("p",$arr_specific_ead_result['EAD_ADMIN']['ADMIN_INFO_PREFERRED_CITATION']);
		
			$prefercite->appendChild($admin_info_preferred_citation);
		
		
		$accessrestrict = $xml->createElement("accessrestrict");
		$accessrestrict->setAttribute("encodinganalog", "524");
		$descgrp->appendChild($accessrestrict);
				
		$accessrestrict_head = $xml->createElement("head");
		$accessrestrict->appendChild($accessrestrict_head);
		
		$admin_info_access = $xml->createElement("p",$arr_specific_ead_result['EAD_ADMIN']['ADMIN_INFO_ACCESS']);
		
		
			$accessrestrict->appendChild($admin_info_access);
		
		
		$processinfo = $xml->createElement("processinfo");
		$processinfo->setAttribute("encodinganalog", "583");
		$descgrp->appendChild($processinfo);
				
		$processinfo_head = $xml->createElement("head");
		$processinfo->appendChild($processinfo_head);
			
		$admin_info_processing = $xml->createElement("p",$arr_specific_ead_result['EAD_ADMIN']['ADMIN_INFO_PROCESSING']);
		
			$processinfo->appendChild($admin_info_processing);
		
		
		$bioghist = $xml->createElement("bioghist");
		$bioghist->setAttribute("encodinganalog", "545");
		$descgrp->appendChild($bioghist);
				
		$bioghist_head = $xml->createElement("head");
		$bioghist->appendChild($bioghist_head);
			
		$admin_info_bio = $xml->createElement("p",$arr_specific_ead_result['EAD_ADMIN']['ADMIN_INFO_BIO']);

			$bioghist->appendChild($admin_info_bio);

// end  of administrative information	
	
	
// beginning of scope and content

		$scope_content = $xml->createElement("scopecontent");
		$scope_content->setAttribute("encodinganalog", "520");
		$descgrp->appendChild($scope_content);
		
		$scope_content_head = $xml->createElement("head");
		$scope_content->appendChild($scope_content_head);
			
		$scope_content_note = $xml->createElement("p",$arr_specific_ead_result['SCOPE_CONTENT_NOTE']);
		
			$scope_content->appendChild($scope_content_note);
		
		
		$arrangement = $xml->createElement("arrangement");
		$arrangement->setAttribute("encodinganalog", "520");
		$descgrp->appendChild($arrangement);
		
		$arrangement_head = $xml->createElement("head");
		$arrangement->appendChild($arrangement_head);
			
		
		$scope_content_arrangement = $xml->createElement("p",$arr_specific_ead_result['SCOPE_CONTENT_ARRANGEMENT']);
		
			$arrangement->appendChild($scope_content_arrangement);
		
		
		$access_control = $xml->createElement("controlaccess");
		$descgrp->appendChild($access_control);
		
		$controlaccess_head = $xml->createElement("head");
		$access_control->appendChild($controlaccess_head);
			
		
		

		/* Multiple Acess Control with Subject, name and corporate name */
		for ($i=0;$i<$max_count_access_control_array;$i++) {
			$access_control = $xml->createElement("access-control");
			if($access_control_title[$i] || $access_control_name[$i] || $access_control_corp_name[$i]) {
				if($access_control_title[$i]) {
					$access_control->setAttribute("subject",$access_control_title[$i]);
				}

				if($access_control_name[$i]) {
					$access_control->setAttribute("persname",$access_control_name[$i]);
				}

				if($access_control_corp_name[$i]) {
					$access_control->setAttribute("corpname",$access_control_corp_name[$i]);
				}
			}
			$descgrp->appendChild($access_control);
		}

// end of scope and content

	$dsc = $xml->createElement("dsc");
	$dsc->setAttribute("type", "combined");
	$archdesc->appendChild($dsc);

	$dsc_head = $xml->createElement("head");
	$dsc->appendChild($dsc_head);

	/* Container section begins */
	/* 
		Container type depends on TYPE value 
		1 -> class
		2 -> collection
		3 -> files
		4 -> item
		5 -> series

		Container date type depends on DATE_TYPE value
		1 -> N/A
		2 -> Bulk Date
		3 -> Inclusive Date
	*/
	$container_type = 0;
	$arr_container_content = $arr_specific_ead_result['CONTAINER'];
	
	if(is_array($arr_container_content)) {
		foreach ($arr_container_content as $container_row) {
			$container = $xml->createElement("container");
			if ($container_row['ID']) {
				$container->setAttribute("id",$container_row["ID"]);
			}

			if ($container_row['TYPE'] == "1") {
				$container->setAttribute("data-type","class");

			}
			else if ($container_row['TYPE'] == "2") {
				$container->setAttribute("type","collection");

			}
			else if ($container_row['TYPE'] == "3") {
				$container->setAttribute("type","files");
			}
			else if ($container_row['TYPE'] == "4") {
				$container->setAttribute("type","item");
			}
			else if ($container_row['TYPE'] == "5") {
				$container->setAttribute("type","series");
			}

			if ($container_row['DATE_TYPE'] == "1") {
				$container->setAttribute("date-type","N/A");
			}
			else if ($container_row['DATE_TYPE'] == "2") {
				$container->setAttribute("encodinganalog","245g");
			}
			else if ($container_row['DATE_TYPE'] == "3") {
				$container->setAttribute("encodinganalog","245f");
			}
			
			$container_title = $xml->createElement("container-title",$container_row['TITLE']);
			$container_lang = $xml->createElement("container-language",$container_row['LANGUAGE']);

			if ($container_row['TITLE']) {
				$container->appendChild($container_title);
			}
			if ($container_row['LANGUAGE']) {
				$container->appendChild($container_lang);
			}
			$ead->appendChild($container);
		}
	}
	/* Container section ends   */

	$xml->appendChild($ead);

	$xml->FormatOutput = true;
	$string_value = $xml->saveXML();
	$xml->save("isEad.xml");



	/* XML format generator ends   */
?>

<script type="text/javascript">
	$(document).ready(function(){
		setTimeout(function(){
			window.location.href = "http://localhost/archive/isEad.xml"}, 1000);
	})

</script>