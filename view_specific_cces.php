<div class="back-to-view-cces"><a href="/archive/view_cces.php">Back</a></div>
<div class="specific_cces_content">
	<h2>Identifiers:</h2>
	<div class="specific_ead_cces_id">
		<div class="specific_ead_cces_id_left specific_cces_left">
			CCES ID:
		</div>
		<div class="specific_ead_cces_id_right specific_cces_right">
			<?php echo $arr_specific_cces_result['CCESID']; ?>
		</div>
		<div class="clear"></div>
	</div>

	<?php if($arr_specific_cces_result['OTHERID']) { ?>
	<div class="specific_ead_other_id_no">
		<div class="specific_ead_other_id_no_left specific_cces_left">
			Other ID:
		</div>
		<div class="specific_ead_other_id_no_right specific_cces_right">
			<?php echo $arr_specific_cces_result['OTHERID']; ?>
		</div>
		<div class="clear"></div>
	</div>
	<?php } ?>

	<h2>Location Information:</h2>

	<?php if($arr_specific_cces_result['STATE']) { ?>
	<div class="specific_cces_content_state">
		<div class="specific_cces_content_state_left specific_cces_left">
			State:
		</div>
		<div class="specific_cces_content_state_right specific_cces_right">
			<?php echo $arr_specific_cces_result['STATE']; ?>
		</div>
		<div class="clear"></div>
	</div>
	<?php } ?>

	<?php if($arr_specific_cces_result['TOWN_OR_CITY']) { ?>
		<div class="specific_cces_content_town_or_city">
			<div class="specific_cces_content_town_or_city_left specific_cces_left">
				Town or City:
			</div>
			<div class="specific_cces_content_town_or_city_right specific_cces_right">
				<?php echo $arr_specific_cces_result['TOWN_OR_CITY']; ?>
			</div>
			<div class="clear"></div>
		</div>
	<?php } ?>

		
	<?php if($arr_specific_cces_result['DIOCESE']) { ?>
		<div class="specific_cces_content_diocese">
			<div class="specific_cces_content_diocese_left specific_cces_left">
				Diocese:
			</div>
			<div class="specific_cces_content_diocese_right specific_cces_right">
				<?php echo $arr_specific_cces_result['DIOCESE']; ?>
			</div>
			<div class="clear"></div>
		</div>
	<?php } ?>
	
		
	<?php if($arr_specific_cces_result['CHURCH']) { ?>
		<div class="specific_cces_content_church">
			<div class="specific_cces_content_church_left specific_cces_left">
				Church:
			</div>
			<div class="specific_cces_content_church_right specific_cces_right">
				<?php echo $arr_specific_cces_result['CHURCH']; ?>
			</div>
			<div class="clear"></div>
		</div>
	<?php } ?>
	
	<h2>Image Information:</h2>
		
	<?php if($arr_specific_cces_result['DATE']) { ?>
		<div class="specific_cces_content_date">
			<div class="specific_cces_content_date_left specific_cces_left">
				Date:
			</div>
			<div class="specific_cces_content_date_right specific_cces_right">
				<?php echo $arr_specific_cces_result['DATE']; ?>
			</div>
			<div class="clear"></div>
		</div>
	<?php } ?>
	
			
	<?php if($arr_specific_cces_result['DESCRIPTION']) { ?>
		<div class="specific_cces_content_description">
			<div class="specific_cces_content_description_left specific_cces_left">
				Description of Image:
			</div>
			<div class="specific_cces_content_description_right specific_cces_right">
				<?php echo $arr_specific_cces_result['DESCRIPTION']; ?>
			</div>
			<div class="clear"></div>
		</div>
	<?php } ?>
	
			
	<?php if($arr_specific_cces_result['TRANSCRIPTION']) { ?>
		<div class="specific_cces_content_transcription">
			<div class="specific_cces_content_transcription_left specific_cces_left">
				Transcription:
			</div>
			<div class="specific_cces_content_transcription_right specific_cces_right">
				<?php echo $arr_specific_cces_result['TRANSCRIPTION']; ?>
			</div>
			<div class="clear"></div>
		</div>
	<?php } ?>
	
			
	<?php if($arr_specific_cces_result['IMAGE_SIZE_L']) { ?>
		<div class="specific_cces_content_image_size_l">
			<div class="specific_cces_content_image_size_l_left specific_cces_left">
				Image Size (Length):
			</div>
			<div class="specific_cces_content_image_size_l_right specific_cces_right">
				<?php echo $arr_specific_cces_result['IMAGE_SIZE_L']; ?>
			</div>
			<div class="clear"></div>
		</div>
	<?php } ?>
	
				
	<?php if($arr_specific_cces_result['IMAGE_SIZE_W']) { ?>
		<div class="specific_cces_content_image_size_w">
			<div class="specific_cces_content_image_size_w_left specific_cces_left">
				Image Size (Width):
			</div>
			<div class="specific_cces_content_image_size_w_right specific_cces_right">
				<?php echo $arr_specific_cces_result['IMAGE_SIZE_W']; ?>
			</div>
			<div class="clear"></div>
		</div>
	<?php } ?>
	
			
	<?php if($arr_specific_cces_result['TYPE']) { ?>
		<div class="specific_cces_content_type">
			<div class="specific_cces_content_type_left specific_cces_left">
				Type:
			</div>
			<div class="specific_cces_content_type_right specific_cces_right">
				<?php echo $arr_specific_cces_result['TYPE']; ?>
			</div>
			<div class="clear"></div>
		</div>
	<?php } ?>

	<h2>Subjects:</h2>
	<?php 
	foreach ($arr_specific_cces_result['SUBJECTS'] as $row):
		if($row['NAME']) { ?>
			<div class="specific_ead_cces_name">
				<div class="specific_ead_cces_name_left specific_ead_left">
					
				</div>
				<div class="specific_ead_cces_name_left specific_ead_right specific_ead_right">
					<?php echo $row['NAME']; ?>
				</div>
				<div class="clear"></div>
			</div>
		<?php  }
	endforeach; ?>

	<h2>Image:</h2>

	<?php if($arr_specific_cces_result['FILENAME']) { ?>
		<div class="specific_cces_content_filename">
			<div class="specific_cces_content_filename_left specific_cces_left">
			</div>
			<div class="specific_cces_content_filename_right specific_cces_right">
				<img width="500" src="/archive/uploads/<?php echo $arr_specific_cces_result['FILENAME']; ?>" >
			</div>
			<div class="clear"></div>
		</div>
	<?php } ?>

</div>