<?php
	//include "header.php";
	session_start();
	if (isset($_SESSION['archive']['email'])){
		$email = $_SESSION['archive']['email'];
		track_logout_user($email);
	}
	unset($_SESSION['archive']['loggedIn']);
  unset($_SESSION['archive']['email']);
  unset($_SESSION['archive']['fname']);
  unset($_SESSION['archive']['lname']);
  unset($_SESSION['archive']['role']);
  header("Location: index.php");
	include "footer.php";

	function track_logout_user($email) {
		include $_SERVER['DOCUMENT_ROOT']. "/archive/connection.php";
		try {
	 		$con = new PDO("mysql:host=$db_host;dbname=$mysql_name", $db_user, $db_pass);
	 		$date = date("Y-m-d h:i:s");
			$sql = "INSERT INTO archive_log (DESCRIPTION, LOGGED_TIME) values (\"User logged out\",\"" .  $date . "\")";
			$stmt = $con->prepare($sql);
			$result = $stmt->execute();
		} catch (PDOException $e) {
			die("Error occurred:" . $e->getMessage());
		}
	}
?>

