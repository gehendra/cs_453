<?php if ($container_row['ID'] || $container_row['TYPE'] || $container_row['DATE_TYPE'] || $container_row['TITLE'] || $container_row['LANGUAGE']) {  ?>
	<?php if ($container_row['TYPE']) { ?>
		<div class="specific_ead_content_container_type">
			<div class="specific_ead_content_container_type_left specific_ead_left">
				Container Type:
			</div>
			<div class="specific_ead_content_container_type_right specific_ead_right">
				<?php if ($container_row['TYPE'] == 1) { ?>
					Class
				<?php } else if ($container_row['TYPE'] == 2) { ?>
					Collection
				<?php } else if ($container_row['TYPE'] == 3) { ?>
					Files
				<?php } else if ($container_row['TYPE'] == 4) { ?>
					Item
				<?php } else if ($container_row['TYPE'] == 5) { ?>
					Series
				<?php } ?>
			</div>
			<div class="clear"></div>
		</div>
	<?php } ?>

	<?php if ($container_row['ID']) { ?>
		<div class="specific_ead_content_container_id">
			<div class="specific_ead_content_container_id_left specific_ead_left">
				Accession Number:
			</div>
			<div class="specific_ead_content_container_id_right specific_ead_right">
				<?php echo $container_row['ID']; ?>
			</div>
			<div class="clear"></div>
		</div>
	<?php } ?>

	<?php if ($container_row['TITLE']) { ?>
		<div class="specific_ead_content_container_title">
			<div class="specific_ead_content_container_title_left specific_ead_left">
				Title:
			</div>
			<div class="specific_ead_content_container_title_right specific_ead_right">
				<?php echo $container_row['TITLE']; ?>
			</div>
			<div class="clear"></div>
		</div>
	<?php } ?>

	<?php if ($container_row['DATE_TYPE']) { ?>
		<div class="specific_ead_content_container_data_type">
			<div class="specific_ead_content_container_data_type_left specific_ead_left">
				Date Type:
			</div>
			<div class="specific_ead_content_container_data_type_right specific_ead_right">
				<?php if ($container_row['DATE_TYPE'] == 1) { ?>
					N/A
				<?php } else if ($container_row['DATE_TYPE'] == 2) { ?>
					Bulk Date
				<?php } else if ($container_row['DATE_TYPE'] == 3) { ?>
					Inclusive Date
				<?php } ?>
			</div>
			<div class="clear"></div>
		</div>
	<?php } ?>

	<?php if ($container_row['LANGUAGE']) { ?>
		<div class="specific_ead_content_container_language">
			<div class="specific_ead_content_container_language_left specific_ead_left">
				Language:
			</div>
			<div class="specific_ead_content_container_language_right specific_ead_right">
				<?php echo $container_row['LANGUAGE']?>
			</div>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	<?php } ?>	
<?php } ?>