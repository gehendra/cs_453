<?php
	$access_control_title = unserialize($arr_specific_ead_result['ACCESS_CONTROL']['ACCESS_CONTROL_TITLE']);
	$count_access_control_title = count($access_control_title);
	$access_control_name = unserialize($arr_specific_ead_result['ACCESS_CONTROL']['ACCESS_CONTROL_NAME']);
	$count_access_control_name = count($access_control_name);
	$access_control_corp_name = unserialize($arr_specific_ead_result['ACCESS_CONTROL']['ACCESS_CONTROL_CORP_NAME']);
	$count_access_control_corp_name = count($access_control_corp_name);

	$max_count_access_control_array = max ($count_access_control_title,$count_access_control_name,$count_access_control_corp_name);

// "Create" the document.
	$xml = new DOMDocument("1.0", "ISO-8859-15");
	$ead = $xml->createElement( "ead" );
	$ead->setAttribute("access_number", $arr_specific_ead_result['ID']);

	$title = $xml->createElement("title", $arr_specific_ead_result['TITLE']);
	$ead->appendChild($title);

	if($arr_specific_ead_result['SUBTITLE'] != "") {
		$sub_title = $xml->createElement("sub-title", $arr_specific_ead_result['SUBTITLE']);
		$ead->appendChild($sub_title);
	}
	if($arr_specific_ead_result['DATE'] != "") {
		$date = $xml->createElement("date", $arr_specific_ead_result['DATE']);
		$ead->appendChild($date);
	}
	if($arr_specific_ead_result['AUTHOR'] != "") {
		$author = $xml->createElement("author",$arr_specific_ead_result['AUTHOR']);
		$ead->appendChild($author);
	}

	if ($arr_specific_ead_result['PUBLISHER']['PUBLISHER'] || $arr_specific_ead_result['PUBLISHER']['PUBLISHER_DESC'] || $arr_specific_ead_result['PUBLISHER']['PUBLISHER_DATE'] || $arr_specific_ead_result['PUBLISHER']['PUBLISHER_LANG'] || $arr_specific_ead_result['PUBLISHER']['REVISION_DESC'] || $arr_specific_ead_result['PUBLISHER']['REVISION_DATE']) {
		$publication = $xml->createElement("publication");
		$publication_publisher = $xml->createElement("publisher", $arr_specific_ead_result['PUBLISHER']['PUBLISHER']);
		$publication_description = $xml->createElement("publication-description", $arr_specific_ead_result['PUBLISHER']['PUBLISHER_DESC']);
		$publication_date = $xml->createElement("publication-date", $arr_specific_ead_result['PUBLISHER']['PUBLISHER_DATE']);
		$publication_lang = $xml->createElement("publication-lang", $arr_specific_ead_result['PUBLISHER']['PUBLISHER_LANG']);
		$publication_revision_desc = $xml->createElement("revision-description", $arr_specific_ead_result['PUBLISHER']['REVISION_DESC']);
		$publication_revision_date = $xml->createElement("revision-date", $arr_specific_ead_result['PUBLISHER']['REVISION_DATE']);

		if ($arr_specific_ead_result['PUBLISHER']['PUBLISHER']) {
			$publication->appendChild($publication_publisher);
		}
		if ($arr_specific_ead_result['PUBLISHER']['PUBLISHER_DESC']) {
			$publication->appendChild($publication_description);
		}
		if ($arr_specific_ead_result['PUBLISHER']['PUBLISHER_DATE']) {
			$publication->appendChild($publication_date);
		}
		if ($arr_specific_ead_result['PUBLISHER']['PUBLISHER_LANG']) {
			$publication->appendChild($publication_lang);
		}
		if($arr_specific_ead_result['REVISION_DESC'] || $arr_specific_ead_result['REVISION_DATE']) {
			$revision = $xml->createElement("revision");
			if ($arr_specific_ead_result['REVISION_DESC']) {
				$revision->appendChild($publication_revision_desc);
			}
			if ($arr_specific_ead_result['REVISION_DATE']) {
				$revision->appendChild($publication_revision_date);
			}
			$publication->appendChild($revision);
		}
		$ead->appendChild($publication);
	}

	if ($arr_specific_ead_result['SUMMARY_INFO_DESC'] || $arr_specific_ead_result['SUMMARY_INFO_CORP_NAME'] || $arr_specific_ead_result['SUMMARY_INFO_ADDR']) {
		$summary_info = $xml->createElement("summary-information");
		$summary_info_desc = $xml->createElement("summary-info-desc",$arr_specific_ead_result['SUMMARY_INFO_DESC']);
		$summary_info_corp_name = $xml->createElement("summary-info-corp-name",$arr_specific_ead_result['SUMMARY_INFO_CORP_NAME']);
		$summary_info_addr = $xml->createElement("summary-info-address",$arr_specific_ead_result['SUMMARY_INFO_ADDR']);

		if($arr_specific_ead_result['SUMMARY_INFO_DESC']) {
			$summary_info->appendChild($summary_info_desc);
		}

		if($arr_specific_ead_result['SUMMARY_INFO_CORP_NAME']) {
			$summary_info->appendChild($summary_info_corp_name);
		}

		if($arr_specific_ead_result['SUMMARY_INFO_ADDR']) {
			$summary_info->appendChild($summary_info_addr);
		}
		$ead->appendChild($summary_info);
	}

	if ($arr_specific_ead_result['COLLECTION']['COLLECTION_TITLE'] || $arr_specific_ead_result['COLLECTION']['COLLECTION_INCL'] || $arr_specific_ead_result['COLLECTION']['COLLECTION_BULK'] || $arr_specific_ead_result['COLLECTION']['COLLECTION_CREATOR'] || $arr_specific_ead_result['COLLECTION']['COLLECTION_LINEAR_FEET'] || $arr_specific_ead_result['COLLECTION']['COLLECTION_ITEM_NUM'] || $arr_specific_ead_result['COLLECTION']['COLLECTION_LANG'] || $arr_specific_ead_result['COLLECTION']['COLLECTION_NON_ENG']) {
		$collection = $xml->createElement("collection");
		$collection_title = $xml->createElement("collection-title",$arr_specific_ead_result['COLLECTION']['COLLECTION_TITLE']);
		$collection_incl = $xml->createElement("collection-inclusive-date",$arr_specific_ead_result['COLLECTION']['COLLECTION_INCL']);
		$collection_bulk = $xml->createElement("collection-bulk-date",$arr_specific_ead_result['COLLECTION']['COLLECTION_BULK']);
		$collection_creator = $xml->createElement("collection-creator",$arr_specific_ead_result['COLLECTION']['COLLECTION_CREATOR']);
		$collection_linear_feet = $xml->createElement("collection-linear-feet",$arr_specific_ead_result['COLLECTION']['COLLECTION_LINEAR_FEET']);
		$collection_item_num = $xml->createElement("collection-item-number",$arr_specific_ead_result['COLLECTION']['COLLECTION_ITEM_NUM']);
		$collection_lang = $xml->createElement("collection-language",$arr_specific_ead_result['COLLECTION']['COLLECTION_LANG']);
		$collection_non_eng = $xml->createElement("collection-non-eng",$arr_specific_ead_result['COLLECTION']['COLLECTION_NON_ENG']);

		if ($arr_specific_ead_result['COLLECTION']['COLLECTION_TITLE']) {
			$collection->appendChild($collection_title);
		}

		if ($arr_specific_ead_result['COLLECTION']['COLLECTION_INCL']) {
			$collection->appendChild($collection_incl);
		}

		if ($arr_specific_ead_result['COLLECTION']['COLLECTION_BULK']) {
			$collection->appendChild($collection_bulk);
		}

		if ($arr_specific_ead_result['COLLECTION']['COLLECTION_CREATOR']) {
			$collection->appendChild($collection_creator);
		}

		if ($arr_specific_ead_result['COLLECTION']['COLLECTION_LINEAR_FEET']) {
			$collection->appendChild($collection_linear_feet);
		}

		if ($arr_specific_ead_result['COLLECTION']['COLLECTION_ITEM_NUM']) {
			$collection->appendChild($collection_item_num);
		}

		if ($arr_specific_ead_result['COLLECTION']['COLLECTION_LANG']) {
			$collection->appendChild($collection_lang);
		}

		if ($arr_specific_ead_result['COLLECTION']['COLLECTION_NON_ENG']) {
			$collection->appendChild($collection_non_eng);
		}
		$ead->appendChild($collection);
	}

	if ($arr_specific_ead_result['SEPERATED_MATERIAL_TITLE'] || $arr_specific_ead_result['SEPERATED_MATERIAL_BODY']) {

		$related_material = $xml->createElement("related-material");
		$related_material_title = $xml->createElement("related-material-title",$arr_specific_ead_result['SEPERATED_MATERIAL_TITLE']);
		$related_material_body = $xml->createElement("related-material-body",$arr_specific_ead_result['SEPERATED_MATERIAL_BODY']);

		if ($arr_specific_ead_result['SEPERATED_MATERIAL_TITLE']) {
			$related_material->appendChild($related_material_title);

		}

		if ($arr_specific_ead_result['SEPERATED_MATERIAL_BODY']) {
			$related_material->appendChild($related_material_body);
		}

		$ead->appendChild($related_material);
	}

	if ($arr_specific_ead_result['RELATED_MATERIAL_TITLE'] || $arr_specific_ead_result['RELATED_MATERIAL_BODY']) {

		$seperated_material = $xml->createElement("seperated-material");
		$seperated_material_title = $xml->createElement("seperated-material-title",$arr_specific_ead_result['RELATED_MATERIAL_TITLE']);
		$seperated_material_body = $xml->createElement("seperated-material-body",$arr_specific_ead_result['RELATED_MATERIAL_BODY']);

		if ($arr_specific_ead_result['RELATED_MATERIAL_TITLE']) {
			$seperated_material->appendChild($seperated_material_title);
		}

		if ($arr_specific_ead_result['RELATED_MATERIAL_BODY']) {
			$seperated_material->appendChild($seperated_material_body);
		}

		$ead->appendChild($seperated_material);
	}

	if ($arr_specific_ead_result['EAD_ADMIN']['ADMIN_INFO_ACCURAL'] || $arr_specific_ead_result['EAD_ADMIN']['ADMIN_INFO_PROVENANCE'] || $arr_specific_ead_result['EAD_ADMIN']['ADMIN_INFO_ACQUISITION'] || $arr_specific_ead_result['EAD_ADMIN']['ADMIN_INFO_USAGE'] || $arr_specific_ead_result['EAD_ADMIN']['ADMIN_INFO_PREFERRED_CITATION'] || $arr_specific_ead_result['EAD_ADMIN']['ADMIN_INFO_ACCESS'] || $arr_specific_ead_result['EAD_ADMIN']['ADMIN_INFO_PROCESSING'] || $arr_specific_ead_result['EAD_ADMIN']['ADMIN_INFO_BIO']) {

		$admin_info = $xml->createElement("administration-information");
		$admin_info_accural = $xml->createElement("administration-information-accural",$arr_specific_ead_result['EAD_ADMIN']['ADMIN_INFO_ACCURAL']);
		$admin_info_provenance = $xml->createElement("administration-information-provenance",$arr_specific_ead_result['EAD_ADMIN']['EAD_ADMIN']['ADMIN_INFO_PROVENANCE']);
		$admin_info_acquisition = $xml->createElement("administration-information-acquisition",$arr_specific_ead_result['EAD_ADMIN']['ADMIN_INFO_ACQUISITION']);
		$admin_info_usage = $xml->createElement("administration-information-usage",$arr_specific_ead_result['EAD_ADMIN']['ADMIN_INFO_USAGE']);
		$admin_info_preferred_citation = $xml->createElement("administration-information-citation",$arr_specific_ead_result['EAD_ADMIN']['ADMIN_INFO_PREFERRED_CITATION']);
		$admin_info_access = $xml->createElement("administration-information-access",$arr_specific_ead_result['EAD_ADMIN']['ADMIN_INFO_ACCESS']);
		$admin_info_processing = $xml->createElement("administration-information-processing",$arr_specific_ead_result['EAD_ADMIN']['ADMIN_INFO_PROCESSING']);
		$admin_info_bio = $xml->createElement("administration-information-bio",$arr_specific_ead_result['EAD_ADMIN']['ADMIN_INFO_BIO']);

		if ($arr_specific_ead_result['EAD_ADMIN']['ADMIN_INFO_ACCURAL']) {
			$admin_info->appendChild($admin_info_accural);

		}

		if ($arr_specific_ead_result['EAD_ADMIN']['ADMIN_INFO_PROVENANCE']) {
			$admin_info->appendChild($admin_info_provenance);
		}

		if ($arr_specific_ead_result['EAD_ADMIN']['ADMIN_INFO_ACQUISITION']) {
			$admin_info->appendChild($admin_info_acquisition);
		}

		if ($arr_specific_ead_result['EAD_ADMIN']['ADMIN_INFO_USAGE']) {
			$admin_info->appendChild($admin_info_usage);
		}

		if ($arr_specific_ead_result['EAD_ADMIN']['ADMIN_INFO_PREFERRED_CITATION']) {
			$admin_info->appendChild($admin_info_preferred_citation);
		}

		if ($arr_specific_ead_result['EAD_ADMIN']['ADMIN_INFO_ACCESS']) {
			$admin_info->appendChild($admin_info_access);
		}

		if ($arr_specific_ead_result['EAD_ADMIN']['ADMIN_INFO_PROCESSING']) {
			$admin_info->appendChild($admin_info_processing);
		}

		if ($arr_specific_ead_result['EAD_ADMIN']['ADMIN_INFO_BIO']) {
			$admin_info->appendChild($admin_info_bio);
		}

		$ead->appendChild($admin_info);
	}

	/* Scope and content begins */
	if ($arr_specific_ead_result['SCOPE_CONTENT_NOTE'] || $arr_specific_ead_result['SCOPE_CONTENT_ARRANGEMENT']) {
		$scope_content = $xml->createElement("scope-content");
		$scope_content_note = $xml->createElement("scope-content-note",$arr_specific_ead_result['SCOPE_CONTENT_NOTE']);
		$scope_content_arrangement = $xml->createElement("scope-content-arrangement",$arr_specific_ead_result['SCOPE_CONTENT_ARRANGEMENT']);
		$access_control = $xml->createElement("access-control");

		if ($arr_specific_ead_result['SCOPE_CONTENT_NOTE']) {
			$scope_content->appendChild($scope_content_note);
		}

		if ($arr_specific_ead_result['SCOPE_CONTENT_ARRANGEMENT']) {
			$scope_content->appendChild($scope_content_arrangement);
		}

		/* Multiple Acess Control with Subject, name and corporate name */
		for ($i=0;$i<$max_count_access_control_array;$i++) {
			$access_control = $xml->createElement("access-control");
			if($access_control_title[$i] || $access_control_name[$i] || $access_control_corp_name[$i]) {
				if($access_control_title[$i]) {
					$access_control->setAttribute("title",$access_control_title[$i]);
				}

				if($access_control_name[$i]) {
					$access_control->setAttribute("name",$access_control_name[$i]);
				}

				if($access_control_corp_name[$i]) {
					$access_control->setAttribute("corporate-name",$access_control_corp_name[$i]);
				}
			}
			$scope_content->appendChild($access_control);
		}

		$ead->appendChild($scope_content);
	}
	/* Scope and content ends   */

	/* Container section begins */
	/* 
		Container type depends on TYPE value 
		1 -> class
		2 -> collection
		3 -> files
		4 -> item
		5 -> series

		Container date type depends on DATE_TYPE value
		1 -> N/A
		2 -> Bulk Date
		3 -> Inclusive Date
	*/
	$container_type = 0;
	$arr_container_content = $arr_specific_ead_result['CONTAINER'];
	
	if(is_array($arr_container_content)) {
		foreach ($arr_container_content as $container_row) {
			$container = $xml->createElement("container");
			if ($container_row['ID']) {
				$container->setAttribute("id",$container_row["ID"]);
			}

			if ($container_row['TYPE'] == "1") {
				$container->setAttribute("data-type","class");

			}
			else if ($container_row['TYPE'] == "2") {
				$container->setAttribute("type","collection");

			}
			else if ($container_row['TYPE'] == "3") {
				$container->setAttribute("type","files");
			}
			else if ($container_row['TYPE'] == "4") {
				$container->setAttribute("type","item");
			}
			else if ($container_row['TYPE'] == "5") {
				$container->setAttribute("type","series");
			}

			if ($container_row['DATE_TYPE'] == "1") {
				$container->setAttribute("date-type","N/A");
			}
			else if ($container_row['DATE_TYPE'] == "2") {
				$container->setAttribute("encodinganalog","245g");
			}
			else if ($container_row['DATE_TYPE'] == "3") {
				$container->setAttribute("encodinganalog","245f");
			}
			
			$container_title = $xml->createElement("container-title",$container_row['TITLE']);
			$container_lang = $xml->createElement("container-language",$container_row['LANGUAGE']);

			if ($container_row['TITLE']) {
				$container->appendChild($container_title);
			}
			if ($container_row['LANGUAGE']) {
				$container->appendChild($container_lang);
			}
			$ead->appendChild($container);
		}
	}
	/* Container section ends   */

	$xml->appendChild($ead);

	$xml->FormatOutput = true;
	$string_value = $xml->saveXML();
	$xml->save("ead.xml");



	/* XML format generator ends   */
?>

<script type="text/javascript">
	$(document).ready(function(){
		setTimeout(function(){
			window.location.href = "http://localhost:8888/archive/ead.xml"}, 1000);
	});

</script>