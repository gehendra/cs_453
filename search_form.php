<h2 style="margin-top:0">Encoded Archival Description</h2>
<legend legend style="margin-bottom:10">Search Form</legend>
<form role="form" name="frmUpdateEAD" method="post" enctype="multipart/form-data" onsubmit="return isValidForm();">
	<div class="form-group" id="ead_title">
		<label for="Title">Title</label>
		<div class="ead_right">
		  <input class="form-control" type="text" id="txt_ead_title" name="txt_ead_title" placeholder="Enter the title"/>
		</div>
	</div>

	<div class="form-group" id="ead_subtitle">
		<label for="Subtitle">Subtitle</label>
		<div class="ead_right">
			<input class="form-control" type="text" id="txt_ead_subtitle" name="txt_ead_subtitle" placeholder="Enter the Subtitle" />
		</div>
	</div>

	<div class="form-group" id="ead_subtitle">
		<label for="Author">Author</label>
		<div class="ead_right">
			<input class="form-control" type="text" id="txt_ead_author" name="txt_ead_author" placeholder="Enter the authors" />
		</div>
	</div>
	<input type="submit" name="submit" id="submit" value="Search" class="btn btn-success"/>
</form>