<?php
	session_start();
	if (!isset($_SESSION['archive']['email']) && $_SESSION['archive']['role'] != "1") {
		header("Location: /archive/index.php");
	}
	if (isset($_POST['delete_ead']) && isset($_POST['ead_id']) && $_POST['ead_id'] != "") {
		$ead_id = intval($_POST['ead_id']);
		delete_ead_by_id($ead_id);
		delete_access_control_by_ead_id($ead_id);
		delete_collection_by_ead_id($ead_id);
		delete_ead_admin_by_ead_id($ead_id);
		delete_publisher_by_ead_id($ead_id);
		delete_container_by_ead_id($ead_id);
		header("Location: " . "/archive/view.php");
	}

	function delete_ead_by_id($ead_id) {
		include $_SERVER['DOCUMENT_ROOT']. "/archive/connection.php";
		try {
			if ($ead_id) {
				$con = new PDO("mysql:host=$db_host;dbname=$mysql_name", $db_user, $db_pass);
				$sql = "CALL delete_ead_by_id(:ead_id)";
				$stmt = $con->prepare($sql);
				$stmt->bindParam(':ead_id', intval($ead_id), PDO::PARAM_INT);
				$stmt->execute();
			}
		}
		catch (PDOException $e)
	  {
	    $error = 'Dattabase connection error.' ;
	    exit();
	  }
	}
	function delete_access_control_by_ead_id($ead_id) {
		include $_SERVER['DOCUMENT_ROOT']. "/archive/connection.php";
		try {
			if ($ead_id) {
				$con = new PDO("mysql:host=$db_host;dbname=$mysql_name", $db_user, $db_pass);
				$sql = "CALL delete_access_control_by_ead_id(:ead_id)";
				$stmt = $con->prepare($sql);
				$stmt->bindParam(':ead_id', intval($ead_id), PDO::PARAM_INT);
				$stmt->execute();
			}
		}
		catch (PDOException $e)
	  {
	    $error = 'Dattabase connection error.' ;
	    exit();
	  }
	}
	function delete_collection_by_ead_id($ead_id) {
		include $_SERVER['DOCUMENT_ROOT']. "/archive/connection.php";
		try {
			if ($ead_id) {
				$con = new PDO("mysql:host=$db_host;dbname=$mysql_name", $db_user, $db_pass);
				$sql = "CALL delete_collection_by_ead_id(:ead_id)";
				$stmt = $con->prepare($sql);
				$stmt->bindParam(':ead_id', intval($ead_id), PDO::PARAM_INT);
				$stmt->execute();
			}
		}
		catch (PDOException $e)
	  {
	    $error = 'Dattabase connection error.' ;
	    exit();
	  }
	}
	function delete_ead_admin_by_ead_id($ead_id) {
		include $_SERVER['DOCUMENT_ROOT']. "/archive/connection.php";
		try {
			if ($ead_id) {
				$con = new PDO("mysql:host=$db_host;dbname=$mysql_name", $db_user, $db_pass);
				$sql = "CALL delete_ead_admin_by_ead_id(:ead_id)";
				$stmt = $con->prepare($sql);
				$stmt->bindParam(':ead_id', intval($ead_id), PDO::PARAM_INT);
				$stmt->execute();
			}
		}
		catch (PDOException $e)
	  {
	    $error = 'Dattabase connection error.' ;
	    exit();
	  }
	}
	
	function delete_publisher_by_ead_id($ead_id) {
		include $_SERVER['DOCUMENT_ROOT']. "/archive/connection.php";
		try {
			if ($ead_id) {
				$con = new PDO("mysql:host=$db_host;dbname=$mysql_name", $db_user, $db_pass);
				$sql = "CALL delete_publisher_by_ead_id(:ead_id)";
				$stmt = $con->prepare($sql);
				$stmt->bindParam(':ead_id', intval($ead_id), PDO::PARAM_INT);
				$stmt->execute();
			}
		}
		catch (PDOException $e)
	  {
	    $error = 'Dattabase connection error.' ;
	    exit();
	  }
	}
	function delete_container_by_ead_id($ead_id) {
		include $_SERVER['DOCUMENT_ROOT']. "/archive/connection.php";
		try {
			if ($ead_id) {
				$con = new PDO("mysql:host=$db_host;dbname=$mysql_name", $db_user, $db_pass);
				$sql = "CALL delete_container_by_ead_id(:ead_id)";
				$stmt = $con->prepare($sql);
				$stmt->bindParam(':ead_id', intval($ead_id), PDO::PARAM_INT);
				$stmt->execute();
			}
		}
		catch (PDOException $e)
	  {
	    $error = 'Dattabase connection error.' ;
	    exit();
	  }
	}

?>