$(document).ready(function (){
	/* Access Control Section Begins */
	var delete_access_control = '<button class="btn btn-primary remove_access_control">Delete</button><br /><br />';
	$('.add_access_control').click(function(){
		$(".access-control-container:last").after($("div.access-control-container:first").clone());
		$(".access-control-container:last").append(delete_access_control);
	});
	$(".remove_access_control").live('click',function(){
		$(this).closest("div.access-control-container").remove();
	});
	/* Access Control Section Ends   */
	/* container section begins */
	var delete_container = '<button class="btn btn-primary remove_container">Delete</button>';
	$('.add_container_type').click(function(){
		$(".container_type_container:last").after($("div.container_type_container:first").clone());
		$(".container_type_container:last").append(delete_container);
	});
	$(".remove_container").live('click',function() {
		$(this).closest("div.container_type_container").remove();
	});

	/* container section ends   */
});

