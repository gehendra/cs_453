<?php
session_start();
if(!isset($_SESSION['archive']['email']))
{
	header("Location: index.php");
}
?>
<?php
	if (isset($_POST['upd_submit']) && isset($_POST['ead_id']) && $_POST['ead_id'] != "" && isset($_POST['txt_ead_title'])) {
		include "connection.php";
		try {
			$con = new PDO("mysql:host=$db_host;dbname=$mysql_name", $db_user, $db_pass);
			$ID = $_POST['ead_id'];
			$TITLE = $_POST['txt_ead_title'];
			$SUBTITLE = $_POST['txt_ead_subtitle'];
			$DATE = $_POST['txt_ead_date'];
			$AUTHOR = $_POST['txt_ead_author'];
			$PUBLISHER = $_POST['txt_ead_publisher_description_publisher'];
			$PUBLISHER_DESC = $_POST['txt_ead_publisher_description_creation'];
			$PUBLISHER_DATE = $_POST['txt_ead_publisher_description_date'];
			$PUBLISHER_LANG = $_POST['txt_ead_publisher_description_lang'];
			$REVISION_DESC = $_POST['txt_ead_publisher_revision_desc'];
			$REVISION_DATE = $_POST['txt_ead_publisher_revision_date'];
			$SUMMARY_INFO_DESC = $_POST['txt_ead_summary_info_desc'];
			$SUMMARY_INFO_CORP_NAME = $_POST['txt_ead_summary_info_corp_name'];
			$SUMMARY_INFO_ADDR = $_POST['txt_ead_summary_info_addr'];
			$COLLECTION_TITLE = $_POST['txt_ead_collection_title'];
			$COLLECTION_INCL = $_POST['txt_ead_collection_inclusive_date'];
			$COLLECTION_BULK = $_POST['txt_ead_collection_bulk_date'];
			$COLLECTION_CREATOR = $_POST['txt_ead_collection_creator'];
			$COLLECTION_LINEAR_FEET = $_POST['txt_ead_collection_linear_feet_1'];
			$COLLECTION_ITEM_NUM = $_POST['txt_ead_collection_linear_number'];
			$COLLECTION_LANG = $POST['txt_ead_collection_material_in'];

			$chk_ead_collection_non_english = 0;
			if ($_POST['chk_ead_collection_non_english'])
			{
				$chk_ead_collection_non_english = 1;
			}

			$COLLECTION_NON_ENG = $chk_ead_collection_non_english;
			$SEPERATED_MATERIAL_TITLE = $_POST['txt_ead_seperated_head'];
			$SEPERATED_MATERIAL_BODY = $_POST['txt_ead_seperated_body'];
			$RELATED_MATERIAL_TITLE = $_POST['txt_ead_related_head'];
			$RELATED_MATERIAL_BODY = $_POST['txt_ead_related_body'];
			$ADMIN_INFO_ACCURAL = $_POST['txt_ead_admin_info_acc_info'];
			$ADMIN_INFO_PROVENANCE = $_POST['txt_ead_admin_info_prov_info'];
			$ADMIN_INFO_ACQUISITION = $_POST['txt_ead_admin_info_acq_info'];
			$ADMIN_INFO_USAGE = $_POST['txt_ead_admin_info_usage_restriction'];
			$ADMIN_INFO_PREFERRED_CITATION = $_POST['txt_ead_admin_info_pref_citation'];
			$ADMIN_INFO_ACCESS = $_POST['txt_ead_admin_info_access_restrictions'];
			$ADMIN_INFO_PROCESSING = $_POST['txt_ead_admin_info_processing_info'];
			$ADMIN_INFO_BIO = $_POST['txt_ead_admin_info_bio_info'];
			$SCOPE_CONTENT_NOTE = $_POST['txt_ead_scope_content_note'];
			$SCOPE_CONTENT_ARRANGEMENT = $_POST['txt_ead_scope_content_arrangement_note'];

			// Multiple Access Control Handler Begins
			$txt_ead_access_control_subject = "";
			$txt_ead_access_control_name = "";
			$txt_ead_access_control_corp_name = "";
			if($_POST['txt_ead_access_control_subject']) {
				$txt_ead_access_control_subject = serialize($_POST['txt_ead_access_control_subject']);
			}
			if($_POST['txt_ead_access_control_name']) {
				$txt_ead_access_control_name = serialize($_POST['txt_ead_access_control_name']);
			}
			if($_POST['txt_ead_access_control_corp_name']) {
				$txt_ead_access_control_corp_name = serialize($_POST['txt_ead_access_control_corp_name']);
			}
			// Multiple Access Control Handler Ends

			$ACCESS_CONTROL_TITLE = $txt_ead_access_control_subject;
			$ACCESS_CONTROL_NAME = $txt_ead_access_control_name;
			$ACCESS_CONTROL_CORP_NAME = $txt_ead_access_control_corp_name;

			$sql = "CALL update_ead_by_id(:id, :title, :subtitle, :ead_date, :author, :publisher, :publisher_desc, :publisher_date, :publisher_lang, :revision_desc, :revision_date, :summary_info_desc, :summary_info_corp_name, :summary_info_addr, :collection_title, :collection_incl, :collection_bulk, :collection_creator, :collection_linear_feet, :collection_item_num, :collection_lang, :collection_non_eng, :seperated_material_title, :seperated_material_body, :related_material_title, :related_material_body, :admin_info_accural, :admin_info_provenance, :admin_info_acquisition, :admin_info_usage, :admin_info_preferred_citation, :admin_info_access, :admin_info_processing, :admin_info_bio, :scope_content_note, :scope_content_arrangement, :access_control_title, :access_control_name, :access_control_corp_name)";
			$stmt = $con->prepare($sql);
			$stmt->bindParam(':id', intval($ID), PDO::PARAM_INT);
			$stmt->bindParam(':title', $TITLE, PDO::PARAM_STR);
			$stmt->bindParam(':subtitle', $SUBTITLE, PDO::PARAM_STR);
			$stmt->bindParam(':ead_date', $DATE, PDO::PARAM_STR);
			$stmt->bindParam(':author', $AUTHOR, PDO::PARAM_STR);
			$stmt->bindParam(':publisher', $PUBLISHER, PDO::PARAM_STR);
			$stmt->bindParam(':publisher_desc', $PUBLISHER_DESC, PDO::PARAM_STR);
			$stmt->bindParam(':publisher_date', $PUBLISHER_DATE, PDO::PARAM_STR);
			$stmt->bindParam(':publisher_lang', $PUBLISHER_LANG, PDO::PARAM_STR);
			$stmt->bindParam(':revision_desc', $REVISION_DESC, PDO::PARAM_STR);
			$stmt->bindParam(':revision_date', $REVISION_DATE, PDO::PARAM_STR);
			$stmt->bindParam(':summary_info_desc', $SUMMARY_INFO_DESC, PDO::PARAM_STR);
			$stmt->bindParam(':summary_info_corp_name', $SUMMARY_INFO_CORP_NAME, PDO::PARAM_STR);
			$stmt->bindParam(':summary_info_addr', $SUMMARY_INFO_ADDR, PDO::PARAM_STR);
			$stmt->bindParam(':collection_title', $COLLECTION_TITLE, PDO::PARAM_STR);
			$stmt->bindParam(':collection_incl', $COLLECTION_INCL, PDO::PARAM_STR);
			$stmt->bindParam(':collection_bulk', $COLLECTION_BULK, PDO::PARAM_STR);
			$stmt->bindParam(':collection_creator', $COLLECTION_CREATOR, PDO::PARAM_STR);
			$stmt->bindParam(':collection_linear_feet', $COLLECTION_LINEAR_FEET, PDO::PARAM_STR);
			$stmt->bindParam(':collection_item_num', $COLLECTION_ITEM_NUM, PDO::PARAM_STR);
			$stmt->bindParam(':collection_lang', $COLLECTION_LANG, PDO::PARAM_STR);
			$stmt->bindParam(':collection_non_eng', intval($COLLECTION_NON_ENG), PDO::PARAM_INT);
			$stmt->bindParam(':seperated_material_title', $SEPERATED_MATERIAL_TITLE, PDO::PARAM_STR);
			$stmt->bindParam(':seperated_material_body', $SEPERATED_MATERIAL_BODY, PDO::PARAM_STR);
			$stmt->bindParam(':related_material_title', $RELATED_MATERIAL_TITLE, PDO::PARAM_STR);
			$stmt->bindParam(':related_material_body', $RELATED_MATERIAL_BODY, PDO::PARAM_STR);
			$stmt->bindParam(':admin_info_accural', $ADMIN_INFO_ACCURAL, PDO::PARAM_STR);
			$stmt->bindParam(':admin_info_provenance', $ADMIN_INFO_PROVENANCE, PDO::PARAM_STR);
			$stmt->bindParam(':admin_info_acquisition', $ADMIN_INFO_ACQUISITION, PDO::PARAM_STR);
			$stmt->bindParam(':admin_info_usage', $ADMIN_INFO_USAGE, PDO::PARAM_STR);
			$stmt->bindParam(':admin_info_preferred_citation', $ADMIN_INFO_PREFERRED_CITATION, PDO::PARAM_STR);
			$stmt->bindParam(':admin_info_access', $ADMIN_INFO_ACCESS, PDO::PARAM_STR);
			$stmt->bindParam(':admin_info_processing', $ADMIN_INFO_PROCESSING, PDO::PARAM_STR);
			$stmt->bindParam(':admin_info_bio', $ADMIN_INFO_BIO, PDO::PARAM_STR);
			$stmt->bindParam(':scope_content_note', $SCOPE_CONTENT_NOTE, PDO::PARAM_STR);
			$stmt->bindParam(':scope_content_arrangement', $SCOPE_CONTENT_ARRANGEMENT, PDO::PARAM_STR);
			$stmt->bindParam(':access_control_title', $ACCESS_CONTROL_TITLE, PDO::PARAM_STR);
			$stmt->bindParam(':access_control_name', $ACCESS_CONTROL_NAME, PDO::PARAM_STR);
			$stmt->bindParam(':access_control_corp_name', $ACCESS_CONTROL_CORP_NAME, PDO::PARAM_STR);
			$result = $stmt->execute();
			if ($result) {
				header("Location: view.php");
			}
		}
		catch (PDOException $e) {
    $error = 'Database connection issue.' ;
    exit();
  	}
	}
?>

<?php include "header.php";
	if (isset($_POST['submit']) && isset($_POST['ead_id']) && $_POST['ead_id']!="") {
		$ead_id = $_POST['ead_id'];
		$ead_container_details = ead_info($ead_id);
	}
?>

<div class="content">
	<?php include $_SERVER['DOCUMENT_ROOT']. "/archive/update_ead/update_ead_form.php";?>
</div>
<?php include "footer.php"; ?>

<?php
function ead_info($view_specific_ead) {
	include "connection.php";
	try {
		$con = new PDO("mysql:host=$db_host;dbname=$mysql_name", $db_user, $db_pass);
		$sql = "CALL get_specific_ead(:id)";
		$stmt = $con->prepare($sql);
		$stmt->bindParam(':id', intval($view_specific_ead), PDO::PARAM_INT);
		$result = $stmt->execute();
		$arr_ead_result = $stmt->fetchAll();
		$arr_ead_result = $arr_ead_result[0];

		$con = new PDO("mysql:host=$db_host;dbname=$mysql_name", $db_user, $db_pass);
		$sql = "CALL get_ead_container(:container_id)";
		$stmt = $con->prepare($sql);
		$stmt->bindParam(':container_id', intval($view_specific_ead), PDO::PARAM_INT);
		$stmt->execute();
		$arr_ead_result_container = $stmt->fetchAll();
		$arr_ead_result['CONTAINER'] = $arr_ead_result_container;
		$arr_ead_result['CONTAINER_COUNT'] = count($arr_ead_result_container);
	}
	catch (PDOException $e) {
    $error = 'Database connection issue.' ;
    exit();
  }
	return $arr_ead_result;
}
?>