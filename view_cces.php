<?php include "header.php";?>
	<?php
		$view_specific_cces = "";
		$is_xml_cces = "";
		$display_cces = "";

		if (isset($_GET['cces'])) {
			$view_specific_cces = $_GET['cces'];
		}
		
		if (isset($_GET['xml'])) {
			$is_xml_cces = $_GET['xml'];
		}

		if (isset($_GET['isCCES'])) {
			$display_cces = $_GET['isCCES'];
		}
		
	?>
	<div class="content">
		<div class="form-content">
			<div class="view_cces_content">
				<?php
					if($view_specific_cces != "" && $is_xml_cces == "") {
						$arr_specific_cces_result = view_cces($view_specific_cces);
						include "view_specific_cces.php";
					}
					else if ($is_xml_cces != "" &&  $display_cces == "" && $view_specific_cces == "") {
						$arr_specific_cces_result = view_cces($is_xml_cces);
						include "view_xml_cces.php";
					}
					else if ($display_cces != "" && $is_xml_cces == "" && $view_specific_cces == "") {
						$arr_specific_cces_result = view_cces($display_cces);
						include "view_xml_cces_test.php";
					}
					else {
						$arr_cces_result = array();
						$arr_cces_result = view_all_cces();
						include "view_all_cces.php";
					}
						
				?>
			</div>
			<!--EAD Information Ends Here-->
		</div>
	</div>
<?php include "footer.php"; ?>


<?php 
	function view_all_cces() {
		include "connection.php";
		try {
 			$con = new PDO("mysql:host=$db_host;dbname=$mysql_name", $db_user, $db_pass);
			$sql = "CALL get_all_cces()";
			$stmt = $con->prepare($sql);
			$stmt->execute();
 		}
 		catch (PDOException $e)
	  {
	    $error = 'Error searching by email.' ;
	    exit();
	  }
	  $arr_result = $stmt->fetchAll();
	  return $arr_result;
	}

	function view_cces($view_specific_cces) {
		include "connection.php";
		try {
 			$con = new PDO("mysql:host=$db_host;dbname=$mysql_name", $db_user, $db_pass);
			$sql = "CALL get_specific_cces(:image_no)";
			$stmt = $con->prepare($sql);
			$stmt->bindParam(':image_no', intval($view_specific_cces), PDO::PARAM_INT);
			$result = $stmt->execute();
			$arr_cces_result = $stmt->fetchAll();
			$arr_cces_result = $arr_cces_result[0];

			$con = new PDO("mysql:host=$db_host;dbname=$mysql_name", $db_user, $db_pass);
			$sql = "CALL get_cces_subjects(:id)";
			$stmt = $con->prepare($sql);
			$stmt->bindParam(':id', intval($view_specific_cces), PDO::PARAM_INT);
			$stmt->execute();
			$arr_cces_result_container = $stmt->fetchAll();
			$arr_cces_result['SUBJECTS'] = $arr_cces_result_container;
			$arr_cces_result['SUBJECTS_COUNT'] = count($arr_cces_result['SUBJECTS']);	
 		}
 		catch (PDOException $e)
	  {
	    $error = 'Error searching by email.' ;
	    exit();
	  }
		return $arr_cces_result;
	}
?>