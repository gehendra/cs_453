<?php

// "Create" the document.
	$imp = new DOMImplementation;

	$dtd = $imp->createDocumentType('ead', '', '');
	//$dtd = $imp->createDocumentType('ead', '', 'C:\Bitnami\wampstack-5.4.32-0\apache2\htdocs\archive\ead.dtd');
	// dtd hidden so will show on browser
	$xml = $imp->createDocument("", "", $dtd);
	$xml->encoding = "UTF-8";
	$xml->version = "1.0";

	$ead = $xml->createElement( "ead" );
	$ead->setAttribute("xsi:schemaLocation", "urn:isbn:1-931666-22-9 http://www.loc.gov/ead/ead.xsd");
	$ead->setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
	$ead->setAttribute("xmlns:xlink", "http://www.w3.org/1999/xlink");
	$ead->setAttribute("xmlns", "urn:isbn:1-931666-22-9");

	
// beginning of ead header

	
	$eadheader = $xml->createElement("eadheader");
	$eadheader->setAttribute("countryencoding", "iso3166-1");
	$eadheader->setAttribute("scriptencoding", "iso15924");
	$eadheader->setAttribute("langencoding", "iso639-2b");
	$eadheader->setAttribute("audience", "external");
	$eadheader->setAttribute("relatedencoding", "dublincore"); 
	
	
	$ead->appendChild($eadheader);

	
	
	$eadid = $xml->createElement("eadid");
	$eadid->setAttribute("identifier", $arr_specific_cces_result['IMAGE_NO']);
	$eadid->setAttribute("mainagencycode", "ICL-Ar");
	$eadid->setAttribute("countrycode", "US");
	
	
	$eadheader->appendChild($eadid);



// beginning of file description section

	$filedesc = $xml->createElement("filedesc");
	$eadheader->appendChild($filedesc);

	$ccesid = $xml->createElement("num", $arr_specific_cces_result['CCESID']);
	$ccesid->setAttribute("type", "CCES ID");
	$filedesc->appendChild($ccesid);

	$num = $xml->createElement("num", $arr_specific_cces_result['OTHERID']);
	$num->setAttribute("type", "other");
	$filedesc->appendChild($num);
	
	$church = $xml->createElement("name", $arr_specific_cces_result['CHURCH']);
	$filedesc->appendChild($church);

	$date = $xml->createElement("date", $arr_specific_cces_result['DATE']);
	$date->setAttribute("type", "depiction");
	$filedesc->appendChild($date);







// beginning of Descriptive Identification section



	$did = $xml->createElement("did");
	$eadheader->appendChild($did);

	$description = $xml->createElement("unittitle", $arr_specific_cces_result['DESCRIPTION']);
	$did->appendChild($description);

	$transscription = $xml->createElement("note", $arr_specific_cces_result['TRANSCRIPTION']);
	$did->appendChild($transscription);

	$ill = $xml->createElement("dimensions", $arr_specific_cces_result['IMAGE_LENGTH_L']);
	$ill->setAttribute("unit", "inches");
	$ill->setAttribute("type", "length");
	$did->appendChild($ill);

	$ilw = $xml->createElement("dimensions", $arr_specific_cces_result['IMAGE_LENGTH_W']);
	$ilw->setAttribute("unit", "inches");
	$ilw->setAttribute("type", "width");
	$did->appendChild($ilw);

	$type = $xml->createElement("genreform", $arr_specific_cces_result['TYPE']);
	$did->appendChild($type);




// beginning of Control Access section

	$archdesc = $xml->createElement("archdesc");
	$eadheader->appendChild($archdesc);
	
	$controlaccess = $xml->createElement("controlaccess");
	$archdesc->appendChild($controlaccess);

	$state = $xml->createElement("geoname", $arr_specific_cces_result['STATE']);
	$controlaccess->appendChild($state);


	$town_or_city = $xml->createElement("geoname", $arr_specific_cces_result['TOWN_OR_CITY']);
	$controlaccess->appendChild($town_or_city);

	$diocese = $xml->createElement("corpname",$arr_specific_cces_result['DIOCESE']);
	$diocese->setAttribute("source", "lcnaf");
	$controlaccess->appendChild($diocese);


	$arr_container_content = $arr_specific_cces_result['SUBJECTS'];
	if(is_array($arr_container_content)) {
		foreach ($arr_container_content as $subjects_row) {
			$name = $xml->createElement("subjects", $subjects_row['NAME']);
			$controlaccess->appendChild($name);
		}	
	}

	$xml->appendChild($ead);

	$xml->FormatOutput = true;
	$string_value = $xml->saveXML();
	$xml->save("isCCES.xml");



	/* XML format generator ends   */
?>

<script type="text/javascript">
	$(document).ready(function(){
		setTimeout(function(){
			window.location.href = "http://localhost/archive/isCCES.xml"}, 1000);
	})

</script>