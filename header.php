<html>
	<head>
		<title>Archive Project</title>
			<script src="/archive/js/jquery-2.0.3.min.js"></script>
    	<script src="/archive/bootstrap/dist/js/bootstrap.min.js"></script>
    	<script src="/archive/bootstrap/assets/js/holder.js"></script>
			<script type="text/javascript" src="/archive/js/jquery/jquery.js"></script>
			<script type="text/javascript" src="/archive/js/ead.js"></script>
			<script type="text/javascript" src="/archive/js/general.js"></script>
        
      <link rel="stylesheet" type="text/css" href="/archive/css/general.css">
      <link href="bootstrap/dist/css/carousel.css" rel="stylesheet">
			<link rel="stylesheet" type="text/css" href="/archive/css/ead.css">
			<link rel="stylesheet" type="text/css" href="/archive/css/style.css">

			<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
			<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap-theme.min.css">
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
			<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
			<script src="http://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
			<script src="/archive/js/jquery/jquery.complexify.php"></script>
			<!--<script src="http://jqueryvalidation.org/files/dist/additional-methods.min.js"></script>-->
	</head>
	<body>
		<?php 
			include "global_functions.php"; 
			$current_user = current_user_info();
			$is_user_logged_in = false;
			$role = "";
			$fname = "";
			if (isset($current_user)) {
				$is_user_logged_in = true;
				$fname = $current_user['FIRST_NAME'];
				$role = $current_user['ROLE'];
			}
		?>
		<div class="main-content">
			<div class="header">
				<div class="header_left">	
					<a href="/archive/index.php"><img src="/archive/images/logo.png" /></a>
				</div>
				<div class="header_right">
					<ul class="nav nav-pills">
		        <li><a href="/archive/index.php" onclick="alertify.error("Error notification");">Home</a></li>
		        <li class="dropdown">
		            <a href="#" data-toggle="dropdown" class="dropdown-toggle">EAD <b class="caret"></b></a>
		            <ul class="dropdown-menu">
		            	<?php if ($is_user_logged_in && ($role == "1" || $role == "2")) { ?> 	
		                <li><a href="/archive/add_ead.php">Add</a></li>
		              <?php } ?>
		                <li><a href="/archive/view.php">View</a></li>
		                <li><a href="/archive/search.php">Search</a></li>
		            </ul>
		        </li>
		        <li class="dropdown">
		            <a href="#" data-toggle="dropdown" class="dropdown-toggle">CCES <b class="caret"></b></a>
		            <ul class="dropdown-menu">
		                <?php if ($is_user_logged_in && ($role == "1" || $role == "2")) { ?> 
		                	<li><a href="/archive/add_cces.php">Add</a></li> 
		                <?php } ?>
		                <li><a href="/archive/view_cces.php">View</a></li>
		                <li><a href="/archive/cces_search.php">Search</a></li>
		            </ul>
		        </li>
<<<<<<< HEAD
		        <?php if($is_user_logged_in &&  $role == "1") { ?>
=======
		        <?php if($is_user_logged_in &&  $role == "1" ) { ?>
>>>>>>> e5092ea4e2e1bd2cc16a3d4f593362afe41eef45
			        <li class="dropdown">
			            <a href="#" data-toggle="dropdown" class="dropdown-toggle">Admin <b class="caret"></b></a>
			            <ul class="dropdown-menu">
			                <li><a href="/archive/admin/admin_users.php">User Management</a></li>
			                <li><a href="/archive/admin/logs.php">Logs</a></li>
			            </ul>
			        </li>
			      <?php } ?>
		          <li class="dropdown">
		            <a href="#" data-toggle="dropdown" class="dropdown-toggle">Welcome <?php 
				        		if ($fname) { 
				        			echo $fname;
				        		} else {
				        			echo "Guest";
				        		}
				        	?><b class="caret"></b></a>
		            <ul class="dropdown-menu">
		      			  <?php 
					        	if ($fname) { 
					        ?>
								<li><a href="/archive/profile.php">Profile</a></li>
								<li class="divider"></li>
					        	<li><a href="/archive/logout.php">Log out</a></li>
					        	<?php } else { ?>
					        		<li><a href="/archive/login.php">LogIn</a></li>
					        	<?php } ?>
					            </ul>
					        </li>

		    </ul>
				</div>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>