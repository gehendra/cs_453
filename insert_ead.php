<?php
session_start();
if(!($_SESSION['archive']['email']))
{
	header("Location: index.php");
}
?>
<?php 
include_once "mysql_info.php";
include_once "MySQL.php";
$table_name = "EAD";
$ead = new MySQL($mysql_name,$db_user,$db_pass,$db_host);
if ($_POST['txt_ead_title'] != "") {
// Insert to EAD Begins
	// Multiple Access Control Handler Begins
		$txt_ead_access_control_subject = "";
		$txt_ead_access_control_name = "";
		$txt_ead_access_control_corp_name = "";
		if($_POST['txt_ead_access_control_subject']) {
			$txt_ead_access_control_subject = serialize($_POST['txt_ead_access_control_subject']);
		}
		if($_POST['txt_ead_access_control_name']) {
			$txt_ead_access_control_name = serialize($_POST['txt_ead_access_control_name']);
		}
		if($_POST['txt_ead_access_control_corp_name']) {
			$txt_ead_access_control_corp_name = serialize($_POST['txt_ead_access_control_corp_name']);
		}
	// Multiple Access Control Handler Ends
	$chk_ead_collection_non_english = 0;
	if ($_POST['chk_ead_collection_non_english'])
	{
		$chk_ead_collection_non_english = 1;
	}
	$ead_array = array (
		'TITLE' => $_POST['txt_ead_title'],
		'SUBTITLE' => $_POST['txt_ead_subtitle'],
		'DATE' => $_POST['txt_ead_date'],
		'AUTHOR' => $_POST['txt_ead_author'],
		'REVISION_DESC' => $_POST['txt_ead_publisher_revision_desc'],
		'REVISION_DATE' => $_POST['txt_ead_publisher_revision_date'],
		'SUMMARY_INFO_DESC' => $_POST['txt_ead_summary_info_desc'],
		'SUMMARY_INFO_CORP_NAME' => $_POST['txt_ead_summary_info_corp_name'],
		'SUMMARY_INFO_ADDR' => $_POST['txt_ead_summary_info_addr'],
		'SEPERATED_MATERIAL_TITLE' => $_POST['txt_ead_seperated_head'],
		'SEPERATED_MATERIAL_BODY' => $_POST['txt_ead_seperated_body'],
		'RELATED_MATERIAL_TITLE' => $_POST['txt_ead_related_head'],	
		'RELATED_MATERIAL_BODY' => $_POST['txt_ead_related_body'],
		'SCOPE_CONTENT_NOTE' => $_POST['txt_ead_scope_content_note'],
		'SCOPE_CONTENT_ARRANGEMENT' => $_POST['txt_ead_scope_content_arrangement_note']
	);
	if ($_POST['txt_ead_title'] != ""  &&  $_POST['txt_ead_subtitle'] != "" && $_POST['txt_ead_date'] != "" && $_POST['txt_ead_author'] != "")  {
		$ead->insert($ead_array,$table_name);
	}
	$ead_id = $ead->LastInsertID(); //Get the EAD ID 
	$ead_id = 1;

	$table_name = "PUBLISHER"; 
	$arr_publisher = array (
		'PUBLISHER' => $_POST['txt_ead_publisher_description_publisher'],
		'PUBLISHER_DESC' => $_POST['txt_ead_publisher_description_creation'],
		'PUBLISHER_DATE' => $_POST['txt_ead_publisher_description_date'],
		'PUBLISHER_LANG' => $_POST['txt_ead_publisher_description_lang'],
		'EAD_ID' => intval($ead_id)
	);
	if ($_POST['txt_ead_publisher_description_publisher'] != "" || $_POST['txt_ead_publisher_description_creation'] != "" || $_POST['txt_ead_publisher_description_date'] != "" || $_POST['txt_ead_publisher_description_lang'] != "") {
		$ead->insert($arr_publisher,$table_name);
	}

	$table_name = "COLLECTION";
	$arr_collection = array (
		'COLLECTION_TITLE' => $_POST['txt_ead_collection_title'],
		'COLLECTION_INCL' => $_POST['txt_ead_collection_inclusive_date'],
		'COLLECTION_BULK' => $_POST['txt_ead_collection_bulk_date'],
		'COLLECTION_CREATOR' => $_POST['txt_ead_collection_creator'],
		'COLLECTION_LINEAR_FEET' => $_POST['txt_ead_collection_linear_feet_1'],
		'COLLECTION_ITEM_NUM' => $_POST['txt_ead_collection_linear_number'],
		'COLLECTION_LANG' => $POST['txt_ead_collection_material_in'],
		'COLLECTION_NON_ENG' => $chk_ead_collection_non_english,
		'EAD_ID' => intval($ead_id)
	);
	if ($_POST['txt_ead_collection_title'] != "" || $_POST['txt_ead_collection_inclusive_date'] != "" || $_POST['txt_ead_collection_bulk_date'] != "" || $_POST['txt_ead_collection_creator'] != "" || $_POST['txt_ead_collection_linear_feet_1'] != "" || $_POST['txt_ead_collection_linear_number'] != "" || $POST['txt_ead_collection_material_in'] || $chk_ead_collection_non_english) {
		$ead->insert($arr_collection,$table_name);
	}

	$table_name = "EAD_ADMIN";
	$arr_ead_admin = array (
		'ADMIN_INFO_ACCURAL' => $_POST['txt_ead_admin_info_acc_info'],
		'ADMIN_INFO_PROVENANCE' => $_POST['txt_ead_admin_info_prov_info'],
		'ADMIN_INFO_ACQUISITION' => $_POST['txt_ead_admin_info_acq_info'],
		'ADMIN_INFO_USAGE' => $_POST['txt_ead_admin_info_usage_restriction'],
		'ADMIN_INFO_PREFERRED_CITATION' => $_POST['txt_ead_admin_info_pref_citation'],
		'ADMIN_INFO_ACCESS' => $_POST['txt_ead_admin_info_access_restrictions'],
		'ADMIN_INFO_PROCESSING' => $_POST['txt_ead_admin_info_processing_info'],
		'ADMIN_INFO_BIO' => $_POST['txt_ead_admin_info_bio_info'],
		'EAD_ID' => intval($ead_id)
	);
	if ($_POST['txt_ead_admin_info_acc_info'] != "" || $_POST['txt_ead_admin_info_prov_info'] != "" || $_POST['txt_ead_admin_info_acq_info'] != "" || $_POST['txt_ead_admin_info_usage_restriction'] != "" || $_POST['txt_ead_admin_info_pref_citation'] != "" || $_POST['txt_ead_admin_info_access_restrictions'] != "" || $_POST['txt_ead_admin_info_processing_info'] != "" || $_POST['txt_ead_admin_info_bio_info']) {
		$ead->insert($arr_ead_admin,$table_name);
	}	

	$table_name = "ACCESS_CONTROL";
	$arr_access_control = array (
		'ACCESS_CONTROL_TITLE' => $txt_ead_access_control_subject,
		'ACCESS_CONTROL_NAME' => $txt_ead_access_control_name,
		'ACCESS_CONTROL_CORP_NAME' => $txt_ead_access_control_corp_name,
		'EAD_ID' => intval($ead_id)
	);
	if ($_POST['txt_ead_access_control_subject'] != "" || $_POST['txt_ead_access_control_name'] != "" || $_POST['txt_ead_access_control_corp_name'] != "") {
		$ead->insert($arr_access_control,$table_name);
	}

// Insert to EAD Ends 
// Insert to Container Begins
	$count_container = 0;
	if($_POST['collection_type'] && $_POST['txt_ead_container_content_title']) {
	  $count_container = count($_POST['txt_ead_container_content_title']);
	}

	for ($i=0;$i<$count_container;$i++) {
		$container_array = array (
			'TYPE' => $_POST['collection_type'][$i],
			'TITLE' => $_POST['txt_ead_container_content_title'][$i],
			'DATE_TYPE' => $_POST['txt_ead_container_content_date'][$i],
			'LANGUAGE' => $_POST['txt_ead_container_content_lang'][$i],
			'EAD_ID' => intval($ead_id)
		);
		$table_name = "CONTAINER";
		$ead->insert($container_array,$table_name);	
	}
	header("Location: /archive/view.php");
}
?>