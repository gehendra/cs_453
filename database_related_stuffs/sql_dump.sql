SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

CREATE DATABASE IF NOT EXISTS `archive` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `archive`;

DELIMITER $$
DROP PROCEDURE IF EXISTS `add_user`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `add_user`(IN `fname` VARCHAR(100), IN `lname` VARCHAR(100), IN `email` VARCHAR(100), IN `pass` VARCHAR(100))
    NO SQL
INSERT INTO users(`FIRST_NAME`,`LAST_NAME`,`EMAIL`,`PASSWORD`) values (fname, lname,email, pass)$$

DROP PROCEDURE IF EXISTS `delete_access_control_by_ead_id`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_access_control_by_ead_id`(IN `ead_id` INT)
    NO SQL
DELETE FROM ACCESS_CONTROL where ACCESS_CONTROL.EAD_ID=ead_id$$

DROP PROCEDURE IF EXISTS `delete_cces_by_image_no`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_cces_by_image_no`(IN `image_no` INT(11))
    NO SQL
DELETE FROM cces_photos where cces_photos.IMAGE_NO=image_no$$

DROP PROCEDURE IF EXISTS `delete_collection_by_ead_id`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_collection_by_ead_id`(IN `ead_id` INT(11))
    NO SQL
DELETE FROM COLLECTION where COLLECTION.EAD_ID=ead_id$$

DROP PROCEDURE IF EXISTS `delete_container_by_ead_id`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_container_by_ead_id`(IN `ead_id` INT(11))
    NO SQL
DELETE FROM CONTAINER where CONTAINER.EAD_ID=ead_id$$

DROP PROCEDURE IF EXISTS `delete_ead_admin_by_ead_id`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_ead_admin_by_ead_id`(IN `ead_id` INT(11))
    NO SQL
DELETE FROM EAD_ADMIN where EAD_ADMIN.EAD_ID=ead_id$$

DROP PROCEDURE IF EXISTS `delete_ead_by_id`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_ead_by_id`(IN `ead_id` INT(11))
    NO SQL
DELETE FROM EAD where EAD.ID=ead_id$$

DROP PROCEDURE IF EXISTS `delete_publisher_by_ead_id`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_publisher_by_ead_id`(IN `ead_id` INT(11))
    NO SQL
DELETE FROM PUBLISHER where PUBLISHER.EAD_ID=ead_id$$

DROP PROCEDURE IF EXISTS `delete_subject_by_image_no`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_subject_by_image_no`(IN `image_no` INT(11))
    NO SQL
DELETE FROM subjects where subjects.IMAGE_NO=image_no$$

DROP PROCEDURE IF EXISTS `delete_user_info_by_id`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_user_info_by_id`(IN `id` INT(11))
DELETE FROM users where users.ID=id$$

DROP PROCEDURE IF EXISTS `get_access_control_by_ead_id`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_access_control_by_ead_id`(IN `ead_id` INT(11))
    NO SQL
SELECT * FROM ACCESS_CONTROL where EAD_ID=ead_id$$

DROP PROCEDURE IF EXISTS `get_all_admin_users`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_all_admin_users`()
    NO SQL
SELECT count(*) FROM USERS WHERE ROLE in (SELECT ID from roles where NAME="Admin")$$

DROP PROCEDURE IF EXISTS `get_all_cces`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_all_cces`()
SELECT * FROM CCES_PHOTOS$$

DROP PROCEDURE IF EXISTS `get_all_ead`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_all_ead`()
    NO SQL
SELECT * FROM EAD$$

DROP PROCEDURE IF EXISTS `get_all_log`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_all_log`()
    NO SQL
SELECT * FROM archive_log$$

DROP PROCEDURE IF EXISTS `get_all_member_users`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_all_member_users`()
    NO SQL
SELECT * FROM USERS WHERE ROLE in (SELECT ID from roles where NAME="Member")$$

DROP PROCEDURE IF EXISTS `get_all_roles`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_all_roles`()
    NO SQL
SELECT * FROM roles$$

DROP PROCEDURE IF EXISTS `get_all_users`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_all_users`()
    NO SQL
SELECT * FROM users$$

DROP PROCEDURE IF EXISTS `get_cces_subjects`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_cces_subjects`(IN `id` INT)
SELECT * FROM SUBJECTS WHERE IMAGE_NO=id$$

DROP PROCEDURE IF EXISTS `get_collection_by_ead_id`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_collection_by_ead_id`(IN `ead_id` INT(11))
    NO SQL
SELECT * FROM COLLECTION where COLLECTION.EAD_ID=ead_id$$

DROP PROCEDURE IF EXISTS `get_ead_admin_by_ead_id`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_ead_admin_by_ead_id`(IN `ead_id` INT(11))
    NO SQL
SELECT * FROM EAD_ADMIN WHERE EAD_ADMIN.EAD_ID=ead_id$$

DROP PROCEDURE IF EXISTS `get_ead_container`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_ead_container`(IN `id` INT)
    NO SQL
SELECT * FROM CONTAINER WHERE EAD_ID=id$$

DROP PROCEDURE IF EXISTS `get_publisherd_by_eid`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_publisherd_by_eid`(IN `ead_id` INT(11))
    NO SQL
SELECT * FROM PUBLISHER where PUBLISHER.EAD_ID=ead_id$$

DROP PROCEDURE IF EXISTS `get_specific_cces`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_specific_cces`(IN `image_no` INT)
SELECT * FROM CCES_PHOTOS where CCES_PHOTOS.IMAGE_NO=image_no$$

DROP PROCEDURE IF EXISTS `get_specific_ead`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_specific_ead`(IN `id` INT)
    NO SQL
SELECT * FROM EAD where EAD.ID=id$$

DROP PROCEDURE IF EXISTS `get_user_info`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_user_info`(IN `email` VARCHAR(100))
    NO SQL
SELECT * FROM users where users.email = email limit 1$$

DROP PROCEDURE IF EXISTS `get_user_info_by_id`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_user_info_by_id`(IN `id` INT(11))
    NO SQL
SELECT * FROM users where users.ID=id$$

DROP PROCEDURE IF EXISTS `update_cces_by_id`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_cces_by_id`(IN `image_no` INT(11), IN `ccesid` INT(11), IN `otherid` VARCHAR(1000), IN `state` VARCHAR(200), IN `town_or_city` VARCHAR(200), IN `diocese` VARCHAR(1000), IN `church` VARCHAR(100), IN `cces_date` VARCHAR(100), IN `description` VARCHAR(1000), IN `transcription` VARCHAR(1000), IN `image_size_l` VARCHAR(11), IN `image_size_w` VARCHAR(11), IN `type` VARCHAR(200))
UPDATE CCES_PHOTOS SET 
CCES_PHOTOS.CCESID=ccesid,
CCES_PHOTOS.OTHERID=otherid,
CCES_PHOTOS.STATE=state,
CCES_PHOTOS.TOWN_OR_CITY=town_or_city,
CCES_PHOTOS.DIOCESE=diocese,
CCES_PHOTOS.CHURCH=church,
CCES_PHOTOS.DATE=date,
CCES_PHOTOS.DESCRIPTION=description,
CCES_PHOTOS.TRANSCRIPTION=transcription,
CCES_PHOTOS.IMAGE_SIZE_L=image_size_l,
CCES_PHOTOS.IMAGE_SIZE_W=image_size_w,
CCES_PHOTOS.TYPE=type
where CCES_PHOTOS.IMAGE_NO=image_no$$

DROP PROCEDURE IF EXISTS `update_cces_filename`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_cces_filename`(IN `image_no` INT(11), IN `filename` VARCHAR(200))
    NO SQL
UPDATE cces_photos as c set c.FILENAME = filename where c.IMAGE_NO=image_no$$

DROP PROCEDURE IF EXISTS `update_cces_subject`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_cces_subject`(IN `id` INT(11), IN `name` TEXT)
    NO SQL
UPDATE subjects as s set s.NAME=name where s.ID=id$$

DROP PROCEDURE IF EXISTS `update_ead_by_id`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_ead_by_id`(IN `id` INT(11), 
IN `title` VARCHAR(200), 
IN `subtitle` VARCHAR(200), 
IN `ead_date` VARCHAR(200), 
IN `author` VARCHAR(200), 
IN `publisher` VARCHAR(200), 
IN `publisher_desc` VARCHAR(1000), 
IN `publisher_date` VARCHAR(100), 
IN `publisher_lang` VARCHAR(100), 
IN `revision_desc` VARCHAR(100), 
IN `revision_date` VARCHAR(200), 
IN `summary_info_desc` VARCHAR(1000), 
IN `summary_info_corp_name` VARCHAR(200), 
IN `summary_info_addr` VARCHAR(200),
IN `collection_title` VARCHAR(200),
IN `collection_incl` VARCHAR(100),
IN `collection_bulk` VARCHAR(200), 
IN `collection_creator` VARCHAR(100), 
IN `collection_linear_feet` VARCHAR(100), 
IN `collection_item_num` VARCHAR(200), 
IN `collection_lang` VARCHAR(200), 
IN `collection_non_eng` TINYINT(1), 
IN `seperated_material_title` VARCHAR(200), 
IN `seperated_material_body` VARCHAR(1000), 
IN `related_material_title` VARCHAR(200), 
IN `related_material_body` VARCHAR(1000), 
IN `admin_info_accural` VARCHAR(1000), 
IN `admin_info_provenance` VARCHAR(1000), 
IN `admin_info_acquisition` VARCHAR(1000), 
IN `admin_info_usage` VARCHAR(1000), 
IN `admin_info_preferred_citation` VARCHAR(1000),
IN `admin_info_access` VARCHAR(1000),
IN `admin_info_processing` VARCHAR(1000),
IN `admin_info_bio` VARCHAR(1000),
IN `scope_content_note` VARCHAR(1000), 
IN `scope_content_arrangement` VARCHAR(1000), 
IN `access_control_title` VARCHAR(200), 
IN `access_control_name` VARCHAR(200), 
IN `access_control_corp_name` VARCHAR(200))
UPDATE EAD SET 
EAD.TITLE=title,
EAD.SUBTITLE=subtitle,
EAD.DATE=ead_date,
EAD.AUTHOR=author,
EAD.PUBLISHER=publisher,
EAD.PUBLISHER_DESC=publisher_desc,
EAD.PUBLISHER_DATE=publisher_date,
EAD.PUBLISHER_LANG=publisher_lang,
EAD.REVISION_DESC=revision_desc,
EAD.REVISION_DATE=revision_date,
EAD.SUMMARY_INFO_DESC=summary_info_desc,
EAD.SUMMARY_INFO_CORP_NAME=summary_info_corp_name,
EAD.SUMMARY_INFO_ADDR=summary_info_addr,
EAD.COLLECTION_TITLE=collection_title,
EAD.COLLECTION_INCL=collection_incl,
EAD.COLLECTION_BULK=collection_bulk,
EAD.COLLECTION_CREATOR=collection_creator,
EAD.COLLECTION_LINEAR_FEET=collection_linear_feet,
EAD.COLLECTION_ITEM_NUM=collection_item_num,
EAD.COLLECTION_LANG=collection_lang,
EAD.COLLECTION_NON_ENG=collection_non_eng,
EAD.SEPERATED_MATERIAL_TITLE=seperated_material_title,
EAD.SEPERATED_MATERIAL_BODY=seperated_material_body,
EAD.RELATED_MATERIAL_TITLE=related_material_title,
EAD.RELATED_MATERIAL_BODY=related_material_body,
EAD.ADMIN_INFO_ACCURAL=admin_info_accural,
EAD.ADMIN_INFO_PROVENANCE=admin_info_provenance,
EAD.ADMIN_INFO_ACQUISITION=admin_info_acquisition,
EAD.ADMIN_INFO_USAGE=admin_info_usage,
EAD.ADMIN_INFO_PREFERRED_CITATION=admin_info_preferred_citation,
EAD.ADMIN_INFO_ACCESS=admin_info_access,
EAD.ADMIN_INFO_PROCESSING=admin_info_processing,
EAD.ADMIN_INFO_BIO=admin_info_bio,
EAD.SCOPE_CONTENT_NOTE=scope_content_note,
EAD.SCOPE_CONTENT_ARRANGEMENT=scope_content_arrangement,
EAD.ACCESS_CONTROL_TITLE=access_control_title,
EAD.ACCESS_CONTROL_NAME=access_control_name,
EAD.ACCESS_CONTROL_CORP_NAME=access_control_corp_name
where EAD.ID=id$$

DROP PROCEDURE IF EXISTS `user_login`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `user_login`(IN `email` VARCHAR(100))
    NO SQL
SELECT * FROM users where users.EMAIL=email$$

DELIMITER ;

DROP TABLE IF EXISTS `access_control`;
CREATE TABLE IF NOT EXISTS `access_control` (
`ID` int(11) NOT NULL,
  `ACCESS_CONTROL_TITLE` varchar(200) DEFAULT NULL,
  `ACCESS_CONTROL_NAME` varchar(200) DEFAULT NULL,
  `ACCESS_CONTROL_CORP_NAME` varchar(200) DEFAULT NULL,
  `EAD_ID` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO `access_control` (`ID`, `ACCESS_CONTROL_TITLE`, `ACCESS_CONTROL_NAME`, `ACCESS_CONTROL_CORP_NAME`, `EAD_ID`) VALUES
(2, 'a:1:{i:0;s:27:"Controlled Access Subject 1";}', 'a:1:{i:0;s:33:"Controlled Access Personal Name 1";}', 'a:1:{i:0;s:34:"Controlled Access Corporate Name 1";}', 1);

DROP TABLE IF EXISTS `archive_log`;
CREATE TABLE IF NOT EXISTS `archive_log` (
`ID` int(11) NOT NULL,
  `DESCRIPTION` varchar(500) NOT NULL,
  `LOGGED_TIME` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

INSERT INTO `archive_log` (`ID`, `DESCRIPTION`, `LOGGED_TIME`) VALUES
(1, 'Modification made to user table', '2014-11-29 12:31:47'),
(3, 'Modification made to user table', '2014-11-29 16:53:55'),
(4, 'Modification made to user table', '2014-11-30 17:42:15'),
(5, 'Modification made to user table', '2014-11-30 20:37:54'),
(6, 'Modification made to user table', '2014-11-30 22:01:55'),
(7, 'Modification made to user table', '2014-12-01 18:13:08'),
(8, 'Modification made to user table', '2014-12-01 19:44:21'),
(9, 'Modification made to user table', '2014-12-01 20:36:32'),
(10, 'User added to the user table.', '2014-12-01 20:42:47'),
(12, 'User added to the user table.', '2014-12-02 08:04:25'),
(13, 'Modification made to user table', '2014-12-03 22:34:37'),
(14, 'Modification made to user table', '2014-12-04 19:49:42'),
(15, 'Modification made to user table', '2014-12-06 13:20:03'),
(16, 'CCES deleted', '2014-12-10 00:21:31'),
(17, 'CCES deleted', '2014-12-10 00:21:31'),
(18, 'CCES deleted', '2014-12-10 00:21:31'),
(19, 'CCES deleted', '2014-12-10 00:21:31'),
(20, 'CCES deleted', '2014-12-10 00:21:31'),
(21, 'CCES deleted', '2014-12-10 00:21:31'),
(22, 'CCES deleted', '2014-12-10 00:21:32'),
(23, 'CCES deleted', '2014-12-10 00:21:32'),
(24, 'CCES deleted', '2014-12-10 00:21:32'),
(25, 'CCES deleted', '2014-12-10 00:21:32');

DROP TABLE IF EXISTS `cces_photos`;
CREATE TABLE IF NOT EXISTS `cces_photos` (
`IMAGE_NO` int(11) NOT NULL,
  `CCESID` int(11) NOT NULL,
  `OTHERID` varchar(1000) DEFAULT NULL,
  `STATE` varchar(200) NOT NULL,
  `TOWN_OR_CITY` varchar(200) DEFAULT NULL,
  `DIOCESE` varchar(1000) DEFAULT NULL,
  `CHURCH` varchar(100) DEFAULT NULL,
  `DATE` varchar(100) DEFAULT NULL,
  `DESCRIPTION` varchar(1000) NOT NULL,
  `TRANSCRIPTION` varchar(1000) DEFAULT NULL,
  `IMAGE_SIZE_L` varchar(11) NOT NULL,
  `IMAGE_SIZE_W` varchar(11) NOT NULL,
  `TYPE` varchar(200) DEFAULT NULL,
  `FILENAME` varchar(200) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

INSERT INTO `cces_photos` (`IMAGE_NO`, `CCESID`, `OTHERID`, `STATE`, `TOWN_OR_CITY`, `DIOCESE`, `CHURCH`, `DATE`, `DESCRIPTION`, `TRANSCRIPTION`, `IMAGE_SIZE_L`, `IMAGE_SIZE_W`, `TYPE`, `FILENAME`) VALUES
(11, 7545, '39-P-9', 'CA', 'San Francisco', 'Roman Catholic Archdiocese of San Francisco', 'St. Francis Xavier', '5/12/1936', 'Japanese Catholic Mission Confirmation', 'Nov. 19/36 Fix All Pencil Lines, Reduce to Width 5.5', '6', '6', 'Photograph (B&W)', '11.jpg'),
(12, 5110, '', 'IL', 'Chicago', 'Chicago', '', '', 'Y.W.C.A. Building Chicago, Ill.', '', '6', '3.75', 'Photograph (B&W)', '12.jpg'),
(13, 6561, '32366', 'MI', '', '', '', '', 'Portrait of Rev. Thomas F. X. Hally', 'Used in Crt 1930, pg. 55', '6', '3', 'Portrait - Photograph (B&W)', '13.jpg'),
(14, 1405, '8-G-3', 'WA', 'Spokane', 'Roman Catholic Diocese of Spokane', 'Ft. Spokane', '', 'Place near Ft. Spokane where Father Caldi said Mass occasionally. Washington ', 'Reduce to width 4" x at least 2.25', '2.25', '4', 'Photograph (B&W)', '14.jpg'),
(15, 3286, '5-H-1', 'WA', 'Tacoma', 'Roman Catholic Archdiocese of Seattle', '', '', 'Graduates in Tacoma, Wa.', 'Sr. M. Bernard Tacoma, Wn. (Graduates)', '6', '3', 'Photograph (B&W)', '15.jpg'),
(16, 7545, '39-P-11', 'CA', 'San Francisco', 'Roman Catholic Archdiocese of San Francisco', 'St. Francis Xavier', '', 'First Communion - Boys', 'Reduce to width 4.25" x height 3"', '3', '4.25', 'Photograph (B&W)', '16.jpg');
DROP TRIGGER IF EXISTS `add_cces_photo_trigger`;
DELIMITER //
CREATE TRIGGER `add_cces_photo_trigger` BEFORE INSERT ON `cces_photos`
 FOR EACH ROW INSERT INTO archive_log (`DESCRIPTION`,`LOGGED_TIME`) values ("Added  to cces table",now())
//
DELIMITER ;
DROP TRIGGER IF EXISTS `delete_ccess`;
DELIMITER //
CREATE TRIGGER `delete_ccess` BEFORE DELETE ON `cces_photos`
 FOR EACH ROW INSERT INTO archive_log (DESCRIPTION, LOGGED_TIME) values ("CCES deleted", now())
//
DELIMITER ;
DROP TRIGGER IF EXISTS `update_cces_photo_trigger`;
DELIMITER //
CREATE TRIGGER `update_cces_photo_trigger` BEFORE UPDATE ON `cces_photos`
 FOR EACH ROW INSERT INTO archive_log (`DESCRIPTION`,`LOGGED_TIME`) values ("Updated  to cces table",now())
//
DELIMITER ;

DROP TABLE IF EXISTS `collection`;
CREATE TABLE IF NOT EXISTS `collection` (
`ID` int(11) NOT NULL,
  `COLLECTION_TITLE` varchar(200) DEFAULT NULL,
  `COLLECTION_INCL` varchar(100) NOT NULL,
  `COLLECTION_BULK` varchar(100) NOT NULL,
  `COLLECTION_CREATOR` varchar(200) DEFAULT NULL,
  `COLLECTION_LINEAR_FEET` varchar(200) DEFAULT NULL,
  `COLLECTION_ITEM_NUM` varchar(200) DEFAULT NULL,
  `COLLECTION_LANG` varchar(200) DEFAULT NULL,
  `COLLECTION_NON_ENG` tinyint(1) DEFAULT NULL,
  `EAD_ID` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO `collection` (`ID`, `COLLECTION_TITLE`, `COLLECTION_INCL`, `COLLECTION_BULK`, `COLLECTION_CREATOR`, `COLLECTION_LINEAR_FEET`, `COLLECTION_ITEM_NUM`, `COLLECTION_LANG`, `COLLECTION_NON_ENG`, `EAD_ID`) VALUES
(2, 'Special Collections and Archives', '12/12/2002', '12/12/2002', 'COLLECTION_CREATOR', 'Linear Feet', 'Number of Items', 'english', 0, 1);

DROP TABLE IF EXISTS `container`;
CREATE TABLE IF NOT EXISTS `container` (
`ID` int(11) NOT NULL,
  `TYPE` tinyint(4) NOT NULL COMMENT '1->Class 2->Collection 3->Files 4->Items 5->Series',
  `TITLE` varchar(200) NOT NULL,
  `DATE_TYPE` int(11) NOT NULL COMMENT '1->N/A 2->Bulk 3->Inclusive',
  `LANGUAGE` varchar(200) DEFAULT NULL,
  `EAD_ID` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

INSERT INTO `container` (`ID`, `TYPE`, `TITLE`, `DATE_TYPE`, `LANGUAGE`, `EAD_ID`) VALUES
(8, 1, 'Boston Girls'' High School, commencement program', 1, 'english', 1);

DROP TABLE IF EXISTS `ead`;
CREATE TABLE IF NOT EXISTS `ead` (
`ID` int(11) NOT NULL,
  `TITLE` varchar(200) NOT NULL,
  `SUBTITLE` varchar(1000) NOT NULL,
  `DATE` varchar(200) NOT NULL,
  `AUTHOR` varchar(200) DEFAULT NULL,
  `REVISION_DESC` varchar(1000) DEFAULT NULL,
  `REVISION_DATE` varchar(200) DEFAULT NULL,
  `SUMMARY_INFO_DESC` varchar(1000) DEFAULT NULL,
  `SUMMARY_INFO_CORP_NAME` varchar(200) DEFAULT NULL,
  `SUMMARY_INFO_ADDR` varchar(200) DEFAULT NULL,
  `SEPERATED_MATERIAL_TITLE` varchar(200) DEFAULT NULL,
  `SEPERATED_MATERIAL_BODY` varchar(1000) DEFAULT NULL,
  `RELATED_MATERIAL_TITLE` varchar(200) DEFAULT NULL,
  `RELATED_MATERIAL_BODY` varchar(1000) DEFAULT NULL,
  `SCOPE_CONTENT_NOTE` varchar(1000) DEFAULT NULL,
  `SCOPE_CONTENT_ARRANGEMENT` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO `ead` (`ID`, `TITLE`, `SUBTITLE`, `DATE`, `AUTHOR`, `REVISION_DESC`, `REVISION_DATE`, `SUMMARY_INFO_DESC`, `SUMMARY_INFO_CORP_NAME`, `SUMMARY_INFO_ADDR`, `SEPERATED_MATERIAL_TITLE`, `SEPERATED_MATERIAL_BODY`, `RELATED_MATERIAL_TITLE`, `RELATED_MATERIAL_BODY`, `SCOPE_CONTENT_NOTE`, `SCOPE_CONTENT_ARRANGEMENT`) VALUES
(1, 'Guide to the Mildred Davenport Dance Programs and Dance School Material', 'Guide to the Edna Phelps', 'ca. 1810-1', 'Processed by Adrian Turner; machine-readable finding aid created by Adrian Turner', 'REVISION DESC1', 'REVISION DATE', 'Mildred Davenport dance programs and dance school materials', 'University of California, Irvine. Library. Special Collections and Archives.', 'SUMMARY_INFO_ADDR', 'Sample Separated Materials', 'Sample Separated Materials1Sample Separated Materials2Sample Separated Materials13', 'University of California, Irvine. Library. Special Collections and Archives.', 'University of California, Irvine. Library. Special Collections and Archives.          ', 'This collection comprises dance programs, dance school materials, photographs,and ephemera documenting the early career of the Boston-based African-American dancer, dance instructor, and civic official Mildred Davenport. The bulk of this collection consists of dance programs and dance school materials. The collection also contains 29 photographs of Davenport, her students in various performances, and friends or individual students. Dance programs from 1925 to 1942 feature her solo performances and group performances with her students. The collection includes a complete run of programs for Bronze Rhapsody, an annual performance series choreographed, staged, and directed by Davenport. Her personal copy of a typescript of stage directions for a 1934 performance is included with these programs. Her dance schools, Davenport School of the Dance and Silver Box Studio, are documented in course brochures and applications. Biographical and academic materials include a 1939 newspaper article', 'arranged');
DROP TRIGGER IF EXISTS `ead_delete`;
DELIMITER //
CREATE TRIGGER `ead_delete` BEFORE DELETE ON `ead`
 FOR EACH ROW INSERT INTO archive_log (DESCRIPTION, LOGGED_TIME) values ("EAD deleted.", now())
//
DELIMITER ;
DROP TRIGGER IF EXISTS `ead_insert`;
DELIMITER //
CREATE TRIGGER `ead_insert` BEFORE INSERT ON `ead`
 FOR EACH ROW INSERT INTO archive_log (DESCRIPTION, LOGGED_TIME) values ("EAD added.", now())
//
DELIMITER ;
DROP TRIGGER IF EXISTS `ead_update`;
DELIMITER //
CREATE TRIGGER `ead_update` BEFORE UPDATE ON `ead`
 FOR EACH ROW INSERT INTO archive_log (DESCRIPTION, LOGGED_TIME) values ("EAD updates", now())
//
DELIMITER ;

DROP TABLE IF EXISTS `ead_admin`;
CREATE TABLE IF NOT EXISTS `ead_admin` (
`ID` int(11) NOT NULL,
  `ADMIN_INFO_ACCURAL` varchar(1000) DEFAULT NULL,
  `ADMIN_INFO_PROVENANCE` varchar(1000) DEFAULT NULL,
  `ADMIN_INFO_ACQUISITION` varchar(1000) DEFAULT NULL,
  `ADMIN_INFO_USAGE` varchar(1000) DEFAULT NULL,
  `ADMIN_INFO_PREFERRED_CITATION` varchar(1000) DEFAULT NULL,
  `ADMIN_INFO_ACCESS` varchar(1000) DEFAULT NULL,
  `ADMIN_INFO_PROCESSING` varchar(1000) DEFAULT NULL,
  `ADMIN_INFO_BIO` varchar(1000) DEFAULT NULL,
  `EAD_ID` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO `ead_admin` (`ID`, `ADMIN_INFO_ACCURAL`, `ADMIN_INFO_PROVENANCE`, `ADMIN_INFO_ACQUISITION`, `ADMIN_INFO_USAGE`, `ADMIN_INFO_PREFERRED_CITATION`, `ADMIN_INFO_ACCESS`, `ADMIN_INFO_PROCESSING`, `ADMIN_INFO_BIO`, `EAD_ID`) VALUES
(2, 'ADMIN_INFO_ACCURAL', 'ADMIN_INFO_PROVENANCE', 'ADMIN_INFO_ACQUISITION', 'ADMIN_INFO_USAGE', 'ADMIN_INFO_PREFERRED_CITATION', 'List any Restrictions:\r\nList any Restrictions:\r\nList any Restrictions:\r\nList any Restrictions:', 'Processing Information:', 'Biographical Note/Administrative History:', 1);

DROP TABLE IF EXISTS `publisher`;
CREATE TABLE IF NOT EXISTS `publisher` (
`ID` int(11) NOT NULL,
  `PUBLISHER` varchar(200) DEFAULT NULL,
  `PUBLISHER_DESC` varchar(1000) DEFAULT NULL,
  `PUBLISHER_DATE` varchar(100) DEFAULT NULL,
  `PUBLISHER_LANG` varchar(100) DEFAULT NULL,
  `EAD_ID` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT INTO `publisher` (`ID`, `PUBLISHER`, `PUBLISHER_DESC`, `PUBLISHER_DATE`, `PUBLISHER_LANG`, `EAD_ID`) VALUES
(2, 'spcoll', 'Publication Description  Field', '2001', 'English', 1);

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `roles` (`ID`, `NAME`) VALUES
(1, 'Administrator'),
(2, 'Editor'),
(3, 'Guest');

DROP TABLE IF EXISTS `subjects`;
CREATE TABLE IF NOT EXISTS `subjects` (
`ID` int(11) NOT NULL,
  `IMAGE_NO` int(20) NOT NULL,
  `NAME` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

INSERT INTO `subjects` (`ID`, `IMAGE_NO`, `NAME`) VALUES
(1, 11, 'Japanese Catholic Mission Confirmation'),
(2, 12, 'Y.W.C.A. Building '),
(3, 13, 'Rev. Thomas F. X. Hally'),
(4, 14, 'Building'),
(5, 15, 'Graduates in Tacoma'),
(6, 16, 'First Communion - Boys');

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
`ID` int(11) NOT NULL,
  `FIRST_NAME` varchar(100) NOT NULL,
  `LAST_NAME` varchar(100) NOT NULL,
  `EMAIL` varchar(100) NOT NULL,
  `PASSWORD` varchar(200) NOT NULL,
  `ROLE` int(11) NOT NULL DEFAULT '3'
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

INSERT INTO `users` (`ID`, `FIRST_NAME`, `LAST_NAME`, `EMAIL`, `PASSWORD`, `ROLE`) VALUES
(1, 'Gehendra', 'Karmacharya', 'gehendra.karmacharya@gmail.com', '$2y$11$GX3JPMNkHGGHocn/UhhmaeRXxuHY2bMwoz9rrBb0cT/vrmjewUu.K', 1),
(2, 'Bijay', 'Karmacharya', 'bijay.karmacharya@gmail.com', '$2y$11$LdR8uAdg/wlQ03nCe.DZD.IomNI6A7lfB2CJ/XDofVFOxH.MoNol2', 2),
(3, 'Loyola', 'Archive', 'loyola.archive@gmail.com', '$2y$11$LdR8uAdg/wlQ03nCe.DZD.IomNI6A7lfB2CJ/XDofVFOxH.MoNol2', 2),
(4, 'Loyola', 'Chicago', 'loyola.chicago@luc.edu', '$2y$11$gZN844IML7xIzuEyK2Bikeph6LjmWLfIYybPnZ7kgz23.ZyzGhlPy', 2),
(5, 'Marco', 'Reynoso', 'mreynoso@luc.edu', '$2y$11$HiCtEkGBL/z3QAMPyjw2OOEB9puYPJf/IQu85i4fyahDeQB8Jcy3K', 2),
(7, 'Adams', 'Smith', 'adams.smith@gmail.com', '$2y$11$56ntqepzhSwnMsQxlRtZtOASHHzti4OXYsfiVZ2F1xrcBIz44WJpq', 2);
DROP TRIGGER IF EXISTS `user_delete`;
DELIMITER //
CREATE TRIGGER `user_delete` BEFORE DELETE ON `users`
 FOR EACH ROW INSERT INTO archive_log (`DESCRIPTION`,`LOGGED_TIME`) values ("User was deleted",now())
//
DELIMITER ;
DROP TRIGGER IF EXISTS `user_insert`;
DELIMITER //
CREATE TRIGGER `user_insert` BEFORE INSERT ON `users`
 FOR EACH ROW INSERT INTO archive_log (`DESCRIPTION`,`LOGGED_TIME`) values ("User added to the user table.", now())
//
DELIMITER ;
DROP TRIGGER IF EXISTS `user_update`;
DELIMITER //
CREATE TRIGGER `user_update` BEFORE UPDATE ON `users`
 FOR EACH ROW INSERT INTO archive_log (`DESCRIPTION`,`LOGGED_TIME`) values ("Modification made to user table",now())
//
DELIMITER ;


ALTER TABLE `access_control`
 ADD PRIMARY KEY (`ID`), ADD KEY `EAD_ID` (`EAD_ID`), ADD KEY `ID` (`ID`);

ALTER TABLE `archive_log`
 ADD PRIMARY KEY (`ID`);

ALTER TABLE `cces_photos`
 ADD PRIMARY KEY (`IMAGE_NO`);

ALTER TABLE `collection`
 ADD PRIMARY KEY (`ID`), ADD KEY `EAD_ID` (`EAD_ID`), ADD KEY `ID` (`ID`), ADD KEY `ID_2` (`ID`);

ALTER TABLE `container`
 ADD PRIMARY KEY (`ID`);

ALTER TABLE `ead`
 ADD PRIMARY KEY (`ID`);

ALTER TABLE `ead_admin`
 ADD PRIMARY KEY (`ID`), ADD KEY `EAD_ID` (`EAD_ID`);

ALTER TABLE `publisher`
 ADD PRIMARY KEY (`ID`), ADD KEY `EAD_ID` (`EAD_ID`);

ALTER TABLE `roles`
 ADD PRIMARY KEY (`ID`);

ALTER TABLE `subjects`
 ADD PRIMARY KEY (`ID`), ADD KEY `IMAGE_NO` (`IMAGE_NO`);

ALTER TABLE `users`
 ADD PRIMARY KEY (`ID`);


ALTER TABLE `access_control`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
ALTER TABLE `archive_log`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
ALTER TABLE `cces_photos`
MODIFY `IMAGE_NO` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
ALTER TABLE `collection`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
ALTER TABLE `container`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
ALTER TABLE `ead`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
ALTER TABLE `ead_admin`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
ALTER TABLE `publisher`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
ALTER TABLE `subjects`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
ALTER TABLE `users`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;

ALTER TABLE `access_control`
ADD CONSTRAINT `access_control_ibfk_1` FOREIGN KEY (`EAD_ID`) REFERENCES `ead` (`ID`);

ALTER TABLE `collection`
ADD CONSTRAINT `collection_ibfk_1` FOREIGN KEY (`EAD_ID`) REFERENCES `ead` (`ID`);

ALTER TABLE `ead_admin`
ADD CONSTRAINT `ead_admin_ibfk_1` FOREIGN KEY (`EAD_ID`) REFERENCES `ead` (`ID`);

ALTER TABLE `publisher`
ADD CONSTRAINT `publisher_ibfk_1` FOREIGN KEY (`EAD_ID`) REFERENCES `ead` (`ID`);

ALTER TABLE `roles`
ADD CONSTRAINT `roles_ibfk_1` FOREIGN KEY (`ID`) REFERENCES `users` (`ID`);

ALTER TABLE `subjects`
ADD CONSTRAINT `FK` FOREIGN KEY (`IMAGE_NO`) REFERENCES `cces_photos` (`IMAGE_NO`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
