<div class="form-group">
 <fieldset id="cces_main_information">
	<legend>Catholic Church Extension Society Photograph Collection</legend>
	<p>* indicates required</p>	
	<div class="form-group" id="ead_subtitle">
		<label for="CCESID">* CCES ID#</label>
		<div class="ead_right">
			<input class="form-control" type="text" id="txt_ead_cces_id_no" name="txt_ead_cces_id_no" required="required"  value="<?php echo $ead_container_details['CCESID'];?>" />
		</div>
	</div>

	<div class="form-group" id="ead_subtitle">
		<label for="OTHERID">Other ID#</label>
		<div class="ead_right">
			<input class="form-control" type="text" id="txt_ead_other_id_no" name="txt_ead_other_id_no"  value="<?php echo $ead_container_details['OTHERID'];?>"/>
		</div>
	</div>

	<div class="form-group" id="ead_subtitle">
		<label for="STATE">* State</label>
		<div class="ead_right">
			<select id="txt_ead_state" name="txt_ead_state">
				<option value="">-- Select a State--</option>
				<?php foreach($states as $key=>$value): ?>
					<option value="<?php echo $key;?>" <?php if ($key == $ead_container_details['STATE']){echo "selected";}?>><?php echo $value;?></option>
				<?php endforeach; ?>
			</select>
		</div>
	</div>

	<div class="form-group" id="ead_subtitle">
		<label for="TOWN_OR_CITY">Town OR City</label>
		<div class="ead_right">
			<input class="form-control" type="text" id="txt_ead_town_or_city" name="txt_ead_town_or_city" value="<?php echo $ead_container_details['TOWN_OR_CITY'];?>" />
		</div>
	</div>

	<div class="form-group" id="ead_subtitle">
		<label for="DIOCESE">Diocese</label>
		<div class="ead_right">
			<input class="form-control" type="text" id="txt_ead_diocese" name="txt_ead_diocese"  value="<?php echo $ead_container_details['DIOCESE'];?>"/>
		</div>
	</div>

	<div class="form-group" id="ead_subtitle">
		<label for="CHURCH">Church</label>
		<div class="ead_right">
			<input class="form-control" type="text" id="txt_ead_church" name="txt_ead_church"  value="<?php echo $ead_container_details['CHURCH'];?>"/>
		</div>
	</div>

	<div class="form-group" id="ead_subtitle">
		<label for="DATE">Date</label>
		<div class="ead_right">
			<input class="form-control" type="text" id="txt_ead_date" name="txt_ead_date" value="<?php echo $ead_container_details['DATE'];?>"/>
		</div>
	</div>

	<div class="form-group" id="ead_subtitle">
		<label for="DESCRIPTION">* Description of the Image</label>
		<div class="ead_right">
			<input class="form-control" type="text" id="txt_ead_description_of_image" name="txt_ead_description_of_image" required="required" value="<?php echo $ead_container_details['DESCRIPTION'];?>"/>
		</div>
	</div>

	<div class="form-group" id="ead_subtitle">
		<label for="TRANSRIPTION">Transcription</label>
		<div class="ead_right">
			<input class="form-control" type="text" id="txt_ead_transcription" name="txt_ead_transcription"  value="<?php echo $ead_container_details['TRANSCRIPTION'];?>"/>
		</div>
	</div>

	<div class="form-group" id="ead_subtitle">
		<label for="IMAGE_SIZE_L">* Image Size (L)</label>
		<div class="ead_right">
			<input class="form-control" type="text" id="txt_ead_summary_image_size_l" name="txt_ead_summary_image_size_l" required="required"  value="<?php echo $ead_container_details['IMAGE_SIZE_L'];?>"/>
		</div>
	</div>
	
		<div class="form-group" id="ead_subtitle">
		<label for="IMAGE_SIZE_W">* Image Size (W)</label>
		<div class="ead_right">
			<input class="form-control" type="text" id="txt_ead_summary_image_size_w" name="txt_ead_summary_image_size_w" required="required" value="<?php echo $ead_container_details['IMAGE_SIZE_W'];?>"/>
		</div>
	</div>

	<div class="form-group" id="ead_subtitle">
		<label for="TYPE">Image Type</label>
		<div class="ead_right">
			<input class="form-control" type="text" id="txt_ead_summary_image_type" name="txt_ead_summary_image_type" value="<?php echo $ead_container_details['TYPE'];?>" />
		</div>
	</div>
	
	<div class="form-group" id="ead_subtitle">
		<input type="hidden" value="<?php echo $ead_container_details['FILENAME'];?>" name="old_image" id="old_image" />
		<label for="FILENAME">Select image to upload:</label>
		<div class="ead_right">
			<input type="file" name="fileToUpload" id="fileToUpload" /> 
		</div>
	</div>
	
	<div>
		<label> Current Image:</label>
		<div class="specific_cces_content_filename_right specific_cces_right">
			<img width="500" src="/archive/uploads/<?php echo $ead_container_details['FILENAME']; ?>" >
		</div>
	</div>
	<div class="clear"></div>
	
 </fieldset>
</div>
