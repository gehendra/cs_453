<div class="form-group">	
	<fieldset id="ead_admin_info">
		<legend>Administrative Information</legend>
    <div class="form-group" id="lbl_ead_admin_info_acc_info">    
      <label for="Accrual Information">Accrual Information:</label>
			<div class="ead_right">
		  	<input class="form-control" type="text" id="txt_ead_admin_info_acc_info" name="txt_ead_admin_info_acc_info" />
			</div>
  	</div>

		<div class="form-group" id="lbl_ead_admin_info_prov_info">    
    	<label for="Provenance Information">Provenance Information:</label>
			<div class="ead_right">
		  	<input class="form-control" type="text" id="txt_ead_admin_info_prov_info" name="txt_ead_admin_info_prov_info" />
			</div>
    </div>
      
		<div class="form-group" id="lbl_ead_admin_info_acq_info">    
    	<label for="Acquisition Information">Acquisition Information:</label>
			<div class="ead_right">
		  	<input class="form-control" type="text" id="txt_ead_admin_info_acq_info" name="txt_ead_admin_info_acq_info" />
			</div>
		</div>        
          
		<div class="form-group" id="lbl_ead_admin_info_usage_restriction">    
			<label for="Usage Restrictions">Usage Restrictions:</label>
			<div class="ead_right">
		  	<input class="form-control" type="text" id="txt_ead_admin_info_usage_restriction" name="txt_ead_admin_info_usage_restriction" />
			</div>
    </div>

		<div class="form-group" id="lbl_ead_admin_info_pref_citation">    
    	<label for="Preferred Citation">Preferred Citation:</label>
			<div class="ead_right">
		  	<input class="form-control" type="text" id="txt_ead_admin_info_pref_citation" name="txt_ead_admin_info_pref_citation" />
			</div>
    </div>  
          
		<div class="form-group" id="lbl_ead_admin_info_access_restrictions">
			<div class="row">
   			<div class="col-xs-4">
   				<label for="Access Restrictions">List any Restrictions:</label>
  				<textarea class="form-control" name="txt_ead_admin_info_access_restrictions" id="txt_ead_admin_info_access_restrictions" cols="45" rows="5" title="List any restrictions on the collection."></textarea>
				</div>
 			</div>     
			</div>    
      
		<div class="form-group" id="lbl_ead_admin_info_processing_info">    
			<label for="Processing Information">Processing Information:</label>
			<div class="ead_right">
			  <input class="form-control" type="text" id="txt_ead_admin_info_processing_info" name="txt_ead_admin_info_processing_info" />
			</div>
		</div>         
		             
		<div class="form-group" id="lbl_ead_admin_info_bio_info">
		<div class="row">
 			<div class="col-xs-4">
 				<label for="Biographical Note/Administrative History:">Biographical Note/Administrative History:</label>
				<textarea class="form-control" name="txt_ead_admin_info_bio_info" id="txt_ead_admin_info_bio_info" cols="45" rows="5" title="List any restrictions on the collection."></textarea>
			</div>
			</div>     
		</div>
	</fieldset>
</div>