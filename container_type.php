<div class="container_type_container form-group">
	<div class="form-group">
		<input type="radio" name="collection_type[]" checked="checked" value="1">collection
		<input type="radio" name="collection_type[]" value="2">class
		<input type="radio" name="collection_type[]" value="3">files
		<input type="radio" name="collection_type[]" value="4">items
		<input type="radio" name="collection_type[]" value="5">series
  </div>
    
	<div class="container_content form-group">
    <div class="form-group" id="title">    
      <label for="title">Title:</label>
			<div class="ead_right">
				<input class="form-control" type="text" id="txt_ead_container_content_title" name="txt_ead_container_content_title[]" />
			</div>
    </div>
           
		<div class="form-group">
			<label for="Date">Date*</label>
			<div class="ead_right">
				<input type="radio" class="txt_ead_container_content_date" name="txt_ead_container_content_date[]" checked="checked" value="1">Not Applicable
				<input type="radio" class="txt_ead_container_content_date" name="txt_ead_container_content_date[]" value="2">Bulk
				<input type="radio" class="txt_ead_container_content_date" name="txt_ead_container_content_date[]" value="3">Inclusive
	    </div>
		</div>
		 
		<div class="container_content form-group">
			<div class="form-group" id="lbl_ead_container_content_title">    
				<label for="Language">Language :</label>
				<div class="ead_right">
					<input class="form-control" type="text" id="txt_ead_container_content_lang" name="txt_ead_container_content_lang[]" />
				</div>
		    </div> 
		</div>
	</div>
</div>
