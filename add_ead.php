<?php
session_start();
if(!($_SESSION['archive']['email']))
{
	header("Location: index.php");
}
?>
<?php include "header.php";?>
<?php
	//Insert EAD stuffs goes here
	if($_POST && $_POST["submit"] && $_POST['txt_ead_title'] && $_POST['txt_ead_date']) {
		include "insert_ead.php";
	}
?>
	<div class="content">
		<div class="form-content">
			<!--EAD Information Starts Here-->
				<?php include "add_ead_form.php"; ?>
			<!--EAD Information Ends Here-->
		</div>
	</div>
<?php include "footer.php"; ?>