<h2 style="margin-top:0">ADMIN</h2>
<table class="tbl_view_users_content" width="100%">
	<caption style="font-size:20px">Existing Users</caption>
	<tr>
		<th class="admin">Email</th>
		<th class="admin">First Name</th>
		<th class="admin">Last Name</th>
		<th class="admin">Role</th>
		<th class="admin">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Options</th>
	</tr>
	<?php foreach($all_user as $row): ?>
		<tr>
			<td width="35%"><?php echo $row['EMAIL']; ?></td>
			<td width="15%"><?php echo $row['FIRST_NAME']; ?></td>
			<td width="15%"><?php echo $row['LAST_NAME']; ?></td>
			<td width="20%"><?php if ($row['ROLE']==1) { echo 'Administrator';} 
								  if ($row['ROLE']==2) { echo 'Editor';}
								  if ($row['ROLE']==3) { echo 'Guest';}
			 ?></td>
			<td width="15%">
				<form name="frm_update_user" id="frm_update_user" method="post" action="update_user.php">
					<input type="hidden" value="<?php echo $row['ID'];?>" id="user_id" name="user_id">
					<input type="submit" class="btn btn-primary" name="user_update_submit" id="user_update_submit" value="Update" />
					<input type="submit" class="btn btn-danger" name="user_delete_submit" id="user_delete_submit" value="Delete" />
				</form>
			</td>
		</tr>
		<tr class="empty_row"><td colspan="5">&nbsp;</td></tr>
	<?php endforeach; ?>
</table>
<br></br>
<table class="tbl_view_users_count" width="100%">
	<caption style="font-size:20px">Existing User Count</catpion>
	<tr>
		<th class="admin">Role</th>
		<th class="admin">Count</th>
	</tr>
	<?php if ($admin_users) { ?>
	<tr>
		<td height="50">Administrator</td>
		<td height="50"><?php echo $admin_users;?></td>
	</tr>
	<?php } ?>
	<?php if ($admin_users) { ?>
	<tr>
		<td height="50">Editor</td>
		<td height="50"><?php echo $editor_users;?></td>
	</tr>
	<?php } ?>
	<?php if ($guest_users) { ?>
	<tr class>
		<td height="50">Guest</td>
		<td height="50"><?php echo $guest_users;?></td>
	</tr>
	<?php } ?>
</table>