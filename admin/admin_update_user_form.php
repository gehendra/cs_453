<form name="add_user" method="post" action="/archive/admin/admin_update_user_info.php">
	<input type="hidden" name="txt_update_user" id="txt_update_user" value="txt_update_user" />
	<input type="hidden" name="txt_update_user_id" id="txt_update_user_id" value="<?php echo $user_info["ID"]; ?>" />
	<div class="form-group" id="first_name">
		<label for="Title">First Name*</label>
		<div class="ead_right">
		  <input class="form-control" type="text" id="txt_first_name" name="txt_first_name" required="required" placeholder="Enter the first name" value="<?php echo $user_info["FIRST_NAME"];?>" />
		</div>
	</div>
	<div class="form-group" id="last_name">
		<label for="Title">Last Name*</label>
		<div class="ead_right">
		  <input class="form-control" type="text" id="txt_last_name" name="txt_last_name" required="required" placeholder="Enter the last name" value="<?php echo $user_info["LAST_NAME"];?>" />
		</div>
	</div>
	<div class="form-group" id="email">
		<label for="Title">Email*</label>
		<div class="ead_right">
		  <input class="form-control" type="text" id="txt_email" name="txt_email" required="required" placeholder="Enter email"  value="<?php echo $user_info["EMAIL"];?>" readonly />
		</div>
	</div>
	<div class="form-group" id="password">
		<label for="Title">Password*</label>
		<div class="ead_right">
		  <input class="form-control" type="password" id="txt_password" name="txt_password" placeholder="Enter password" />
		</div>
	</div>
	<div class="form-group" id="password">
		<label for="Title">Role*</label>
		<div class="ead_right">
		  <select name="txt_role" id="txt_role">
			  <?php
				  foreach ($all_roles as $role): ?>
						<option value="<?php echo $role['ID'];?>" <?php if ($user_info["ROLE"] == $role["ID"]) echo "selected=\"selected\""; ?>><?php echo $role['NAME'];?></option>
					<?php endforeach; ?>
			</select>
		</div>
	</div>
	<div class="form-group" id="submit">
		<div class="ead_right">
		  <input class="form-control btn btn-success" type="submit" id="btn_submit" name="btn_submit" value="Update" />
		</div>
	</div>
</form>
