<?php
	session_start();
	if (!isset($_SESSION['archive']['email']) && $_SESSION['archive']['role'] != "1") {
		header("Location: /archive/index.php");
	}

	if(isset($_POST['user_delete_submit']) && isset($_POST['user_id']) && $_POST['user_id'] != "") {
		include $_SERVER['DOCUMENT_ROOT'] . "/archive/connection.php";
		try {
			$con = new PDO("mysql:host=$db_host;dbname=$mysql_name", $db_user, $db_pass);
			$sql = "CALL delete_user_info_by_id(:id)";
			$stmt = $con->prepare($sql);
			$stmt->bindParam(':id', intval($_POST['user_id']), PDO::PARAM_INT);
			$stmt->execute();
			header("Location: /archive/admin/admin_users.php");
			exit();
		}
		catch (PDOException $e)
	  {
	    $error = 'Error searching by email.' ;
	    exit();
	  }
	}

	if(isset($_POST['user_update_submit']) && isset($_POST['user_id']) && $_POST['user_id'] != "") {
		$user_id = intval($_POST['user_id']);
		$user_info = get_user_info($user_id);
	}
	
	include $_SERVER['DOCUMENT_ROOT']. "/archive/header.php"; ?>
	<div class="content">
		<?php 
			$all_roles = get_all_roles();
			include $_SERVER['DOCUMENT_ROOT']. "/archive/admin/admin_update_user_form.php";
		?>
	</div>
<?php
	include $_SERVER['DOCUMENT_ROOT']. "/archive/footer.php";

function get_user_info($user_id) {
	include $_SERVER['DOCUMENT_ROOT']. "/archive/connection.php";
 	$user = "" ;
	try {
		$con = new PDO("mysql:host=$db_host;dbname=$mysql_name", $db_user, $db_pass);
		$sql = "CALL get_user_info_by_id(:id)";
		$stmt = $con->prepare($sql);
		$stmt->bindParam(':id', intval($user_id), PDO::PARAM_INT);
		$stmt->execute();
	}
	catch (PDOException $e)
  {
    $error = 'Error searching by email.' ;
    exit();
  }
  $user = $stmt->fetchAll();

 	return $user[0];
}
function get_all_roles() {
	include $_SERVER['DOCUMENT_ROOT']. "/archive/connection.php";
 	$all_roles = "" ;
	try {
		$con = new PDO("mysql:host=$db_host;dbname=$mysql_name", $db_user, $db_pass);
		$sql = "CALL get_all_roles()";
		$stmt = $con->prepare($sql);
		$stmt->execute();
	}
	catch (PDOException $e)
  {
    $error = 'Database connection error.' ;
    exit();
  }
  $all_roles = $stmt->fetchAll();
 	return $all_roles;
}
?>