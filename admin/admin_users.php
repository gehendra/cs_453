<?php
	session_start();
	if (!isset($_SESSION['archive']['email']) && $_SESSION['archive']['role'] != "1") {
		header("Location: /archive/index.php");
	}
	include $_SERVER['DOCUMENT_ROOT']. "/archive/header.php";
?>
	<div class="content">
		<?php 

			$all_user = get_all_users();
			$admin_users = get_users_by_role("Administrator");
			$editor_users = get_users_by_role("Editor");
			$guest_users = get_users_by_role("Guest");
			include $_SERVER['DOCUMENT_ROOT']. "/archive/admin/admin_all_user.php";
		?>
	</div>
<?php
	include $_SERVER['DOCUMENT_ROOT']. "/archive/footer.php";
?>

<?php 
function get_all_users() {
	include $_SERVER['DOCUMENT_ROOT'] . "/archive/connection.php";
	try {
		$con = new PDO("mysql:host=$db_host;dbname=$mysql_name", $db_user, $db_pass);
		$sql = "CALL get_all_users()";
		$stmt = $con->prepare($sql);
		$stmt->execute();
	}
	catch (PDOException $e)
  {
    $error = 'Error searching by email.' ;
    exit();
  }
  $all_users = $stmt->fetchAll();
 	return $all_users;
}
function get_users_by_role($role) {
	include $_SERVER['DOCUMENT_ROOT'] . "/archive/connection.php";
	try {
		$con = new PDO("mysql:host=$db_host;dbname=$mysql_name", $db_user, $db_pass);
		$sql = "SELECT COUNT(*) as member_count FROM users as u join roles as r on u.ROLE=r.ID WHERE r.NAME=\"" .$role . "\"";
		$stmt = $con->prepare($sql);
		$stmt->execute();
	}
	catch (PDOException $e)
  {
    $error = 'Error: could not connect to database.' ;
    exit();
  }
  $all_users = $stmt->fetchAll();
 	return $all_users[0]["member_count"];
}
?>