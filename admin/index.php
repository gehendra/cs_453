<?php
	session_start();
	if (!isset($_SESSION['archive']['email']) && $_SESSION['archive']['role'] != "1") {
		header("Location: /archive/index.php");
	}
	include $_SERVER['DOCUMENT_ROOT']. "/archive/header.php";
?>
	<div class="content">
		<?php include $_SERVER['DOCUMENT_ROOT']. "/archive/admin/admin_sub_menu.php"; ?>
		<div class="admin-text">
			<h3>Click here for <a href="/archive/admin/admin_users.php">User Management</a>
			<br>
			<br>
			Click here for <a href="/archive/admin/logs.php">Logs</a>
			</h3>
		</div>
	</div>

<?php
	include $_SERVER['DOCUMENT_ROOT']. "/archive/footer.php";
?>

<?php
function get_admin_users() {
	include $_SERVER['DOCUMENT_ROOT']. "/archive/connection.php";
 	$admin_users = "" ;
	try {
		$con = new PDO("mysql:host=$db_host;dbname=$mysql_name", $db_user, $db_pass);
		$sql = "CALL get_all_admin_users()";
		$stmt = $con->prepare($sql);
		$stmt->execute();
	}
	catch (PDOException $e)
  {
    $error = 'Error searching by email.' ;
    exit();
  }
  $admin_users = $stmt->fetchAll();
 	return $admin_users;
}

?>