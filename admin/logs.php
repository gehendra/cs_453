<?php
	session_start();
	if (!isset($_SESSION['archive']['email']) && $_SESSION['archive']['role'] != "1") {
		header("Location: /archive/index.php");
	}
	include $_SERVER['DOCUMENT_ROOT']. "/archive/header.php";
?>
	<div class="content">
		<?php $admin_logs = admin_log();?>
		<table class="tbl_view_users_content" width="100%">
			<tr>
				<th class="admin">Description</th>
				<th class="admin">Time</th>
			</tr>
			<?php foreach($admin_logs as $row): ?>
				<tr>
					<td class="log_file"><?php echo $row['DESCRIPTION']; ?></td>
					<td class="log_file"><?php echo $row['LOGGED_TIME']; ?></td>
				</tr>
			<?php endforeach; ?>
		</table>
	</div>
	<?php include $_SERVER['DOCUMENT_ROOT']. "/archive/footer.php"; ?>
<?php 
	function admin_log() {
		include $_SERVER['DOCUMENT_ROOT'] . "/archive/connection.php";
		try {
			$con = new PDO("mysql:host=$db_host;dbname=$mysql_name", $db_user, $db_pass);
			$sql = "CALL get_all_log()";
			$stmt = $con->prepare($sql);
			$stmt->execute();
		}
		catch (PDOException $e)
	  {
	    $error = 'Error searching by email.' ;
	    exit();
	  }
	  $all_logs = $stmt->fetchAll();	
	 	return $all_logs;
	}
?>