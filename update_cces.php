<?php
session_start();
if(!isset($_SESSION['archive']['email']))
{
	header("Location: index.php");
}
?>
<?php
	if (isset($_POST['upd_submit']) && isset($_POST['txt_ead_cces_id_no']) && $_POST['txt_ead_cces_id_no'] != "" && isset($_POST['txt_ead_description_of_image'])) {
		include "connection.php";
		try {
			$con = new PDO("mysql:host=$db_host;dbname=$mysql_name", $db_user, $db_pass);
			$IMAGE_NO = $_POST['cces_image_no'];
			$CCESID= $_POST['txt_ead_cces_id_no'];
			$OTHERID = $_POST['txt_ead_other_id_no'];
			$STATE = $_POST['txt_ead_state'];
			$TOWN_OR_CITY = $_POST['txt_ead_town_or_city'];
			$DIOCESE = $_POST['txt_ead_diocese'];
			$CHURCH = $_POST['txt_ead_church'];
			$DATE = $_POST['txt_ead_date'];
			$DESCRIPTION = $_POST['txt_ead_description_of_image'];
			$TRANSCRIPTION = $_POST['txt_ead_transcription'];
			$IMAGE_SIZE_L = $_POST['txt_ead_summary_image_size_l'];
			$IMAGE_SIZE_W = $_POST['txt_ead_summary_image_size_w'];
			$TYPE = $_POST['txt_ead_summary_image_type'];
			$OLD_FILENAME = $_POST['old_image'];
			
			$sql = "CALL update_cces_by_id(:image_no, :ccesid, :otherid, :state, :town_or_city, :diocese, :church, :cces_date, :description, :transcription, :image_size_l, :image_size_w, :type)";
			$stmt = $con->prepare($sql);
			$stmt->bindParam(':image_no', intval($IMAGE_NO), PDO::PARAM_INT);
			$stmt->bindParam(':ccesid', intval($CCESID), PDO::PARAM_INT);
			$stmt->bindParam(':otherid', $OTHERID, PDO::PARAM_STR);
			$stmt->bindParam(':state', $STATE, PDO::PARAM_STR);
			$stmt->bindParam(':town_or_city', $TOWN_OR_CITY, PDO::PARAM_STR);
			$stmt->bindParam(':diocese', $DIOCESE, PDO::PARAM_STR);
			$stmt->bindParam(':church', $CHURCH, PDO::PARAM_STR);
			$stmt->bindParam(':cces_date', $DATE, PDO::PARAM_STR);
			$stmt->bindParam(':description', $DESCRIPTION, PDO::PARAM_STR);
			$stmt->bindParam(':transcription', $TRANSCRIPTION, PDO::PARAM_STR);
			$stmt->bindParam(':image_size_l', $IMAGE_SIZE_L, PDO::PARAM_INT);
			$stmt->bindParam(':image_size_w', $IMAGE_SIZE_W, PDO::PARAM_INT);
			$stmt->bindParam(':type', $TYPE, PDO::PARAM_STR);
			$result = $stmt->execute();
			if(isset($_FILES) && $_FILES['fileToUpload']['name']) {
				upload_image($IMAGE_NO, $_FILES, $OLD_FILENAME);
			}
			if ($_POST['txt_ead_subject_name']) {
				update_cces_subject($_POST['txt_ead_subject_name']);
			}
			header("Location: /archive/view_cces.php");
		}
		catch (PDOException $e) {
    $error = 'Database connection issue.' ;
    exit();
  	}
	}
?>

<?php include "header.php";
	if (isset($_POST['submit']) && isset($_POST['cces_image_no']) && $_POST['cces_image_no']!="") {
		$cces_image_no = $_POST['cces_image_no'];
		$ead_container_details = cces_info($cces_image_no);
	}
?>

<div class="content">
		<div class="form-content">
			<?php 
				$states = get_states();
				include $_SERVER['DOCUMENT_ROOT']. "/archive/update_cces/update_cces_form.php";
			?>
		</div>
	</div>
<?php include "footer.php"; ?>

<?php 
	function get_states() {
		$states = array('AL'=>"Alabama",
			'AK'=>"Alaska",
			'AZ'=>"Arizona",
			'AR'=>"Arkansas",
			'CA'=>"California",
			'CO'=>"Colorado",
			'CT'=>"Connecticut",
			'DE'=>"Delaware",
			'DC'=>"District Of Columbia",
			'FL'=>"Florida",
			'GA'=>"Georgia",
			'HI'=>"Hawaii",
			'ID'=>"Idaho",
			'IL'=>"Illinois",
			'IN'=>"Indiana",
			'IA'=>"Iowa",
			'KS'=>"Kansas",
			'KY'=>"Kentucky",
			'LA'=>"Louisiana",
			'ME'=>"Maine",
			'MD'=>"Maryland",
			'MA'=>"Massachusetts",
			'MI'=>"Michigan",
			'MN'=>"Minnesota",
			'MS'=>"Mississippi",
			'MO'=>"Missouri",
			'MT'=>"Montana",
			'NE'=>"Nebraska",
			'NV'=>"Nevada",
			'NH'=>"New Hampshire",
			'NJ'=>"New Jersey",
			'NM'=>"New Mexico",
			'NY'=>"New York",
			'NC'=>"North Carolina",
			'ND'=>"North Dakota",
			'OH'=>"Ohio",
			'OK'=>"Oklahoma",
			'OR'=>"Oregon",
			'PA'=>"Pennsylvania",
			'RI'=>"Rhode Island",
			'SC'=>"South Carolina",
			'SD'=>"South Dakota",
			'TN'=>"Tennessee",
			'TX'=>"Texas",
			'UT'=>"Utah",
			'VT'=>"Vermont",
			'VA'=>"Virginia",
			'WA'=>"Washington",
			'WV'=>"West Virginia",
			'WI'=>"Wisconsin",
			'WY'=>"Wyoming"
		);
		return $states;
	}
?>

<?php
	function cces_info($view_specific_cces) {
		include "connection.php";
		try {
			$con = new PDO("mysql:host=$db_host;dbname=$mysql_name", $db_user, $db_pass);
			$sql = "CALL get_specific_cces(:image_no)";
			$stmt = $con->prepare($sql);
			$stmt->bindParam(':image_no', intval($view_specific_cces), PDO::PARAM_INT);
			$result = $stmt->execute();
			$arr_cces_result = $stmt->fetchAll();
			$arr_cces_result = $arr_cces_result[0];

			$con = new PDO("mysql:host=$db_host;dbname=$mysql_name", $db_user, $db_pass);
			$sql = "CALL get_cces_subjects(:subjects_id)";
			$stmt = $con->prepare($sql);
			$stmt->bindParam(':subjects_id', intval($view_specific_cces), PDO::PARAM_INT);
			$stmt->execute();
			$arr_cces_result_subjects = $stmt->fetchAll();
			$arr_cces_result['SUBJECTS'] = $arr_cces_result_subjects;
			$arr_cces_result['SUBJECTS_COUNT'] = count($arr_cces_result_subjects);
		}
		catch (PDOException $e) {
	    $error = 'Database connection issue.' ;
	    exit();
	  }
		return $arr_cces_result;
	}

	//function upload_image($image_no,$_FILES) {
	function upload_image($image_no, $files, $old_filename) {
		$target_dir = $_SERVER['DOCUMENT_ROOT']. "/archive/uploads/";
		$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
		$break_filename = explode(".",$_FILES["fileToUpload"]["name"]);
		$filename = $image_no . "." . $break_filename[1];
		$target_file =  $target_dir . $filename;
		$uploadOk = 1;
		$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
		// Check if image file is a actual image or fake image
		if($image_no) {
		  $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
	    if($check !== false) {
	      $uploadOk = 1;
	    } else {
	      $uploadOk = 0;
	    }
		}
		// Check if file already exists
		if (file_exists($target_file)) {
			$unlink_file = $_SERVER['DOCUMENT_ROOT']. "/archive/uploads/" . $old_filename;
	    unlink($_SERVER['DOCUMENT_ROOT']. "/archive/uploads/" . $old_filename);
	    $uploadOk = 1;
		}
		else {
			unlink($_SERVER['DOCUMENT_ROOT']. "/archive/uploads/" . $old_filename);
			$uploadOk = 1;
		}

		// Check file size
		/*if ($_FILES["fileToUpload"]["size"] > 500000) {
		    echo "Sorry, your file is too large.";
		    $uploadOk = 0;
		}
		// Allow certain file formats
		if($imageFileType != "jpg" && $imageFileType != "JPG" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) {
		    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
		    $uploadOk = 0;
		}*/
		// Check if $uploadOk is set to 0 by an error
		if ($uploadOk == 0) {
		    echo "Sorry, your file was not uploaded.";
		// if everything is ok, try to upload file
		} else {
		  if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
		  	update_cces_filename($image_no,$filename);
        return $filename;
	    } else {
        echo "Sorry, there was an error uploading your file.";
	    }
		}
	}

	function update_cces_filename($image_no, $filename) {
		include "connection.php";
		try {
			$con = new PDO("mysql:host=$db_host;dbname=$mysql_name", $db_user, $db_pass);
			$sql = "CALL update_cces_filename(:image_no, :filename)";
			$stmt = $con->prepare($sql);
			$stmt->bindParam(':image_no', intval($image_no), PDO::PARAM_INT);
			$stmt->bindParam(':filename', $filename, PDO::PARAM_STR);
			$result = $stmt->execute();
			$arr_cces_result = $stmt->fetchAll();
		}
		catch (PDOException $e) {
	    $error = 'Database connection issue.' ;
	    exit();
	  }
	}

	function update_cces_subject($cces_subject) {
		include "connection.php";
		try {
			$con = new PDO("mysql:host=$db_host;dbname=$mysql_name", $db_user, $db_pass);
			foreach ($cces_subject as $key => $value) {
				$sql = "CALL update_cces_subject(:id, :subject)";
				$id = intval($key);
				$subject = $value;
				$stmt = $con->prepare($sql);
				$stmt->bindParam(':id', $id, PDO::PARAM_INT);
				$stmt->bindParam(':subject', $subject, PDO::PARAM_STR);
				$result = $stmt->execute();
			}
		}	
		catch (PDOException $e) {
	    $error = 'Database connection issue.' ;
	    exit();
	  }
	}
?>
