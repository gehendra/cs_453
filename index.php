<?php include "header.php";?>
	<div class="content">
		<div class="form-content">
			<!--EAD Information Starts Here-->
      <div class="homepage_header">
  		  <h2>Archival Finding Aids</h2>
  		  <p>Finding aids to be used by Loyola Archives.</p>
      </div>
			<h3><strong>Our Team Members:</strong></h3><br/>
      <div class="form-group">           
        <div class="row">
          <div class="col-sm-6 col-md-4">
            <div class="thumbnail team-member-pic">
              <img src="photo/gehendra.jpg" alt="Gehendra Karmacharya" title="Gehendra Karmacharya" />
              <div class="caption">
                <h3>Gehendra</h3>
                <p><a href="resume/gehendra.pdf" class="btn btn-danger" role="button" target="_blank">View Full Resume</a> </p>
              </div>
            </div>      
          </div>
          <!-- For Kristina -->
          <div class="col-sm-6 col-md-4">
            <div class="thumbnail team-member-pic">
              <img src="photo/kristina.png" alt="Kristina" title="Kristina">
              <div class="caption">
                <h3>Kristina</h3>
                <p><a href="resume/kristina.pdf" class="btn btn-danger" role="button" target="_blank">View Full Resume</a> </p>
              </div>
            </div>      
          </div>
          <!--For Marco -->
          <div class="col-sm-6 col-md-4">
            <div class="thumbnail team-member-pic">
              <img src="photo/marco.png" alt="Marco" title="Marco">
              <div class="caption">
                <h3>Marco</h3>
                <p><a href="resume/marco.pdf" class="btn btn-danger" role="button" target="_blank">View Full Resume</a></p>
              </div>
            </div>     
          </div>
        </div>
      </div>
			<table class="table table-hover">
        <tr>
          <th width="103" height="28">First Name</td>
          <th width="95">Last Name</td>
          <th width="50">Email</td>
        </tr>
        <tr>
          <td>Gehendra</td>
          <td>Karmacharya</td>
          <td><a href="mailto:gehendra.karmacharya@gmail.com">gehendra.karmacharya@gmail.com</a></td>
        </tr>
        <tr>
          <td>Kristina</td>
          <td>Schwoebel</td>
          <td><a href="mailto:kschwoe@luc.edu">kschwoe@luc.edu</a></td>
        </tr>
        <tr>
         <td>Marco</td>
          <td>Reynoso</td>
          <td><a href="mailto:mreynoso@luc.edu">mreynoso@luc.edu</a></td>
        </tr>
      </table>
			<!--EAD Information Ends Here-->
		</div>
	</div>
<?php include "footer.php"; ?>