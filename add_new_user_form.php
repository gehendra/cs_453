<form name="add_user" id="add_user" method="post" action="add_new_user.php">
	<div class="form-group" id="first_name">
		<div class="ead_left">
			<label for="Title">First Name: *</label>
		</div>
		<div class="ead_right">
		  <input class="form-control" type="text" id="txt_first_name" name="txt_first_name" required="required" placeholder="Enter the first name"/>
		</div>
	</div>
	<div class="clearfix"></div>
	<div class="form-group" id="last_name">
		<div class="ead_left">
			<label for="Title">Last Name: *</label>
		</div>
		<div class="ead_right">
		  <input class="form-control" type="text" id="txt_last_name" name="txt_last_name" required="required" placeholder="Enter the last name"/>
		</div>
	</div>
	<div class="clearfix"></div>
	<div class="form-group" id="email">
		<div class="ead_left">
			<label for="Title">Email: *</label>
		</div>
		<div class="ead_right">
		  <input class="form-control" type="text" id="txt_email" name="txt_email" required="required" placeholder="Enter email"/>
		</div>
	</div>
	<div class="clearfix"></div>
	<div class="form-group" id="password">
		<div class="ead_left">
			<label for="Title">Password: *</label>
		</div>
		<div class="ead_right">
		  <input class="form-control" type="password" id="txt_password" name="txt_password" required="required" placeholder="Enter password"/>
		</div>
	</div>
	<div class="clearfix"></div>
	<div class="form-group" id="submit">
		<div class="ead_left">
			&nbsp;
		</div>
		<div class="ead_right">
		  <input class="form-control btn btn-success" type="submit" id="btn_submit" name="btn_submit" value="Submit" />
		</div>
	</div>
	<div class="clearfix"></div>
</form>
<script>
$(document).ready(function (){
	$("#add_user").validate({
	  rules: {
	    txt_email: {
	      email: true
	    }
	  }
	});
});
</script>
