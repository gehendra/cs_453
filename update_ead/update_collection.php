<div class="form-group">
	<fieldset id="ead_collection">
		<legend>Collection</legend>
		<div class="form-group" id="ead_subtitle">    
      <label for="Title of the Collection">Title of the Collection:</label>
			<div class="ead_right">
				<input class="form-control" type="text" id="txt_ead_collection_title" name="txt_ead_collection_title" value="<?php echo $ead_container_details['COLLECTION_TITLE'];?>" />
			</div>
    </div>        
    <div class="form-group" id="lbl_ead_collection_inclusive_date">
      <label for="Inclusive Date">Inclusive Date*</label>
      <div class="row">
        <div class="col-xs-4">
          <input name="txt_ead_collection_inclusive_date" type="text" class="form-control" id="txt_ead_collection_inclusive_date" maxlength="10" required="required" value="<?php echo $ead_container_details['COLLECTION_INCL'];?>"/>
        </div>
      </div>
    </div>        

    <div class="form-group" id="lbl_ead_collection_bulk_date">
      <label for="Bulk Date">Bulk Date:*</label>
      <div class="row">
        <div class="col-xs-4">
		      <input name="txt_ead_collection_bulk_date" type="text" class="form-control" id="txt_ead_collection_bulk_date" maxlength="10" required="required" value="<?php echo $ead_container_details['COLLECTION_BULK'];?>" />
        </div>
      </div>
    </div>

    <div class="form-group" id="lbl_ead_collection_creator">    
      <label for="corporate name">Creator (corporate name):</label>
			<div class="ead_right">
			  <input class="form-control" type="text" id="txt_ead_collection_creator" name="txt_ead_collection_creator" value="<?php echo $ead_container_details['COLLECTION_CREATOR'];?>" />
			</div>
    </div>
       
    <div class="form-group" id="lbl_ead_collection_linear_feet">      
      <div class="row">
        <div class="col-xs-4">
          <label for="Number of Linear Feet">Number of Linear Feet :</label>
          <input type="text" class="form-control" id="txt_ead_collection_linear_feet_1" name="txt_ead_collection_linear_feet_1" placeholder="Linear Feet" value="<?php echo $ead_container_details['COLLECTION_LINEAR_FEET'];?>" />
        </div>
        <div class="col-xs-4">
          <label for="Number of Items">Number of items :</label>
          <input type="text" class="form-control" placeholder="Number of items" id="txt_ead_collection_linear_number" name="txt_ead_collection_linear_number" value="<?php echo $ead_container_details['COLLECTION_ITEM_NUM'];?>" />
        </div>
      </div>   
    </div>        
    <div class="form-group" id="lbl_ead_collection_material_in">      
      <div class="row">
        <div class="col-xs-4">
          <label for="Language of Materials">Language of Materials :</label>
          <input type="text" class="form-control" id="txt_ead_collection_material_in" name="txt_ead_collection_material_in" placeholder="Language of Materials" value="<?php echo $ead_container_details['COLLECTION_LANG']; ?>" />
        </div>
      <div class="col-xs-4">
        <input type="checkbox" class="form-control" id="chk_ead_collection_non_english" name="chk_ead_collection_non_english" <?php if ($ead_container_details['COLLECTION_NON_ENG']) echo "checked=\"checked\"" ?> value="<?php if ($ead_container_details['COLLECTION_NON_ENG']) {echo '1';} else {echo '0';}?>">Select if the collection includes a significant amounts of non-english materials
      </div>
    </div>   
	</fieldset>
</div>