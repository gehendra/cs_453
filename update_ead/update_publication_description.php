<div class="form-group" id="ead_publisher_description">
	<fieldset>
		<legend>Publication Description:</legend>
		<div class="ead_row" id="ead_publisher_description_publisher">
      <div class="form-group" id="ead_subtitle">
  			<label for="Publisher">Publisher</label>
  			<div class="ead_right">
  				<input class="form-control" type="text" id="txt_ead_publisher_description_publisher" name="txt_ead_publisher_description_publisher" value="<?php echo $ead_container_details['PUBLISHER'];?>" />
  			</div>
		  </div>
    </div>
    <div class="form-group" id="ead_subtitle">       
		  <label for="Description">Description</label>
		  <div class="ead_right">
        <textarea class="form-control" name="txt_ead_publisher_description_creation" id="txt_ead_publisher_description_creation" cols="45" rows="5"><?php echo $ead_container_details['PUBLISHER_DESC'];?></textarea>
      </div>
    </div>
    <div class="form-group" id="ead_subtitle">      
      <div class="row">
        <div class="col-xs-3">
          <label for="Date">Date</label>
          <input type="text" class="form-control" id="txt_ead_publisher_description_date" name="txt_ead_publisher_description_date" placeholder="Month/dd/yyyy" value="<?php echo $ead_container_details['PUBLISHER_DATE'];?>" />
        </div>
        <div class="col-xs-4">
          <label for="Language Description">Language of Description</label>
          <input type="text" class="form-control" placeholder="Add Languages" id="txt_ead_publisher_description_lang" name="txt_ead_publisher_description_lang" value="<?php echo $ead_container_details['PUBLISHER_LANG'];?>" />
        </div>
      </div>
    </div>		
		<fieldset id="ead_revision">
			<legend>Revision</legend>
      <div class="form-group" id="ead_subtitle">
        <label for="Description">Description</label>
		    <div class="ead_right">
          <textarea class="form-control" name="txt_ead_publisher_revision_desc" id="txt_ead_publisher_revision_desc" cols="45" rows="5"><?php echo $ead_container_details['REVISION_DESC'];?></textarea>
        </div> 
      </div>           
      <div class="row">
        <div class="col-xs-3">
          <label for="Revised">Revised on:</label>
          <input type="text" class="form-control" id="txt_ead_publisher_revision_date" name="txt_ead_publisher_revision_date" placeholder=" Add a revision Date" value="<?php echo $ead_container_details['REVISION_DATE'];?>"/>
        </div>
      </div>
		</fieldset>
	</fieldset>
</div>