<div class="form-group">	
	<fieldset id="ead_scope_and_content">
		<legend>Scope and Content</legend>
    <div class="form-group" id="lbl_ead_scope_content_note">
			<label for="Note">Note :</label>
			<div class="ead_right">
        <textarea class="form-control" name="txt_ead_scope_content_note" id="txt_ead_scope_content_note" cols="45" rows="5">
        	<?php echo $ead_container_details['SCOPE_CONTENT_NOTE'];?>
        </textarea>
      </div> 
		</div>   
  	<div class="form-group" id="lbl_ead_scope_content_arrangement_note">
			<label for="Arrangement Note">Arrangement Note :</label>
			<div class="ead_right">
      	<textarea class="form-control" name="txt_ead_scope_content_note" id="txt_ead_scope_content_note" cols="45" rows="5">
      		<?php echo $ead_container_details['SCOPE_CONTENT_ARRANGEMENT'];?>
      	</textarea>
    	</div> 
		</div>
		<fieldset>
			<legend>Controlled Access</legend>
			<?php
			$access_control_title = unserialize($arr_specific_ead_result['ACCESS_CONTROL_TITLE']);
			$count_access_control_title = count($access_control_title);
			$access_control_name = unserialize($arr_specific_ead_result['ACCESS_CONTROL_NAME']);
			$count_access_control_name = count($access_control_name);
			$access_control_corp_name = unserialize($arr_specific_ead_result['ACCESS_CONTROL_CORP_NAME']);
			$count_access_control_corp_name = count($access_control_corp_name);
			?>
			<?php include "update_access_control.php";?>
		</fieldset>
	</fieldset>
</div>