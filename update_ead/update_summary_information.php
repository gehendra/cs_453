<div class="form-group">	
	<fieldset id="ead_summary_info">
		<legend>Summary Information</legend>
		<div class="form-group" id="ead_subtitle">
			<label for="Publisher">Description</label>
			<div class="ead_right">
        <textarea class="form-control" name="txt_ead_summary_info_desc" id="txt_ead_summary_info_desc" cols="45" rows="5"><?php echo $ead_container_details['SUMMARY_INFO_DESC'];?></textarea>
      </div> 
		</div>    
		<div class="form-group" id="ead_subtitle">    
      <label for="Corporate Name">Corporate Name:</label>
			<div class="ead_right">
				<input class="form-control" type="text" id="txt_ead_summary_info_corp_name" name="txt_ead_summary_info_corp_name" value="<?php echo $ead_container_details['SUMMARY_INFO_CORP_NAME'];?>" />
			</div>
    </div>
    <label for="Address">Address:</label>
    <div class="row">
			<div class="col-xs-3">
        <textarea class="form-control" name="txt_ead_summary_info_addr" id="txt_ead_summary_info_addr" cols="45" rows="5"><?php echo $ead_container_details['SUMMARY_INFO_ADDR'];?></textarea>
      </div>
    </div>  
	</fieldset>
</div>