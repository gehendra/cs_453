<?php foreach($ead_container_details['CONTAINER'] as $container): ?>
<div class="container_type_container form-group">
	<div class="form-group">
		<input type="checkbox" name="collection_type[$container["ID"]][]" value="1" <?php if (isset($container['TYPE']) && $container['TYPE']=="1") echo "checked=\"checked\";"?>>collection
		<input type="checkbox" name="collection_type[$container["ID"]][]" value="2" <?php if (isset($container['TYPE']) && $container['TYPE']=="2") echo "checked=\"checked\";"?>>class
		<input type="checkbox" name="collection_type[$container["ID"]][]" value="3" <?php if (isset($container['TYPE']) && $container['TYPE']=="3") echo "checked=\"checked\";"?>>files
		<input type="checkbox" name="collection_type[$container["ID"]][]" value="4" <?php if (isset($container['TYPE']) && $container['TYPE']=="4") echo "checked=\"checked\";"?>>items
		<input type="checkbox" name="collection_type[$container["ID"]][]" value="5" <?php if (isset($container['TYPE']) && $container['TYPE']=="5") echo "checked=\"checked\";"?>>series
		<span>(*Note: Please select one.)</span>
  </div>
    
	<div class="container_content form-group">
    	<div class="form-group" id="title">    
        	<label for="title">Title:</label>
			<div class="ead_right">
				<input class="form-control" type="text" id="txt_ead_container_content_title" name="txt_ead_container_content_title[]" value="<?php echo $container['TITLE']; ?>"/>
			</div>
      	</div>
           
		<div class="form-group">
			<label for="Date">Date*</label>
			<div class="ead_right">
					<input type="checkbox" class="txt_ead_container_content_date" name="txt_ead_container_content_date[<?php echo $container["ID"];?>][]"  <?php if (isset($container['DATE_TYPE']) && $container['DATE_TYPE']=="1") echo "checked=\"checked\"";?> value="1">Not Applicable
					<input type="checkbox" class="txt_ead_container_content_date" name="txt_ead_container_content_date[<?php echo $container["ID"];?>][]" <?php if (isset($container['DATE_TYPE']) && $container['DATE_TYPE']=="2") echo "checked=\"checked\"";?> value="2">Bulk
					<input type="checkbox" class="txt_ead_container_content_date" name="txt_ead_container_content_date[<?php echo $container["ID"];?>][]" <?php if (isset($container['DATE_TYPE']) && $container['DATE_TYPE']=="3") echo "checked=\"checked\"";?> value="3">Inclusive
					<span>(*Note: Please select one.)</span>
	    </div>
		</div>
		 
		<div class="container_content form-group">
			<div class="form-group" id="lbl_ead_container_content_title">    
				<label for="Language">Language :</label>
				<div class="ead_right">
					<input class="form-control" type="text" id="txt_ead_container_content_lang" name="txt_ead_container_content_lang[<?php echo $container["ID"];?>][]" value="<?php echo $container['LANGUAGE']; ?>"/>
				</div>
		    </div> 
		</div>
	</div>
</div>
<?php endforeach; ?>
