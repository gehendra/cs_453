<div class="form-group">
	<fieldset id="ead_seperated_materials">
		<legend>Separated Materials</legend>
    <div class="form-group" id="lbl_ead_seperated_head">      
      <div class="row">
        <div class="col-xs-4">
          <label for="Head">Head :</label>
          <input type="text" class="form-control" id="txt_ead_seperated_head" name="txt_ead_seperated_head" placeholder="Head" value="<?php echo $ead_container_details['SEPERATED_MATERIAL_TITLE'];?>" />
        </div>
      </div>     
      </div>         
    <div class="form-group" id="List separate material">      
      <div class="row">
        <div class="col-xs-4">
          <label for="List separate material">List any Separated Materials :</label>
          <textarea class="form-control" name="txt_ead_seperated_body" id="txt_ead_seperated_body" cols="45" rows="5"><?php echo $ead_container_details['SEPERATED_MATERIAL_BODY'];?></textarea>
        </div>
      </div>     
    </div>   
	</fieldset>
</div>