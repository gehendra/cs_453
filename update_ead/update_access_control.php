<?php for($i=0; $i<$count_access_control_title;$i++) { ?>
<div class="access-control-container">
	<div class="form-group" id="lbl_ead_access_control_subject">    
		<label for="Subject">Subject:</label>
		<div class="ead_right">
		  <input class="form-control" type="text" id="txt_ead_access_control_subject" name="txt_ead_access_control_subject[]" value="<?php echo $access_control_title[$i];?>" />
		</div>
  </div>
	<div class="form-group" id="lbl_ead_access_control_name">    
		<label for="Personal Name">Personal Name:</label>
		<div class="ead_right">
			<input class="form-control" type="text" id="txt_ead_access_control_name" name="txt_ead_access_control_name[]" value="<?php echo $access_control_name[$i];?>" />
		</div>
  </div>     
	<div class="form-group" id="lbl_ead_access_control_corp_name">    
  	<label for="Corporate Name">Corporate Name:</label>
		<div class="ead_right">
		  <input class="form-control" type="text" id="txt_ead_access_control_corp_name" name="txt_ead_access_control_corp_name[]" value="<?php echo $access_control_corp_name[$i];?>" />
		</div>
	</div>     
</div>
<?php } ?>