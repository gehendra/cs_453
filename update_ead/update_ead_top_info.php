<div class="form-group" id="ead_title">
	<label for="Title">Title*</label>
	<div class="ead_right">
	  <input class="form-control" type="text" id="txt_ead_title" name="txt_ead_title" required="required" placeholder="Enter the title" value="<?php echo $ead_container_details['TITLE'];?>"/>
	</div>
</div>

<div class="form-group" id="ead_subtitle">
	<label for="Subtitle">Subtitle*</label>
	<div class="ead_right">
		<input class="form-control" type="text" id="txt_ead_subtitle" name="txt_ead_subtitle" required="required" placeholder="Enter the Subtitle" value="<?php echo $ead_container_details['SUBTITLE'];?>"/>
	</div>
</div>

<div class="form-group" id="ead_subtitle">
  <label for="Date">Date*</label>
	<div class="row">
    <div class="col-xs-4">
		<input name="txt_ead_date" type="text" class="form-control" id="txt_ead_date" maxlength="10" value="<?php echo $ead_container_details['DATE'];?>"/>
	</div>
    </div>
</div>

<div class="form-group" id="ead_subtitle">
	<label for="Author">Author*</label>
	<div class="ead_right">
		<input class="form-control" type="text" id="txt_ead_author" name="txt_ead_author" required="required" placeholder="Enter the authors" value="<?php echo $ead_container_details['AUTHOR'];?>"/>
	</div>
</div>