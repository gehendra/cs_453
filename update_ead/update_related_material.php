<div class="form-group">  
  <fieldset id="ead_related_materials">
    <legend>Related Materials</legend>
    <div class="form-group" id="lbl_ead_related_head">      
      <div class="row">
        <div class="col-xs-4">
          <label for="Head">Head :</label>
          <input type="text" class="form-control" id="txt_ead_related_head" name="txt_ead_related_head" placeholder="Head" value="<?php echo $ead_container_details['SUMMARY_INFO_CORP_NAME'];?>" />
        </div>
      </div>     
    </div>           
    <div class="form-group" id="List lbl_ead_related_body">      
      <div class="row">
        <div class="col-xs-4">
          <label for="List any Related Materials">List any Related Materials:</label>
          <textarea class="form-control" name="txt_ead_related_body" id="txt_ead_related_body" cols="45" rows="5" title="ex: Additional records such as [...] may be accessed in the...">
            <?php echo $ead_container_details['SUMMARY_INFO_CORP_NAME'];?>
          </textarea>
        </div>
      </div>     
    </div>  
  </fieldset>
</div>