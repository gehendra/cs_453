<form role="form" name="frmUpdateEAD" method="post" enctype="multipart/form-data" onsubmit="return isValidForm();">
	<input type="hidden" name="ead_id" id="ead_id" value="<?php echo $ead_container_details[ID];?>" />
	<?php include "update_ead_top_info.php"; ?>
	<?php include "update_publication_description.php"; ?>
	<?php include "update_summary_information.php"; ?>
	<?php include "update_collection.php"; ?>
	<?php include "update_seperated_material.php"; ?>
	<?php include "update_related_material.php"; ?>
	<?php include "update_administrative_info.php"; ?>
	<?php include "update_scope_content.php"; ?>
	<?php include "update_container.php"; ?>
	<div class="clear"></div> <br />
	<input class="btn btn-success" type="submit" name="upd_submit" id="upd_submit" value="submit" />
</form>