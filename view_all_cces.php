<h2 style="margin-top:0">Catholic Church Extension Society</h2>
<table class="tbl_view_cces_content" width="100%">
	<caption style="font-size:20px">CCES Entry View</caption>
	<tr>
		<th >CCES ID</th>
		<th>State</th>
		<th>Description</th>
		<th>Size (Length)</th>
		<th>Size (Width)</th>	
		<th>Image Thumbnail</th>
		<th>Options</th>
	</tr>
	<?php foreach($arr_cces_result as $row): ?>
		<tr>
			<td width="10%"><?php echo $row['CCESID']; ?></td>
			<td width="10%"><?php echo $row['STATE']; ?></td>
			<td width="40%"><?php echo $row['DESCRIPTION']; ?></td>
			<td width="10%"><?php echo $row['IMAGE_SIZE_L']; ?></td>
			<td width="10%"><?php echo $row['IMAGE_SIZE_W']; ?></td>
			<td width="10%"><img class="thumbnail_ccess" width="200" src="<?php echo "/archive/uploads/" . $row['FILENAME'];?>"></td>

			<td width="10%">
				<form name="frm_view_cces" id="frm_view_cces" method="get" action="view_cces.php">
					<a class="view_all_cces_link" href="view_cces.php?cces=<?php echo $row['IMAGE_NO'];?>">Details</a>  <br />
					<a class="view_all_cces_link" href="view_cces.php?xml=<?php echo $row['IMAGE_NO'];?>" target="_blank">Xml</a> <br />
					<a class="view_all_cces_link" href="view_cces.php?isCCES=<?php echo $row['IMAGE_NO'];?>" target="_blank">Ead</a>
				</form>
				<?php if ($is_user_logged_in && ($role == "1" || $role == "2")) { ?>
				<form name="frm_update_cces" id="frm_update_cces" method="post" action="update_cces.php">
					<input type="hidden" value="<?php echo $row['IMAGE_NO'];?>" id="cces_image_no" name="cces_image_no">
					<input type="submit" name="submit" id="submit" class="btn btn-success" value="Update" />
				</form>
				<?php }?>
				<?php if ($is_user_logged_in && ($role == "1")) { ?>
				
					<form name="frm_delete_cces" id="frm_delete_cces" method="post" action="delete_cces.php">
						<input type="hidden" value="<?php echo $row['IMAGE_NO'];?>" id="cces_image_no" name="cces_image_no">
						<input type="submit" name="delete_cces" id="delete_cces" class="btn btn-danger" value="Delete" />
					</form>
				<?php }?>
			</td>
		</tr>
		<tr class="empty_row"><td colspan="7">&nbsp;</td></tr>
	<?php endforeach; ?>
</table>