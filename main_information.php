<div class="form-group">
 <fieldset id="cces_main_information">
	<legend>Enter an Item for the Catholic Church Extension Society Photograph Collection</legend>
	<p>* indicates required</p>	
	<div class="form-group" id="ead_subtitle">
		<label for="CCESID">* CCES ID#</label>
		<div class="ead_right">
			<input class="form-control" type="text" id="txt_ead_cces_id_no" name="txt_ead_cces_id_no" required="required" placeholder="Enter the CCES ID#" />
		</div>
	</div>

	<div class="form-group" id="ead_subtitle">
		<label for="OTHERID">Other ID#</label>
		<div class="ead_right">
			<input class="form-control" type="text" id="txt_ead_other_id_no" name="txt_ead_other_id_no" placeholder="Enter any Other ID#" />
		</div>
	</div>

	<div class="form-group" id="ead_subtitle">
		<label for="STATE">* State</label>
		<div class="ead_right">
			<select id="txt_ead_state" name="txt_ead_state" required="required">
				<option value="">-- Select State--</option>
				<?php foreach($states as $key=>$value): ?>
					<option value="<?php echo $key;?>"><?php echo $value;?></option>
				<?php endforeach;?>
			</select>
		</div>
	</div>

	<div class="form-group" id="ead_subtitle">
		<label for="TOWN_OR_CITY">Town OR City</label>
		<div class="ead_right">
			<input class="form-control" type="text" id="txt_ead_town_or_city" name="txt_ead_town_or_city" placeholder="Enter the Town/ City" />
		</div>
	</div>

	<div class="form-group" id="ead_subtitle">
		<label for="DIOCESE">Diocese</label>
		<div class="ead_right">
			<input class="form-control" type="text" id="txt_ead_diocese" name="txt_ead_diocese" placeholder="Enter the Diocese" />
		</div>
	</div>

	<div class="form-group" id="ead_subtitle">
		<label for="CHURCH">Church</label>
		<div class="ead_right">
			<input class="form-control" type="text" id="txt_ead_church" name="txt_ead_church" placeholder="Enter the Church" />
		</div>
	</div>

	<div class="form-group" id="ead_subtitle">
		<label for="DATE">Date</label>
		<div class="ead_right">
			<input class="form-control" type="text" id="txt_ead_date" name="txt_ead_date" placeholder="Enter the Date" />
		</div>
	</div>

	<div class="form-group" id="ead_subtitle">
		<label for="DESCRIPTION">* Description of the Image</label>
		<div class="ead_right">
			<input class="form-control" type="text" id="txt_ead_description_of_image" name="txt_ead_description_of_image" required="required" placeholder="Enter a Description of the Image" />
		</div>
	</div>

	<div class="form-group" id="ead_subtitle">
		<label for="TRANSRIPTION">Transcription</label>
		<div class="ead_right">
			<input class="form-control" type="text" id="txt_ead_transcription" name="txt_ead_transcription" placeholder="Enter the Transcription (on the back)" />
		</div>
	</div>

	<div class="form-group" id="ead_subtitle">
		<label for="IMAGE_SIZE_L">* Image Size (L)</label>
		<div class="ead_right">
			<input class="form-control" type="text" id="txt_ead_summary_image_size_l" name="txt_ead_summary_image_size_l" required="required" placeholder="Enter the Image Size (L)" />
		</div>
	</div>
	
		<div class="form-group" id="ead_subtitle">
		<label for="IMAGE_SIZE_W">* Image Size (W)</label>
		<div class="ead_right">
			<input class="form-control" type="text" id="txt_ead_summary_image_size_w" name="txt_ead_summary_image_size_w" required="required" placeholder="Enter the Image Size (W)" />
		</div>
	</div>

	<div class="form-group" id="ead_subtitle">
		<label for="TYPE">Image Type</label>
		<div class="ead_right">
			<input class="form-control" type="text" id="txt_ead_summary_image_type" name="txt_ead_summary_image_type" placeholder="Enter the Image Type" />
		</div>
	</div>
		<div class="form-group" id="ead_subtitle">
		<label for="FILENAME">Select image to upload:</label>
		<div class="ead_right">
			<input type="file" name="fileToUpload" id="fileToUpload" required="required"> 
		</div>
	</div>
 </fieldset>
</div>
