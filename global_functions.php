<?php
function current_user_info() {
 	include "connection.php";
 	include "MySQL.php";
 	session_start();
 	$current_user = "" ;
 	if (isset($_SESSION['archive']['email']) && $_SESSION['archive']['email'] != "") {
 		$current_user_email = $_SESSION['archive']['email'];
 		try {
 			$con = new PDO("mysql:host=$db_host;dbname=$mysql_name", $db_user, $db_pass);
			$sql = "CALL get_user_info(:email)";
			$stmt = $con->prepare($sql);
			$stmt->bindParam(':email', $current_user_email, PDO::PARAM_STR);
			$stmt->execute();
 		}
 		catch (PDOException $e)
	  {
	    $error = 'Database connection issue.' ;
	    exit();
	  }
	  $current_user = $stmt->fetch();
 	}
 	return $current_user;
 }


function login($param)
{
 	include "connection.php";
 	include "MySQL.php";
 	$current_user = "";
 	if (isset($param["txt_user_email"]) && isset($param["txt_user_pass"])) {
 		try {
 			$con = new PDO("mysql:host=$db_host;dbname=$mysql_name", $db_user, $db_pass);
			$sql = "CALL user_login(:email)";
			$stmt = $con->prepare($sql);
			$stmt->bindParam(':email',$param["txt_user_email"], PDO::PARAM_STR);
			$stmt->execute();
 		}
 		catch (PDOException $e)
	  {
	    $error = 'Database connection issue.' ;
	    exit();
	  }
	  $current_user = $stmt->fetch();
 	}
 	return $current_user;
}

?>