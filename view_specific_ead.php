<?php
	$access_control_title = unserialize($arr_specific_ead_result['ACESS_CONTROL']['ACCESS_CONTROL_TITLE']);
	$count_access_control_title = count($access_control_title);
	$access_control_name = unserialize($arr_specific_ead_result['ACESS_CONTROL']['ACCESS_CONTROL_NAME']);
	$count_access_control_name = count($access_control_name);
	$access_control_corp_name = unserialize($arr_specific_ead_result['ACESS_CONTROL']['ACCESS_CONTROL_CORP_NAME']);
	$count_access_control_corp_name = count($access_control_corp_name);

	$max_count_access_control_array = max ($count_access_control_title,$count_access_control_name,$count_access_control_corp_name);
?>

<div class="specific_ead_content">
	<h2><?php echo $arr_specific_ead_result['TITLE'];?></h2>

	<?php if($arr_specific_ead_result['SUBTITLE']) { ?>
	<div class="specific_ead_content_subtitle">
		<div class="specific_ead_content_subtitle_left specific_ead_left">
			Subtitle:
		</div>
		<div class="specific_ead_content_subtitle_right specific_ead_right">
			<?php echo $arr_specific_ead_result['TITLE']; ?>
		</div>
		<div class="clear"></div>
	</div>
	<?php } ?>

	<?php if($arr_specific_ead_result['DATE']) { ?>
	<div class="specific_ead_content_date">
		<div class="specific_ead_content_date_left specific_ead_left">
			Date:
		</div>
		<div class="specific_ead_content_date_right specific_ead_right">
			<?php echo $arr_specific_ead_result['DATE']; ?>
		</div>
		<div class="clear"></div>
	</div>
	<?php } ?>

	<?php if($arr_specific_ead_result['AUTHOR']) { ?>
		<div class="specific_ead_content_author">
			<div class="specific_ead_content_author_left specific_ead_left">
				Author:
			</div>
			<div class="specific_ead_content_author_right specific_ead_right">
				<?php echo $arr_specific_ead_result['AUTHOR']; ?>
			</div>
			<div class="clear"></div>
		</div>
	<?php } ?>

	<?php if($arr_specific_ead_result['PUBLISHER']['PUBLISHER'] || $arr_specific_ead_result['PUBLISHER']['PUBLISHER_DESC'] || $arr_specific_ead_result['PUBLISHER']['PUBLISHER_DATE'] || $arr_specific_ead_result['PUBLISHER']['PUBLISHER_LANG']) { ?>
		<h2>Publication Information</h2>
		<?php if($arr_specific_ead_result['PUBLISHER']['PUBLISHER']) { ?>
			<div class="specific_ead_content_publisher">
				<div class="specific_ead_content_publisher_left specific_ead_left">
					Publisher:
				</div>
				<div class="specific_ead_content_publisher_right specific_ead_right">
					<?php echo $arr_specific_ead_result['PUBLISHER']['PUBLISHER']; ?>
				</div>
				<div class="clear"></div>
			</div>
		<?php } ?>

		<?php if($arr_specific_ead_result['PUBLISHER']['PUBLISHER_DESC']) { ?>
			<div class="specific_ead_content_publisher_desc">
				<div class="specific_ead_content_publisher_desc_left specific_ead_left">
					Description:
				</div>
				<div class="specific_ead_content_publisher_desc_right specific_ead_right">
					<?php echo $arr_specific_ead_result['PUBLISHER']['PUBLISHER_DESC']; ?>
				</div>
				<div class="clear"></div>
			</div>
		<?php } ?>

		<?php if($arr_specific_ead_result['PUBLISHER']['PUBLISHER_DATE']) { ?>
			<div class="specific_ead_content_publisher_date">
				<div class="specific_ead_content_publisher_date_left specific_ead_left">
					Published Date:
				</div>
				<div class="specific_ead_content_publisher_date_right specific_ead_right">
					<?php echo $arr_specific_ead_result['PUBLISHER']['PUBLISHER_DATE']; ?>
				</div>
				<div class="clear"></div>
			</div>
		<?php } ?>

		<?php if($arr_specific_ead_result['PUBLISHER']['PUBLISHER_LANG']) { ?>
			<div class="specific_ead_content_publisher_lang">
				<div class="specific_ead_content_publisher_lang_left specific_ead_left">
					Language:
				</div>
				<div class="specific_ead_content_publisher_lang_right specific_ead_right">
					<?php echo $arr_specific_ead_result['PUBLISHER']['PUBLISHER_LANG']; ?>
				</div>
				<div class="clear"></div>
			</div>
		<?php } ?>
	<?php } ?>

	<?php if($arr_specific_ead_result['REVISION_DESC'] || $arr_specific_ead_result['REVISION_DATE']) { ?>
		<h2> Revision Information </h2>
		<?php if($arr_specific_ead_result['REVISION_DESC']) { ?>
			<div class="specific_ead_content_revision_desc">
				<div class="specific_ead_content_revision_desc_left specific_ead_left">
					Description:
				</div>
				<div class="specific_ead_content_revision_desc_right specific_ead_right">
					<?php echo $arr_specific_ead_result['REVISION_DESC']; ?>
				</div>
				<div class="clear"></div>
			</div>
		<?php } ?>
		<?php if($arr_specific_ead_result['REVISION_DATE']) { ?>
			<div class="specific_ead_content_revision_date">
				<div class="specific_ead_content_revision_date_left specific_ead_left">
					Date:
				</div>
				<div class="specific_ead_content_revision_date_right specific_ead_right">
					<?php echo $arr_specific_ead_result['REVISION_DATE']; ?>
				</div>
				<div class="clear"></div>
			</div>
		<?php } ?>
	<?php } ?>

	<?php if($arr_specific_ead_result['SUMMARY_INFO_DESC'] || $arr_specific_ead_result['SUMMARY_INFO_CORP_NAME'] || $arr_specific_ead_result['SUMMARY_INFO_ADDR']) { ?>
		<h2>Summary Information</h2>
		<?php if($arr_specific_ead_result['SUMMARY_INFO_DESC']) { ?>
			<div class="specific_ead_content_summary_info_desc">
				<div class="specific_ead_content_summary_info_desc_left specific_ead_left">
					Description:
				</div>
				<div class="specific_ead_content_summary_info_desc_right specific_ead_right">
					<?php echo $arr_specific_ead_result['SUMMARY_INFO_DESC']; ?>
				</div>
				<div class="clear"></div>
			</div>
		<?php } ?>
		<?php if($arr_specific_ead_result['SUMMARY_INFO_CORP_NAME']) { ?>
			<div class="specific_ead_content_summary_info_corp_name">
				<div class="specific_ead_content_summary_info_corp_name_left specific_ead_left">
					Description:
				</div>
				<div class="specific_ead_content_summary_info_corp_name_right specific_ead_right">
					<?php echo $arr_specific_ead_result['SUMMARY_INFO_CORP_NAME']; ?>
				</div>
				<div class="clear"></div>
			</div>
		<?php } ?>
		<?php if($arr_specific_ead_result['SUMMARY_INFO_ADDR']) { ?>
			<div class="specific_ead_content_summary_info_addr">
				<div class="specific_ead_content_summary_info_addr_left specific_ead_left">
					Description:
				</div>
				<div class="specific_ead_content_summary_info_addr_right specific_ead_right">
					<?php echo $arr_specific_ead_result['SUMMARY_INFO_ADDR']; ?>
				</div>
				<div class="clear"></div>
			</div>
		<?php } ?>
	<?php } ?>
	
	<?php if($arr_specific_ead_result['COLLECTION']['COLLECTION_TITLE'] || $arr_specific_ead_result['COLLECTION']['COLLECTION_INCL'] || $arr_specific_ead_result['COLLECTION']['COLLECTION_BULK'] || $arr_specific_ead_result['COLLECTION']['COLLECTION_CREATOR'] || $arr_specific_ead_result['COLLECTION']['COLLECTION_LINEAR_FEET'] || $arr_specific_ead_result['COLLECTION']['COLLECTION_ITEM_NUM'] || $arr_specific_ead_result['COLLECTION']['COLLECTION_LANG']) { ?>
		<h2>Collection Information</h2>
		<?php if($arr_specific_ead_result['COLLECTION']['COLLECTION_TITLE']) { ?>
			<div class="specific_ead_content_collection_title">
				<div class="specific_ead_content_collection_title_left specific_ead_left">
					Title:
				</div>
				<div class="specific_ead_content_collection_title_right specific_ead_right">
					<?php echo $arr_specific_ead_result['COLLECTION']['COLLECTION_TITLE']; ?>
				</div>
				<div class="clear"></div>
			</div>
		<?php } ?>

		<?php if($arr_specific_ead_result['COLLECTION']['COLLECTION_CREATOR']) { ?>
			<div class="specific_ead_content_collection_creator">
				<div class="specific_ead_content_collection_creator_left specific_ead_left">
					Creator:
				</div>
				<div class="specific_ead_content_collection_creator_right specific_ead_right">
					<?php echo $arr_specific_ead_result['COLLECTION']['COLLECTION_CREATOR']; ?>
				</div>
				<div class="clear"></div>
			</div>
		<?php } ?>

		<?php if($arr_specific_ead_result['COLLECTION']['COLLECTION_ITEM_NUM']) { ?>
			<div class="specific_ead_content_collection_item_num">
				<div class="specific_ead_content_collection_item_num_left specific_ead_left">
					Item Number:
				</div>
				<div class="specific_ead_content_collection_item_num_right specific_ead_right">
					<?php echo $arr_specific_ead_result['COLLECTION']['COLLECTION_ITEM_NUM']; ?>
				</div>
				<div class="clear"></div>
			</div>
		<?php } ?>

		<?php if($arr_specific_ead_result['COLLECTION']['COLLECTION_INCL']) { ?>
			<div class="specific_ead_content_collection_inclusive">
				<div class="specific_ead_content_collection_inclusive_left specific_ead_left">
					Inclusive Date:
				</div>
				<div class="specific_ead_content_collection_inclusive_right specific_ead_right">
					<?php echo $arr_specific_ead_result['COLLECTION']['COLLECTION_INCL']; ?>
				</div>
				<div class="clear"></div>
			</div>
		<?php } ?>

		<?php if($arr_specific_ead_result['COLLECTION']['COLLECTION_BULK']) { ?>
			<div class="specific_ead_content_collection_bulk">
				<div class="specific_ead_content_collection_bulk_left specific_ead_left">
					Bulk Date:
				</div>
				<div class="specific_ead_content_collection_bulk_right specific_ead_right">
					<?php echo $arr_specific_ead_result['COLLECTION']['COLLECTION_BULK']; ?>
				</div>
				<div class="clear"></div>
			</div>
		<?php } ?>
		
		<?php if($arr_specific_ead_result['COLLECTION']['COLLECTION_LINEAR_FEET']) { ?>
			<div class="specific_ead_content_collection_linear_feet">
				<div class="specific_ead_content_collection_linear_feet_left specific_ead_left">
					Linear Feet:
				</div>
				<div class="specific_ead_content_collection_linear_feet_right specific_ead_right">
					<?php echo $arr_specific_ead_result['COLLECTION']['COLLECTION_LINEAR_FEET']; ?>
				</div>
				<div class="clear"></div>
			</div>
		<?php } ?>

		<?php if($arr_specific_ead_result['COLLECTION']['COLLECTION_LANG']) { ?>
			<div class="specific_ead_content_collection_lang">
				<div class="specific_ead_content_collection_lang_left specific_ead_left">
					Language:
				</div>
				<div class="specific_ead_content_collection_lang_right specific_ead_right">
					<?php echo $arr_specific_ead_result['COLLECTION']['COLLECTION_LANG']; ?>
				</div>
				<div class="clear"></div>
			</div>
		<?php } ?>
		
		<?php if($arr_specific_ead_result['COLLECTION']['COLLECTION_NON_ENG'] == 0  || $arr_specific_ead_result['COLLECTION']['COLLECTION_NON_ENG'] == 1) { ?>
			<div class="specific_ead_content_collection_lang">
				<div class="specific_ead_content_collection_lang_left specific_ead_left">
					Significant Non-English:
				</div>
				<div class="specific_ead_content_collection_lang_right specific_ead_right">
					<?php if($arr_specific_ead_result['COLLECTION']['COLLECTION_NON_ENG'] == 0) { ?>
						No
					<?php } ?>
					<?php if($arr_specific_ead_result['COLLECTION']['COLLECTION_NON_ENG'] == 1) { ?>
						Yes
					<?php } ?>
				</div>
				<div class="clear"></div>
			</div>
		<?php } ?>
	<?php } ?>

	<?php if($arr_specific_ead_result['SEPERATED_MATERIAL_TITLE'] || $arr_specific_ead_result['SEPERATED_MATERIAL_BODY']) { ?>
		<h2>Seperated Material</h2>
		<?php if($arr_specific_ead_result['SEPERATED_MATERIAL_TITLE']) { ?>
			<div class="specific_ead_content_seperated_material_title">
				<div class="specific_ead_content_seperated_material_title_left specific_ead_left">
					Title:
				</div>
				<div class="specific_ead_content_seperated_material_title_right specific_ead_right">
					<?php echo $arr_specific_ead_result['SEPERATED_MATERIAL_TITLE']; ?>
				</div>
				<div class="clear"></div>
			</div>
		<?php } ?>

		<?php if($arr_specific_ead_result['SEPERATED_MATERIAL_BODY']) { ?>
			<div class="specific_ead_content_related_material_body">
				<div class="specific_ead_content_related_material_body_left specific_ead_left">
					Body:
				</div>
				<div class="specific_ead_content_related_material_body_right specific_ead_right">
					<?php echo $arr_specific_ead_result['SEPERATED_MATERIAL_BODY']; ?>
				</div>
				<div class="clear"></div>
			</div>
		<?php } ?>
	<?php } ?>

	<?php if($arr_specific_ead_result['RELATED_MATERIAL_TITLE'] || $arr_specific_ead_result['RELATED_MATERIAL_BODY']) { ?>
		<h2>Related Material</h2>
		<?php if($arr_specific_ead_result['RELATED_MATERIAL_TITLE']) { ?>
			<div class="specific_ead_content_related_material_title">
				<div class="specific_ead_content_related_material_title_left specific_ead_left">
					Title:
				</div>
				<div class="specific_ead_content_related_material_title_right specific_ead_right">
					<?php echo $arr_specific_ead_result['RELATED_MATERIAL_TITLE']; ?>
				</div>
				<div class="clear"></div>
			</div>
		<?php } ?>

		<?php if($arr_specific_ead_result['RELATED_MATERIAL_BODY']) { ?>
			<div class="specific_ead_content_related_material_body">
				<div class="specific_ead_content_related_material_body_left specific_ead_left">
					Body:
				</div>
				<div class="specific_ead_content_related_material_body_right specific_ead_right">
					<?php echo $arr_specific_ead_result['RELATED_MATERIAL_BODY']; ?>
				</div>
				<div class="clear"></div>
			</div>
		<?php } ?>
	<?php } ?>

	<?php if($arr_specific_ead_result['EAD_ADMIN']['ADMIN_INFO_ACCURAL'] || $arr_specific_ead_result['EAD_ADMIN']['ADMIN_INFO_PROVENANCE'] || $arr_specific_ead_result['EAD_ADMIN']['ADMIN_INFO_ACQUISITION'] || $arr_specific_ead_result['EAD_ADMIN']['ADMIN_INFO_USAGE'] || $arr_specific_ead_result['EAD_ADMIN']['ADMIN_INFO_PREFERRED_CITATION'] || $arr_specific_ead_result['EAD_ADMIN']['ADMIN_INFO_ACCESS'] || $arr_specific_ead_result['EAD_ADMIN']['ADMIN_INFO_PROCESSING'] || $arr_specific_ead_result['EAD_ADMIN']['ADMIN_INFO_BIO']) { ?>
		<h2>Administration Information</h2>
		<?php if($arr_specific_ead_result['EAD_ADMIN']['ADMIN_INFO_ACCURAL']) { ?>
			<div class="specific_ead_content_admin_info_accural">
				<div class="specific_ead_content_admin_info_accural_left specific_ead_left">
					Accural:
				</div>
				<div class="specific_ead_content_admin_info_accural_right specific_ead_right">
					<?php echo $arr_specific_ead_result['EAD_ADMIN']['ADMIN_INFO_ACCURAL']; ?>
				</div>
				<div class="clear"></div>
			</div>
		<?php } ?>

		<?php if($arr_specific_ead_result['EAD_ADMIN']['ADMIN_INFO_PROVENANCE']) { ?>
			<div class="specific_ead_content_admin_info_provenance">
				<div class="specific_ead_content_admin_info_provenance_left specific_ead_left">
					Provenance:
				</div>
				<div class="specific_ead_content_admin_info_provenance_right specific_ead_right">
					<?php echo $arr_specific_ead_result['EAD_ADMIN']['ADMIN_INFO_PROVENANCE']; ?>
				</div>
				<div class="clear"></div>
			</div>
		<?php } ?>

		<?php if($arr_specific_ead_result['EAD_ADMIN']['ADMIN_INFO_ACQUISITION']) { ?>
			<div class="specific_ead_content_admin_info_acquisition">
				<div class="specific_ead_content_admin_info_acquisition_left specific_ead_left">
					Acquisition:
				</div>
				<div class="specific_ead_content_admin_info_acquisition_right specific_ead_right">
					<?php echo $arr_specific_ead_result['EAD_ADMIN']['ADMIN_INFO_ACQUISITION']; ?>
				</div>
				<div class="clear"></div>
			</div>
		<?php } ?>

		<?php if($arr_specific_ead_result['EAD_ADMIN']['ADMIN_INFO_USAGE']) { ?>
			<div class="specific_ead_content_admin_info_usage">
				<div class="specific_ead_content_admin_info_usage_left specific_ead_left">
					Usage:
				</div>
				<div class="specific_ead_content_admin_info_usage_right specific_ead_right">
					<?php echo $arr_specific_ead_result['EAD_ADMIN']['ADMIN_INFO_USAGE']; ?>
				</div>
				<div class="clear"></div>
			</div>
		<?php } ?>

		<?php if($arr_specific_ead_result['EAD_ADMIN']['ADMIN_INFO_PREFERRED_CITATION']) { ?>
			<div class="specific_ead_content_admin_info_preferred_citation">
				<div class="specific_ead_content_admin_info_preferred_citation_left specific_ead_left">
					Citation:
				</div>
				<div class="specific_ead_content_admin_info_preferred_citation_right specific_ead_right">
					<?php echo $arr_specific_ead_result['EAD_ADMIN']['ADMIN_INFO_PREFERRED_CITATION']; ?>
				</div>
				<div class="clear"></div>
			</div>
		<?php } ?>

		<?php if($arr_specific_ead_result['EAD_ADMIN']['ADMIN_INFO_ACCESS']) { ?>
			<div class="specific_ead_content_admin_info_access">
				<div class="specific_ead_content_admin_info_access_left specific_ead_left">
					Access:
				</div>
				<div class="specific_ead_content_admin_info_access_right specific_ead_right">
					<?php echo $arr_specific_ead_result['EAD_ADMIN']['ADMIN_INFO_ACCESS']; ?>
				</div>
				<div class="clear"></div>
			</div>
		<?php } ?>

		<?php if($arr_specific_ead_result['EAD_ADMIN']['ADMIN_INFO_PROCESSING']) { ?>
			<div class="specific_ead_content_admin_info_processing">
				<div class="specific_ead_content_admin_info_processing_left specific_ead_left">
					Processing:
				</div>
				<div class="specific_ead_content_admin_info_processing_right specific_ead_right">
					<?php echo $arr_specific_ead_result['EAD_ADMIN']['ADMIN_INFO_PROCESSING']; ?>
				</div>
				<div class="clear"></div>
			</div>
		<?php } ?>

		<?php if($arr_specific_ead_result['EAD_ADMIN']['ADMIN_INFO_BIO']) { ?>
			<div class="specific_ead_content_admin_info_bio">
				<div class="specific_ead_content_admin_info_bio_left specific_ead_left">
					Accural:
				</div>
				<div class="specific_ead_content_admin_info_bio_right specific_ead_right">
					<?php echo $arr_specific_ead_result['EAD_ADMIN']['ADMIN_INFO_BIO']; ?>
				</div>
				<div class="clear"></div>
			</div>
		<?php } ?>
	<?php } ?>

	<?php if($arr_specific_ead_result['SCOPE_CONTENT_NOTE'] || $arr_specific_ead_result['SCOPE_CONTENT_ARRANGEMENT']) { ?>
		<h2>Scope Content</h2>
		<?php if($arr_specific_ead_result['SCOPE_CONTENT_NOTE']) { ?>
			<div class="specific_ead_content_scope_content_note">
				<div class="specific_ead_content_scope_content_note_left specific_ead_left">
					Accural:
				</div>
				<div class="specific_ead_content_scope_content_note_right specific_ead_right">
					<?php echo $arr_specific_ead_result['SCOPE_CONTENT_NOTE']; ?>
				</div>
				<div class="clear"></div>
			</div>
		<?php } ?>

		<?php if($arr_specific_ead_result['SCOPE_CONTENT_ARRANGEMENT']) { ?>
			<div class="specific_ead_content_scope_content_arrangement">
				<div class="specific_ead_content_scope_content_arrangement_left specific_ead_left">
					Provenance:
				</div>
				<div class="specific_ead_content_scope_content_arrangement_right specific_ead_right">
					<?php echo $arr_specific_ead_result['SCOPE_CONTENT_ARRANGEMENT']; ?>
				</div>
				<div class="clear"></div>
			</div>
		<?php } ?>
	<?php } ?>

	<?php for ($i=0;$i<$max_count_access_control_array;$i++) {
		if($access_control_title[$i] || $access_control_name[$i] || $access_control_corp_name[$i]) {
			if($access_control_title[$i]) {?>
				<div class="specific_ead_content_access_control_title">
					<div class="specific_ead_content_access_control_title_left specific_ead_left">
						Title:
					</div>
					<div class="specific_ead_content_access_control_title_right specific_ead_right">
						<?php echo $access_control_title[$i]; ?>
					</div>
					<div class="clear"></div>
				</div>
			<?php } ?>
			<?php if($access_control_name[$i]) { ?>
				<div class="specific_ead_content_access_control_name">
					<div class="specific_ead_content_access_control_name_left specific_ead_left">
						Name:
					</div>
					<div class="specific_ead_content_access_control_name_right specific_ead_right">
						<?php echo $access_control_title[$i]; ?>
					</div>
					<div class="clear"></div>
				</div>
			<?php } ?>

			<?php if($access_control_corp_name[$i]) { ?>
				<div class="specific_ead_content_access_control_corp_name">
					<div class="specific_ead_content_access_control_corp_name_left specific_ead_left">
						Corporate Name:
					</div>
					<div class="specific_ead_content_access_control_corp_name_right specific_ead_right">
						<?php echo $access_control_corp_name[$i]; ?>
					</div>
					<div class="clear"></div>
				</div>
			<?php } ?>
		<?php } ?>
		<br />
	<?php } ?>

	
		<!-- Container section begins

			Container type depends on TYPE value 
			1 -> class
			2 -> collection
			3 -> files
			4 -> item
			5 -> series

			Container date type depends on DATE_TYPE value
			1 -> N/A
			2 -> Bulk Date
			3 -> Inclusive Date
		-->
	<?php
		$container_type = 0;
		$arr_container_content = $arr_specific_ead_result['CONTAINER'];
		$container_loop = 0;
		$container_count = (int)$arr_specific_ead_result['CONTAINER_COUNT'];
		if ($container_count >= 1) {?>
			<h2>Container</h2>
			<?php foreach ($arr_container_content as $container_row):
				include "handle_container.php";?>
				<br /><br />
				<div class="clear"></div>
			<?php endforeach;
		}?>
</div>