# README #


### Loyola University Archives Catholic Church Extension Society Photograph Collection Project  ###

* Web-based application created to upload, insert EAD metadata, store, and retrieve images from the Catholic Church Extension Society 
* 1.0


### SETUP ###

* The following steps detail the locations of the main files that will need to be altered based on your setup
* The SQL dump is located in /archive/database_related_stuffs
* Database configuration is located in the connection.php file
* Our project document path: /archive/*      $home_directory = $_SERVER['DOCUMENT_ROOT']. "/archive”;
* To make image upload work on cces page:  $target_dir = $_SERVER['DOCUMENT_ROOT']. "/archive/uploads/"
* To view xml and ead formats for either project, changes need to be made to view_xml.php and view_xml_cces.php
* line number window.location.href = "http://localhost:8888/archive/ead.xml"}, 1000); 
* in  view_xml.php and window.location.href = "http://localhost:8888/archive/ead_cces.xml"}, 1000); 
* in view_xml_cces.php file. These urls should be updated as per your server set up url.